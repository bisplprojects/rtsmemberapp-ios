//
//  ProfileDashboardViewController.swift
//  RTSPortal
//
//  Created by sajan on 18/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift
import SafariServices

class ProfileDashboardViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var navigationBarLine: UIView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var profileCollectionView: UICollectionView!
    
    @IBOutlet weak var menuBtn: UIButton!
    let screenSize: CGRect = UIScreen.main.bounds

    private let reuseIdentifier = "ProfileCell"
    
    //var dashboardListTitles:[String]!
   // var dashboardListItemImages:[String]!
    var dashboardListTitles:[String] = []
    var dashboardListTitlesFilter:[String] = []
    var dashboardListItemImages:[String] = []
    var dashboardListItemImagesFilter:[String] = []
    var isLandingFromSignIn:Bool?
    var userDetails : User?

    var notificationCount : Int?
    var notificationCountString : String?

    var messageUrl:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.profileCollectionView.backgroundColor = UIColor.clear
        self.addShadowToNavigation()
        
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        self.userNameLbl.text = dict["UserName"]
//        dashboardListTitles = ["MY DETAILS", "MY EVENTS","MEMBERSHIP","COMMS PREF","MESSAGES","MY SIGS"]
//        dashboardListItemImages = ["mydetail_icon","my_events_icon","membership_icon","membership_icon","msg_icon","membership_icon"]
        
       // dashboardListTitles = ["MY DETAILS", "MY EVENTS","MEMBERSHIP","COMMS PREF","MY SIGS"]
        //dashboardListItemImages = ["mydetail_icon","my_events_icon","membership_icon","membership_icon","membership_icon"]

        self.loadProfileMenu()
        
        self.getNotificationCountApiCallFunc()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.loadMyDetailsApiCallFunc()
    }
    
    @IBAction func actionMenuBtnTapped(_ sender: Any) {
        UserActionManager.displayLeftMenu()
    }
    
    @objc func profileItemBtnTapped(_ sender:UIButton)  {
        
        print("tag \(sender.tag)")
        
        let cellItem = self.dashboardListTitles[sender.tag]

      /*  switch sender.tag {
        case 0:
            
            let myDetailVC = UIViewController.myDetailsViewController() as! MyDetailsViewController
            self.navigationController?.pushViewController(myDetailVC, animated: true)
            break
            
        case 1:
            let myEventVC = UIViewController.myEventsListViewController() as! MyEventsListViewController
            self.navigationController?.pushViewController(myEventVC, animated: true)
            break
            
        case 2:
            let membershipVC = UIViewController.membershipViewController() as! MemberShipViewController
            self.navigationController?.pushViewController(membershipVC, animated: true)
            break
            
        case 3:
            let commsPrefVC = UIViewController.commasPreferenceViewController() as! CommsPreferenceViewController
            self.navigationController?.pushViewController(commsPrefVC, animated: true)
            break

        case 4:
            let mySigVC = UIViewController.mySIGListViewController() as! MySIGListViewController
            self.navigationController?.pushViewController(mySigVC, animated: true)
            break


        default:
            print("Push to viewControllers")
        }*/
        
        switch cellItem {
        case "MY DETAILS":
            let myDetailVC = UIViewController.myDetailsViewController() as! MyDetailsViewController
            self.navigationController?.pushViewController(myDetailVC, animated: true)
            break
        case "MY EVENTS":
            let myEventVC = UIViewController.myEventsListViewController() as! MyEventsListViewController
            self.navigationController?.pushViewController(myEventVC, animated: true)
            break
        case "MEMBERSHIP" :
            let membershipVC = UIViewController.membershipViewController() as! MemberShipViewController
            self.navigationController?.pushViewController(membershipVC, animated: true)
            break
        case "COMMS PREF" :
            let commsPrefVC = UIViewController.commasPreferenceViewController() as! CommsPreferenceViewController
            self.navigationController?.pushViewController(commsPrefVC, animated: true)
            break
        case "MY MESSAGES":
            self.openMessagesInWebView()
            break
        case "MY SIGS":
            let mySigVC = UIViewController.mySIGListViewController() as! MySIGListViewController
            self.navigationController?.pushViewController(mySigVC, animated: true)
            break
        default:
            print("Push to viewControllers")
        }
        
    }
    
    func addShadowToNavigation() {
        self.navigationBarLine.layer.shadowColor = UIColor.black.cgColor
        self.navigationBarLine.layer.shadowOpacity = 1
        self.navigationBarLine.layer.shadowOffset = .zero
        self.navigationBarLine.layer.shadowRadius = 1
        self.navigationBarLine.layer.shadowPath = UIBezierPath(rect: self.navigationBarLine.bounds).cgPath
        self.navigationBarLine.layer.shouldRasterize = true
        self.navigationBarLine.layer.rasterizationScale = UIScreen.main.scale
        
    }

    // MARK: - Webservice
    
    func loadProfileMenu(){
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let roles = UserDefaults.standard.getUserRoles()
        
        let rolesString = convertIntoJSONString(arrayObject: roles)
        let param = ["entity" : "CiviMobileApp",
                     "action" : "menuitems",
                     "cid" : dict["ContactId"],
                     "token" : tokenString,
                     "roles" : rolesString];
        print("Menu Items request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Menu List response : \(json)")
            
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let menu = dataDict["menuitems"]?.arrayValue
                        if menu!.count > 0 {
                            let profileMenu = menu?.first
                            print("profile menu \(profileMenu)")
                            let subMenu = profileMenu!["profile_menu"].dictionaryValue
                            let profile_menu = subMenu["submenu"]!.arrayValue
                            print("home menu submenu items : \(profile_menu)")
                            
                            if profile_menu.count > 0 {
                                
                                let responseArray = profile_menu
                                print("response array count : \(responseArray.count)")
                                
                                
                                for (_, menudict) in (responseArray.enumerated()) {
                                    let menuObj = Menu(menuDict: menudict.dictionary!)
                                    print ("menu object : \(menuObj)")
                                    //self.dashboardMenuList.append(menuObj)
                                    let menuTitle = menuObj.name!
                                    let menuIcon = menuObj.icon!
                                    let menuDisplay = menuObj.display!
                                    let menuUrl = menuObj.msgUrl
                                    if menuTitle == "MY MESSAGES" {
                                        self.messageUrl = menuUrl
                                    }
                                    if (menuDisplay == "1"){
                                        
                                        print("value of menutitle: \(menuTitle)")
                                        // self.dashboardListTitlesFilter.append(menuTitle)
                                        self.dashboardListTitlesFilter.append(menuTitle)
                                        self.dashboardListItemImagesFilter.append(menuIcon)
                                        
                                    }
                                    else{
                                        self.dashboardListTitles = self.dashboardListTitlesFilter
                                        self.dashboardListItemImages = self.dashboardListItemImagesFilter
                                    }
                                    
                                }
                                
                                self.dashboardListTitles = self.dashboardListTitlesFilter
                                print("dashboard title list: \(self.dashboardListTitles)")
                                self.dashboardListItemImages = self.dashboardListItemImagesFilter
                                print("dashboard image list: \(self.dashboardListItemImages)")
                                
                                
                                print("title list: \(self.dashboardListTitles)")
                                print("dashboardListTitlescount  \(self.dashboardListTitles.count)")
                                
                                // self.dashboardListItemImages = self.dashboardListItemImagesFilter
                                // print("image list: \(self.dashboardListItemImages)")
                                
                                DispatchQueue.main.async {
                                    SVProgressHUD.dismiss()
                                    
                                    //self.dashboardMenuListFilter = self.dashboardMenuList
                                    self.profileCollectionView.reloadData()
                                    if self.dashboardListTitles.count == 0 {
                                        self.view.makeToast("No Menu Item found", duration: 3.0, position: .center)
                                    } else {
                                        self.profileCollectionView.reloadData()
                                        //self.dashboardMenuListFilter = self.dashboardMenuList
                                    }
                                }
                            }
                            else {
                                //Display msg
                                DispatchQueue.main.async {
                                    SVProgressHUD.dismiss()
                                    self.view.makeToast("No Menu Item found", duration: 3.0, position: .center)
                                }
                            }
                            
                        }
                        
                        
                    }
                    else
                    {
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.view.makeToast("something wrong in json if block", duration: 3.0, position: .center)
                        }
                    }
                }
                else
                {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast("something wrong in status if block", duration: 3.0, position: .center)
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.view.makeToast("something wrong in api block", duration: 3.0, position: .center)
                }
            }
        }
       
        
    }
    
    func loadMyDetailsApiCallFunc() {
        
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileContact",
                     "action" : "mydetails",
                     "token" : tokenString,
                     "cid" : dict["ContactId"]];
        print("My Membership Details request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("My Membership Details response : \(json)")
            
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        if dataDict["status"] == true{
                            
                            let myDetail = dataDict["mydetails"]?.arrayValue
                            if myDetail!.count > 0 {
                                
                                let responseDict = myDetail?.first
                                self.userDetails = User(dict: (responseDict?.dictionary)!)
                                
                                DispatchQueue.main.async {
                                    self.updateUserData()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func updateUserData()  {
        
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        let userName = self.userDetails?.userName
        let userImageString = self.userDetails?.userProfileImgURL
        let sessionID = dict["sessionId"]
        let contactId = dict["ContactId"]
        let roleStr = dict["role"]
        let pinStr = dict["PIN"]
        
        let bgsUserDict = ["UserName": userName, "ContactId": contactId, "PIN" : pinStr, "sessionId" : sessionID, "role" : roleStr, "ImageString" : userImageString]
        UserDefaults.standard.setBGSUserData(dict: bgsUserDict as! [String : String])
        
        self.userNameLbl.text = self.userDetails?.userName
    }
    
    func getNotificationCountApiCallFunc() {
        
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileContact",
                     "action" : "getnotificationscount",
                     "token" : tokenString,
                     "cid" : dict["ContactId"]];
        
        print("Notification Count request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Notification Count response : \(json)")
            
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        
                        self.notificationCountString = dataDict["notifications_count"]?.stringValue
                        self.notificationCount = dataDict["notifications_count"]?.intValue
                        
                        DispatchQueue.main.async {
                            self.profileCollectionView.reloadData()
                        }
                    }
                }
            }
        }
    }

    func openMessagesInWebView()  {
        
        let urlString:String? = self.messageUrl
        guard let stringURL = urlString else {
            return
        }
        //"http://jesusguerra.io/"
        
        guard let url = URL(string: stringURL) else {
            // We should handle an invalid stringURL
            return
        }

        // Present SFSafariViewController
        let safariVC = SFSafariViewController(url: url)
        safariVC.preferredBarTintColor = UIColor(red: 17.0/255.0, green: 131.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        safariVC.preferredControlTintColor = UIColor.white
        safariVC.dismissButtonStyle = .close
        present(safariVC, animated: true, completion: nil)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - UICollectionViewDataSource
extension ProfileDashboardViewController {
   
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dashboardListTitles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ProfileCell
        cell.backgroundColor = .black
        // Configure the cell
        let url = URL(string: self.dashboardListItemImages[indexPath.row])
        let data = try? Data(contentsOf: url!)
       // cell.itemBtn.setImage(UIImage(named: dashboardListItemImages[indexPath.row]), for: .normal)
        cell.itemBtn.setImage(UIImage(data: data!), for: .normal)
        cell.itemBtn.imageEdgeInsets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)

        cell.itemBtn.backgroundColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2)
        cell.contentView.frame = cell.bounds //This is important line
        cell.itemBtn.layoutIfNeeded() //This is important line
        cell.itemBtn.layer.masksToBounds = true
        cell.itemBtn.layer.cornerRadius = cell.itemBtn.frame.width * 0.5
        cell.itemBtn.tag = indexPath.row
        
        cell.itemBtn.addTarget(self, action: #selector(self.profileItemBtnTapped(_:)), for: .touchUpInside)
        
        cell.titleLbl.text = dashboardListTitles[indexPath.row]
        
        let cellItem = self.dashboardListTitles[indexPath.row]

        cell.badgeView.backgroundColor = UIColor(red: 246/255, green: 61/255, blue: 66/255, alpha: 1.0)
        cell.badgeView.layer.cornerRadius = cell.badgeView.frame.width * 0.5
        cell.badgeView.layoutIfNeeded() //This is important line
        cell.badgeView.layer.masksToBounds = true

        if let notifCount = self.notificationCount{
            if notifCount > 0 {
                cell.badgeCountLbl.text = self.notificationCountString
                cell.badgeView.isHidden = (cellItem == "MY MESSAGES") ? false : true
            }
            else{
                cell.badgeView.isHidden =  true
            }
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

// MARK: - Collection View Flow Layout Delegate
extension ProfileDashboardViewController : UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: screenSize.width * 0.31, height: screenSize.width * 0.31)
    }
    
}
