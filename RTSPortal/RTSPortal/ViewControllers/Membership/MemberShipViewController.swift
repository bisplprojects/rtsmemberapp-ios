//
//  MemberShipViewController.swift
//  RTSPortal
//
//  Created by sajan on 29/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD

class MemberShipViewController: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var membershipLogoView: UIView!
    @IBOutlet weak var membershipTypeLbl: UILabel!
    @IBOutlet weak var startDateLbl: UILabel!
    @IBOutlet weak var endDateLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    
    @IBOutlet weak var startDateTitleLbl: UILabel!
    @IBOutlet weak var endDateTitleLbl: UILabel!
    
    var membershipDetails:Membership?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.headerView.layer.cornerRadius = 5
        self.headerView.layer.shadowColor = UIColor.gray.cgColor
        self.headerView.layer.shadowOpacity = 0.3
        self.headerView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        self.headerView.layer.shadowRadius = 4
        
        self.membershipLogoView.layer.cornerRadius = self.membershipLogoView.frame.width / 2
        self.membershipLogoView.layer.shadowColor = UIColor.gray.cgColor
        self.membershipLogoView.layer.shadowOpacity = 0.3
        self.membershipLogoView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        self.membershipLogoView.layer.shadowRadius = 4

        self.hideUI()
        self.loadMembershipDetailsApiCallFunc()
    }
    

    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Webservice
    
    func loadMembershipDetailsApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileMember",
                     "action" : "mymemberships",
                     "token" : tokenString,
                     "cid" :  dict["ContactId"]];
        print("Events Details request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Events Details response : \(json)")
            
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let message = dataDict["message"]?.stringValue
                        
                        if dataDict["status"] == true{
                            let membershipDetailArray = dataDict["mymemberships"]?.arrayValue
                            
                            if membershipDetailArray!.count > 0 {
                                let responseDict = membershipDetailArray?.first
                                self.membershipDetails = Membership(dict: (responseDict?.dictionary)!)
                                DispatchQueue.main.async {
                                    SVProgressHUD.dismiss()
                                    self.updateUI()
                                    self.showUI()
                                }
                            }
                            else {
                                //Display msg
                                DispatchQueue.main.async {
                                    SVProgressHUD.dismiss()
                                    self.view.makeToast("Membership details not found", duration: 3.0, position: .center)
                                }
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast(message, duration: 3.0, position: .center)
                            }
                        }
                        
                    }
                    else{
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.view.makeToast("Membership details not found", duration: 3.0, position: .center)

                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }

    func hideUI()  {
        self.headerView.isHidden = true
        self.startDateLbl.text = ""
        self.endDateLbl.text = ""
        self.startDateTitleLbl.text = ""
        self.endDateTitleLbl.text = ""

    }
    
    func showUI()  {
        self.headerView.isHidden = false
    }
    
    func updateUI() {
        
        self.statusLbl.text = self.membershipDetails?.membershipStatus
        self.membershipTypeLbl.text = self.membershipDetails?.membershipName
     
        let startDate = self.membershipDetails?.membershipStartDate?.toDateFromString()
        let startDateString = startDate?.toString()
        self.startDateLbl.text = startDateString
        
        let endDate = self.membershipDetails?.membershipEndDate?.toDateFromString()
        let endDateString = endDate?.toString()
        self.endDateLbl.text = endDateString

        self.startDateTitleLbl.text = "Start Date"
        self.endDateTitleLbl.text = "End Date"

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
