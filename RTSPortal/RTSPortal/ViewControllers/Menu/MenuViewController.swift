//
//  MenuViewController.swift
//  RTSPortal
//
//  Created by sajan on 17/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift

class MenuViewController: UIViewController,UIGestureRecognizerDelegate{
    
    var revealMenu :UIView!
    var tapGesture:UITapGestureRecognizer!
    var isRevealMenuOpen:Bool!
    var leftSwipe :UISwipeGestureRecognizer!,  rightSwipe:UISwipeGestureRecognizer!
    var screenWidth:CGFloat!, screenHeight:CGFloat!
    var revealMenuHeaderImageView:UIImageView?
    var logoImageView:UIImageView?
    var logoTitleLbl:UILabel!
    
    var currentVC:UIViewController?
    
    var menuView:UIView!
    var menuTableView:UITableView!
   // var menuListTitles:[String]!
    //var menuListItemImages:[String]!
    var menuListTitles:[String] = []
    var menuListTitlesFilter:[String] = []
    var menuListItemImages:[String] = []
    var menuListItemImagesFilter:[String] = []
    
    var regImage:UIImage?
    var titleView: UILabel?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var userDetails : User?

    init() {
        
        super.init(nibName: nil, bundle: nil)
        self.createRevealMenu()
        self.enableGestures()
        isRevealMenuOpen = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    //MARK:- functions
    
    func createRevealMenu(){
        
        let frame:CGRect = UIScreen.main.bounds
        screenWidth = frame.size.width;
        screenHeight = frame.size.height;
        
        //MAIN VIEW
        menuView = UIView.init(frame: CGRect(x: -screenWidth, y: 0, width: screenWidth, height: screenWidth))
        menuView.backgroundColor = UIColor.clear
        self.view.backgroundColor = UIColor.clear
        
       // menuListTitles = ["Home", "Profile","Events", "App Info", "Logout"]
   // menuListItemImages=["home_menu_icon","profile_menu_icon","events_menu_icon","radio_off_icon", "logout_menu_icon"]
        self.loadDynamicMenu()
        
        let imageViewHeight : CGFloat = screenHeight * 0.33
        let contentViewWidth: CGFloat = screenWidth * 0.7
        
        revealMenuHeaderImageView = UIImageView.init(frame: CGRect(x: 0.0, y: 0.0, width: contentViewWidth, height: imageViewHeight))
        revealMenuHeaderImageView?.backgroundColor =  UIColor.white//UIColor(red: 28.0/255.0 , green: 20.0/255.0, blue: 50.0/255.0, alpha: 0.94)
        
        logoImageView = UIImageView.init(frame: CGRect(x: 0.0, y: 0.0, width: (revealMenuHeaderImageView?.frame.size.width)! * 0.4, height: (revealMenuHeaderImageView?.frame.size.width)! * 0.4))
        logoImageView?.contentMode = .scaleAspectFill
        logoImageView?.layer.cornerRadius = (logoImageView?.frame.size.width)! * 0.5
        logoImageView?.center = (revealMenuHeaderImageView?.center)!
        logoImageView?.layer.borderWidth = 0.1
        logoImageView?.layer.masksToBounds = false
        logoImageView?.layer.borderColor = UIColor(red: 28.0/255.0 , green: 20.0/255.0, blue: 50.0/255.0, alpha: 1.0).cgColor
        logoImageView?.clipsToBounds = true
        
        
        
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        let userImageString = dict["ImageString"]
        if (userImageString != nil){
            if userImageString != ""{
                
                if (userImageString?.hasPrefix("https"))!{
                    logoImageView!.sd_setImage(with: URL(string: userImageString!), placeholderImage: UIImage(named: ""))
                }
                else{
                    self.regImage = self.ConvertBase64StringToImage(imageBase64String: userImageString!)
                    if (self.regImage != nil){
                        logoImageView?.image = self.regImage
                    }

                }
            }
            else{
                logoImageView?.image = UIImage(named: "profile_placeholder_icon")
            }
        }

        self.revealMenuHeaderImageView?.addSubview(logoImageView!)
        
        //TITLE VIEW
        let userDisplayName = dict["UserName"]
        
        self.titleView = UILabel(frame: CGRect(x: 10.0, y: (logoImageView?.frame.origin.y)! + (logoImageView?.frame.height)! + 5, width: contentViewWidth - 20, height: 40))
        self.titleView!.text = userDisplayName
        self.titleView!.textAlignment = .center
        self.titleView?.numberOfLines = 0
        self.titleView!.font = UIFont(name: "Arial-BoldMT", size: 20)
        self.titleView!.textColor = UIColor(red: 37.0/255.0, green: 137.0/255.0, blue: 246.0/255.0, alpha: 1.0)//UIColor.red
        self.titleView!.contentMode = .scaleAspectFit
        self.titleView!.adjustsFontSizeToFitWidth = true
        let rightView:UIView = UIView.init(frame: CGRect(x: contentViewWidth, y: 0, width: screenWidth - contentViewWidth, height: screenHeight))
        tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.pushMenu))
        rightView.backgroundColor = UIColor(white:0.0, alpha: 0.8)
        rightView.addGestureRecognizer(tapGesture)
        
        menuTableView = UITableView.init(frame: CGRect(x: 0.0, y: imageViewHeight, width: contentViewWidth, height: screenHeight))
        menuTableView.register(MenuTableViewCell.self, forCellReuseIdentifier: "cell")         // register cell name
        menuTableView.tableFooterView = UIView()
        menuTableView.separatorStyle = .none
        menuTableView.showsVerticalScrollIndicator = false
        menuTableView.backgroundColor = UIColor.white//UIColor(red: 28.0/255.0 , green: 20.0/255.0, blue: 50.0/255.0, alpha: 0.94)
        menuTableView.delegate = self
        menuTableView.dataSource = self
        menuTableView.isScrollEnabled = false
        
        self.menuView.addSubview(menuTableView)
        self.menuView.addSubview(revealMenuHeaderImageView!)
        self.menuView.addSubview(rightView)
        
        // add title view abouve image view in view hirarchi
        self.menuView.addSubview(self.titleView!)
        
        self.view.addSubview(self.menuView)
//        self.updateUserDetails()

    }
    
    func ConvertBase64StringToImage (imageBase64String:String) -> UIImage {
        let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0))
        if imageData != nil{
            let image = UIImage(data: imageData!)
            return image!
        }else{
            return UIImage()
        }
    }

    func enableGestures() {
        
        leftSwipe = UISwipeGestureRecognizer.init(target: self, action: #selector(self.pushMenu))
        leftSwipe.delegate = self
        leftSwipe.isEnabled = false
        leftSwipe.direction = .left
        self.menuView.addGestureRecognizer(leftSwipe)
        
        rightSwipe = UISwipeGestureRecognizer.init(target: self, action: #selector(MenuViewController.pushMenu))
        rightSwipe.delegate = self
        rightSwipe.isEnabled = true
        rightSwipe.direction = .right
        self.menuView.addGestureRecognizer(rightSwipe)
        
    }
    
    @objc func pushMenu(){
        
        if(!isRevealMenuOpen){
            
            self.updateUserDetails()

            self.loadMyDetailsApiCallFunc()
            
            leftSwipe.isEnabled = true;
            rightSwipe.isEnabled = false;
            
            let newFrame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
            UIView.animate(withDuration: 0.4, animations: {
                
                self.currentVC?.navigationController?.view.addSubview(self.menuView)
                self.menuView.frame = newFrame
                
            }, completion: { isCompleted in
                
                self.isRevealMenuOpen = true
            })
        }
        else
        {
            self.hideRevealMenu(withAnimation: true )
        }
        
    }
    func hideRevealMenu(withAnimation shouldAnimate:Bool){
        
        rightSwipe.isEnabled = true
        leftSwipe.isEnabled = false
        let newFrame = CGRect(x: -screenWidth, y: 0, width: screenWidth, height: screenHeight)
        
        if(shouldAnimate){
            
            UIView.animate(withDuration: 0.4, animations: {
                
                self.menuView.frame = newFrame
                
            }, completion: { isCompleted in
                
                self.isRevealMenuOpen = false
                self.currentVC?.view.removeGestureRecognizer(self.leftSwipe)
                self.menuView.removeFromSuperview()
            })
        }
        else{
            
            self.menuView.frame = newFrame
            self.isRevealMenuOpen = false
            self.currentVC?.view.removeGestureRecognizer(self.leftSwipe)
            self.menuView.removeFromSuperview()
        }
        
    }
   func loadDynamicMenu() {
    
    SVProgressHUD.show()
    let apiObj = ApiHandler()
    var deviceToken = UserDefaults.standard.getDeviceToken()
    if deviceToken == nil
    {
    if let uuid = UIDevice.current.identifierForVendor?.uuidString {
    print(uuid)
    UserDefaults.standard.setDeviceToken(value: uuid)
    }
    }
    deviceToken = UserDefaults.standard.getDeviceToken()
    let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
    
    // logintype : veda or live
    let token = ["deviceId": deviceToken,
    "appName": "CiviMemberAPP",
    "sessionId": dict["sessionId"],
    "logintype": "live"];
    let tokenString = JSONStringify(value: token as! [String : String])
    let roles = UserDefaults.standard.getUserRoles()
    
    let rolesString = convertIntoJSONString(arrayObject: roles)
    let param = ["entity" : "CiviMobileApp",
    "action" : "menuitems",
    "cid" : dict["ContactId"],
    "token" : tokenString,
    "roles" : rolesString];
    print("Menu Items request params : \(param)")
    
    apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
    
    print("Menu List response : \(json)")
    
    if status
    {
    if json["is_error"] == 0{
    let dictonary = json["values"]
    if dictonary["validuser"] == true{
    
    let dataDict = dictonary["data"].dictionaryValue
    let menu = dataDict["menuitems"]?.arrayValue
    if menu!.count > 0 {
    let sliderMenu = menu?.first
    print("slider menu \(sliderMenu)")
    let subMenu = sliderMenu!["slider_menu"].dictionaryValue
    let slider_menu = subMenu["submenu"]!.arrayValue
    print("slider menu submenu items : \(slider_menu)")
    
    if slider_menu.count > 0 {
    
    let responseArray = slider_menu
    print("response array count : \(responseArray.count)")
    
    
    for (_, menudict) in (responseArray.enumerated()) {
    let menuObj = Menu(menuDict: menudict.dictionary!)
    print ("menu object : \(menuObj)")
    //self.dashboardMenuList.append(menuObj)
    let menuTitle = menuObj.name!
    let menuIcon = menuObj.icon!
    let menuDisplay = menuObj.display!
    if (menuDisplay == "1"){
    
    print("value of menutitle: \(menuTitle)")
    // self.dashboardListTitlesFilter.append(menuTitle)
    self.menuListTitlesFilter.append(menuTitle)
    self.menuListItemImagesFilter.append(menuIcon)
    
    }
    else{
    self.menuListTitles = self.menuListTitlesFilter
    self.menuListItemImages = self.menuListItemImagesFilter
    }
    
    }
    //self.dashboardMenuListFilter = self.dashboardMenuList
    // print("menulistfilter: \(self.dashboardMenuListFilter)")
    self.menuListTitles = self.menuListTitlesFilter
    print("menu title list: \(self.menuListTitles)")
    self.menuListItemImages = self.menuListItemImagesFilter
    print("menu image list: \(self.menuListItemImages)")
    
    
    print("dashboardListTitlescount  \(self.menuListTitles.count)")
    
   
    
    DispatchQueue.main.async {
    SVProgressHUD.dismiss()
    
    self.menuTableView.reloadData()
    
    if self.menuListTitles.count == 0 {
    self.view.makeToast("No Menu Item found", duration: 3.0, position: .center)
    } else {
   self.menuTableView.restore()
    }
    }
    }
    else {
    //Display msg
    DispatchQueue.main.async {
    SVProgressHUD.dismiss()
    self.view.makeToast("No Menu Item found", duration: 3.0, position: .center)
    }
    }
    
    }
    
    
    }
    else
    {
    //Display msg
    DispatchQueue.main.async {
    SVProgressHUD.dismiss()
    self.view.makeToast("something wrong in json if block", duration: 3.0, position: .center)
    }
    }
    }
    else
    {
    //Display msg
    DispatchQueue.main.async {
    SVProgressHUD.dismiss()
    self.view.makeToast("something wrong in status if block", duration: 3.0, position: .center)
    }
    }
    }
    else
    {
    //Display msg
    DispatchQueue.main.async {
    SVProgressHUD.dismiss()
    self.view.makeToast("something wrong in api block", duration: 3.0, position: .center)
    }
    }
    }
    
    
    }

    @objc func actionMenuItemTapped(_ sender:UIButton)  {
        
        self.hideRevealMenu(withAnimation: false)
                
        print("tag \(sender.tag)")
            switch sender.tag {
            case 0:
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.profileDashboardViewController())
                break
                
            case 1:
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.allEventsListViewController())
                break
                
            case 2 :
                self.logoutApiCallFunc()
                break
                
            default:
                print("Push to viewControllers")
            }
    }
    
    // MARK: -
    
    func moveToController(_ controller:UIViewController){
        
        var viewControllers = ( self.currentVC?.navigationController?.viewControllers)!
        viewControllers.removeLast()
        viewControllers.append(controller)
        self.currentVC?.navigationController?.setViewControllers(viewControllers, animated: true)
    }
    
    func displayRevealMenuToVC(currentVC:UIViewController){
        
        self.currentVC = currentVC
        currentVC.view.addGestureRecognizer(rightSwipe)
        self.pushMenu()
    }
    
    // MARK: - Webservice

    func logoutApiCallFunc() {
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileContact",
                     "action" : "logout",
                     "cid" : dict["ContactId"],
                     "token" : tokenString];
        print("Logout request params : \(param)")
        
        apiObj.postData(url:kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Logout response : \(json)")
            if status
            {
                let dictonary = json["values"].dictionaryValue
                let dataDict = dictonary["data"]
                
                if dataDict?["status"] == true {
                    
                    let titleStr = dataDict?["title"].stringValue
                    let messageStr = dataDict?["message"].stringValue
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        var style = ToastStyle()
                        style.messageColor = UIColor.white
                        self.currentVC?.view?.makeToast(messageStr, duration: 1.0, position: ToastPosition.center, title:titleStr, image:nil, style: style, completion: { (true) in
                            self.clearLoginDetails()
                        })
                    }
                }
                else if dataDict?["status"] == false{
                    //Display msg
                    let messageStr = "Something went wrong. Please contact admin!"
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast(messageStr, duration: 3.0, position: .center)
                    }
                }
                else {
                    //Display msg
                    let messageStr = "Something went wrong. Please contact admin!"
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast(messageStr, duration: 2.0, position: .center)
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }

    func clearLoginDetails()  {
//        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
//        UserDefaults.standard.synchronize()
        
        UserDefaults.standard.setRegisteredStatus(value: true)
        UserDefaults.standard.setTouchIDEnabled(value: true)
        UserDefaults.standard.setLoggedIn(value: false)
        UserDefaults.standard.synchronize()
        
        self.currentVC?.navigationController?.popViewController(animated: true)
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
        
       vc.registeredFlag = true
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func updateUserDetails() {
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        let userImageString = dict["ImageString"]
        
        if (userImageString != nil){
            if (userImageString != "") {
                self.regImage = self.ConvertBase64StringToImage(imageBase64String: userImageString!)
            }
        }
        if (self.regImage != nil){
            logoImageView?.image = self.regImage
        }
        let userDisplayName = dict["UserName"]
        self.titleView!.text = userDisplayName

    }
    
    // MARK: - Webservice
    
    func loadMyDetailsApiCallFunc() {
        
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileContact",
                     "action" : "mydetails",
                     "token" : tokenString,
                     "cid" : dict["ContactId"]];
        print("My Membership Details request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("My Membership Details response : \(json)")
            
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        if dataDict["status"] == true{
                            
                            let myDetail = dataDict["mydetails"]?.arrayValue
                            if myDetail!.count > 0 {
                                
                                let responseDict = myDetail?.first
                                self.userDetails = User(dict: (responseDict?.dictionary)!)
                                
                                DispatchQueue.main.async {
                                    self.updateUserData()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func updateUserData()  {
        
        
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        let userName = self.userDetails?.userName
        let userImageString = self.userDetails?.userProfileImgURL
        let sessionID = dict["sessionId"]
        let contactId = dict["ContactId"]
        let roleStr = dict["role"]
        let pinStr = dict["PIN"]
        
        let bgsUserDict = ["UserName": userName, "ContactId": contactId, "PIN" : pinStr, "sessionId" : sessionID, "role" : roleStr, "ImageString" : userImageString]
        UserDefaults.standard.setBGSUserData(dict: bgsUserDict as! [String : String])
        
        self.titleView!.text = userName

        if (userImageString != nil){
            if userImageString != ""{
                if (userImageString?.hasPrefix("https"))!{
                    logoImageView!.sd_setImage(with: URL(string: userImageString!), placeholderImage: UIImage(named: ""))
                }
                else{
                    self.regImage = self.ConvertBase64StringToImage(imageBase64String: userImageString!)
                    if (self.regImage != nil){
                        logoImageView?.image = self.regImage
                    }
                    
                }
            }
        }

    }

    // MARK: -

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension MenuViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuListTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuTableViewCell
        cell.backgroundColor = UIColor.clear
        
        cell.indicatorView = UIView(frame: CGRect(x: 0, y: 0, width: 3, height: cell.frame.size.height))
        cell.indicatorView!.backgroundColor = UIColor(red: 0.0/255.0, green: 97.0/255.0, blue:223.0/255.0, alpha: 1.0)
        cell.addSubview(cell.indicatorView!)
        
        let url = URL(string: self.menuListItemImages[indexPath.row])
        let data = try? Data(contentsOf: url!)
        cell.iconImageView = UIImageView(frame: CGRect(x: 25, y: 10, width: 25, height: 25))
        cell.iconImageView!.contentMode = UIView.ContentMode.scaleAspectFit
        
        
        
        //cell.iconImageView!.image = UIImage(named: self.menuListItemImages[indexPath.row])
        if data != nil{
            cell.iconImageView!.image = UIImage(data: data!)
        }
//        cell.iconImageView!.image = UIImage(data: data!)
        cell.addSubview(cell.iconImageView!)
        
        cell.titleLabel = UILabel(frame: CGRect(x: 70, y: 0, width: cell.frame.size.width * 0.65 - 50, height: cell.frame.size.height - 2))
        cell.titleLabel!.textColor = .darkGray
        cell.titleLabel!.text = menuListTitles[indexPath.row]
        cell.addSubview(cell.titleLabel!)
        
        cell.selectionStyle = .none

        if cell.isSelected == true{
            cell.indicatorView!.backgroundColor = UIColor(red: 0.0/255.0, green: 97.0/255.0, blue:223.0/255.0, alpha: 1.0)
        }
        else{
            cell.indicatorView!.backgroundColor = UIColor.white
        }
        
        return cell

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 45
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        if indexPath.row != 5{
            cell.backgroundColor = UIColor(red: 240.0/255.0, green: 244.0/255.0, blue:253.0/255.0, alpha: 1.0)
            cell.indicatorView?.backgroundColor = UIColor(red: 0.0/255.0, green: 97.0/255.0, blue:223.0/255.0, alpha: 1.0)
            cell.titleLabel?.textColor = UIColor(red: 82.0/255.0, green: 143.0/255.0, blue:231.0/255.0, alpha: 1.0)
        }
        
        self.hideRevealMenu(withAnimation: false)

        switch indexPath.row {
        case 0:
            if cell.titleLabel?.text == "HOME"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                let currentVc : UIViewController = (UIApplication.shared.delegate?.window??.rootViewController as! UINavigationController).topViewController!
                if !(currentVc.isKind(of: DashboardViewController.self))   {
                    self.moveToController(UIViewController.dashboardViewController())
                }
//                self.moveToController(UIViewController.dashboardViewController())
            }else if cell.titleLabel?.text == "PROFILE"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.profileDashboardViewController())
            } else if cell.titleLabel?.text == "EVENTS"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.allEventsListViewController())
            } else if cell.titleLabel?.text == "JOIN BGS"{
                print("JOIN BGS Menu clicked")
            }else if cell.titleLabel?.text == "LOGOUT"{
                self.logoutApiCallFunc()
                UserDefaults.standard.setRegisteredStatus(value: true)
            }else if cell.titleLabel?.text == "APP INFO"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.appInfoViewController())
            }
            
            else{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.dashboardViewController())
            }
            break

        case 1:
            if cell.titleLabel?.text == "HOME"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
//                self.moveToController(UIViewController.dashboardViewController())
                let currentVc : UIViewController = (UIApplication.shared.delegate?.window??.rootViewController as! UINavigationController).topViewController!
                if !(currentVc.isKind(of: DashboardViewController.self))   {
                    self.moveToController(UIViewController.dashboardViewController())
                }

            }else if cell.titleLabel?.text == "PROFILE"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.profileDashboardViewController())
            } else if cell.titleLabel?.text == "EVENTS"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.allEventsListViewController())
            } else if cell.titleLabel?.text == "JOIN BGS"{
                print("JOIN BGS Menu clicked")
            }else if cell.titleLabel?.text == "LOGOUT"{
                self.logoutApiCallFunc()
                UserDefaults.standard.setRegisteredStatus(value: true)
            }else if cell.titleLabel?.text == "APP INFO"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.appInfoViewController())
            }
            break

        case 2 :
            if cell.titleLabel?.text == "HOME"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
//                self.moveToController(UIViewController.dashboardViewController())
                let currentVc : UIViewController = (UIApplication.shared.delegate?.window??.rootViewController as! UINavigationController).topViewController!
                if !(currentVc.isKind(of: DashboardViewController.self))   {
                    self.moveToController(UIViewController.dashboardViewController())
                }

            }else if cell.titleLabel?.text == "PROFILE"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.profileDashboardViewController())
            } else if cell.titleLabel?.text == "EVENTS"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.allEventsListViewController())
            } else if cell.titleLabel?.text == "JOIN BGS"{
                print("JOIN BGS Menu clicked")
            }else if cell.titleLabel?.text == "LOGOUT"{
                self.logoutApiCallFunc()
                UserDefaults.standard.setRegisteredStatus(value: true)
            }else if cell.titleLabel?.text == "APP INFO"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.appInfoViewController())
            }
            break

        case 3 :
            if cell.titleLabel?.text == "HOME"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
//                self.moveToController(UIViewController.dashboardViewController())
                let currentVc : UIViewController = (UIApplication.shared.delegate?.window??.rootViewController as! UINavigationController).topViewController!
                if !(currentVc.isKind(of: DashboardViewController.self))   {
                    self.moveToController(UIViewController.dashboardViewController())
                }

            }else if cell.titleLabel?.text == "PROFILE"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.profileDashboardViewController())
            } else if cell.titleLabel?.text == "EVENTS"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.allEventsListViewController())
            } else if cell.titleLabel?.text == "JOIN BGS"{
                print("JOIN BGS Menu clicked")
            }else if cell.titleLabel?.text == "LOGOUT"{
                self.logoutApiCallFunc()
                UserDefaults.standard.setRegisteredStatus(value: true)
            }else if cell.titleLabel?.text == "APP INFO"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.appInfoViewController())
            }
            
            break
            
        case 4:
            if cell.titleLabel?.text == "HOME"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
//                self.moveToController(UIViewController.dashboardViewController())
                let currentVc : UIViewController = (UIApplication.shared.delegate?.window??.rootViewController as! UINavigationController).topViewController!
                if !(currentVc.isKind(of: DashboardViewController.self))   {
                    self.moveToController(UIViewController.dashboardViewController())
                }

            }else if cell.titleLabel?.text == "PROFILE"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.profileDashboardViewController())
            } else if cell.titleLabel?.text == "EVENTS"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.allEventsListViewController())
            } else if cell.titleLabel?.text == "JOIN BGS"{
                print("JOIN BGS Menu clicked")
            }else if cell.titleLabel?.text == "LOGOUT"{
                self.logoutApiCallFunc()
                UserDefaults.standard.setRegisteredStatus(value: true)
            }else if cell.titleLabel?.text == "APP INFO"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.appInfoViewController())
            }
            break
            
        case 5:
            if cell.titleLabel?.text == "HOME"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
//                self.moveToController(UIViewController.dashboardViewController())
                let currentVc : UIViewController = (UIApplication.shared.delegate?.window??.rootViewController as! UINavigationController).topViewController!
                if !(currentVc.isKind(of: DashboardViewController.self))   {
                    self.moveToController(UIViewController.dashboardViewController())
                }

            }else if cell.titleLabel?.text == "PROFILE"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.profileDashboardViewController())
            } else if cell.titleLabel?.text == "EVENTS"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.allEventsListViewController())
            } else if cell.titleLabel?.text == "JOIN BGS"{
                print("JOIN BGS Menu clicked")
            }else if cell.titleLabel?.text == "LOGOUT"{
                self.logoutApiCallFunc()
                UserDefaults.standard.setRegisteredStatus(value: true)
            }else if cell.titleLabel?.text == "APP INFO"{
                self.currentVC?.navigationController?.navigationBar.isHidden = true
                self.moveToController(UIViewController.appInfoViewController())
            }
            break

        default:
            print("Push to viewControllers")
        }

    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        cell.backgroundColor = .white
        cell.indicatorView?.backgroundColor = UIColor.white
        cell.titleLabel?.textColor = UIColor.darkGray

    }
    
}
