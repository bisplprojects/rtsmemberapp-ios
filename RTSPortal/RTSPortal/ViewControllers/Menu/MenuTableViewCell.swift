//
//  MenuTableViewCell.swift
//  RTSPortal
//
//  Created by sajan on 11/09/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    var iconImageView:UIImageView?
    var indicatorView:UIView?
    var titleLabel:UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
