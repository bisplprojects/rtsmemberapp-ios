//
//  PaymentViewController.swift
//  RTSPortal
//
//  Created by sajan on 21/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Stripe
import Toast_Swift

class PaymentViewController: UIViewController,STPPaymentCardTextFieldDelegate,STPAuthenticationContext {
    
    var currentVC:UIViewController?

    @IBOutlet weak var cardContainerView: UIView!
    @IBOutlet weak var payBtn: UIButton!
    
    @IBOutlet weak var totalAmountLbl: UILabel!
    
    let paymentCardTextField = STPPaymentCardTextField()

    var eventDetails : Event?

    var eventRegistrationDetails : EventRegister?
    var selectedPriceOptionsStringArray:[String]?
    var totalAmount:Double?

    var initentKey:String = ""
    var transactionId:String?
    var isPaymentSuccessful:Bool?
    
    var participantId : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.currentVC = self

        let frame:CGRect = UIScreen.main.bounds
        let screenWidth = frame.size.width;

        // Do any additional setup after loading the view.
        // Setup payment card text field
        paymentCardTextField.delegate = self
        
        // Add payment card text field to view
        self.paymentCardTextField.frame = CGRect(x: 20, y: 150, width: screenWidth - 40, height: 50)
        self.view.addSubview(paymentCardTextField)
       
        self.payBtn.isEnabled = false
        
        let currencySymbol = getSymbol(forCurrencyCode: "GBP")

        let totalAmount = String(format: "%.2f", self.totalAmount!)
        self.totalAmountLbl.text = currencySymbol! + " " + totalAmount
    }
    
    // MARK: - IBActions

    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionPayBtnTapped(_ sender: Any) {
        print("Validated")
        self.view.endEditing(true)
        let cardParams = paymentCardTextField.cardParams

        self.getPaymentIntentApiCallFunc(cardParams)
    }
    
    func getSymbol(forCurrencyCode code: String) -> String? {
        let locale = NSLocale(localeIdentifier: code)
        if locale.displayName(forKey: .currencySymbol, value: code) == code {
            let newlocale = NSLocale(localeIdentifier: code.dropLast() + "_en")
            return newlocale.displayName(forKey: .currencySymbol, value: code)
        }
        return locale.displayName(forKey: .currencySymbol, value: code)
    }

    // MARK: STPPaymentCardTextFieldDelegate
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        // Toggle buy button state
        self.payBtn.isEnabled = textField.isValid
    }

    func authenticationPresentingViewController() -> UIViewController {
        return self
    }

    // MARK: - Webservice
    
    func getPaymentIntentApiCallFunc(_ cardParams:STPPaymentMethodCardParams) {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        
        let totalAmount:String  = String(self.totalAmount!)
        
        let param = ["entity" : "CiviMobilePayment",
                     "action" : "createintent",
                     "token" : tokenString,
                     "amount" : totalAmount,
                     "currency" : "gbp",
                     "scope" : "event",
                     "cid" :  dict["ContactId"]];
        print("Payment Intent request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Payment Intent response : \(json)")
            if status
            {
                
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let msg = dataDict["message"]?.stringValue
                        
                        if dataDict["status"] == true{
                            let intentDetail = dataDict["payment_intent"]?.dictionaryValue
                            self.initentKey = intentDetail!["client_secret"]!.stringValue
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                // TO DO : Display toast msg
                                self.view.makeToast("Payment initiated...", duration: 1.0, position: .bottom)
                                self.makePaymentCallFunc(cardParams)
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                // TO DO : Display toast msg
                                self.view.makeToast(msg, duration: 3.0, position: .bottom)
                            }
                        }
                    }
                    else{
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast("Cannot make payment", duration: 3.0, position: .bottom)
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    func makePaymentCallFunc(_ cardParams:STPPaymentMethodCardParams)  {
        
        SVProgressHUD.show()

        self.isPaymentSuccessful = false

        var cardDetailParams = STPPaymentMethodCardParams()
        let billingDetails = STPPaymentMethodBillingDetails()
        // Fill in card, billing details
        
//        cardParams.number = "4242424242424242"
//        cardParams.expMonth = 11
//        cardParams.expYear = 21
//        cardParams.cvc = "123"
        
        cardDetailParams = cardParams
        let paymentMethodParams = STPPaymentMethodParams(card: cardDetailParams, billingDetails: billingDetails, metadata: nil)
        
        let paymentIntentParams = STPPaymentIntentParams(clientSecret: self.initentKey)
        paymentIntentParams.paymentMethodParams = paymentMethodParams
        
        let paymentManager = STPPaymentHandler.shared()
        
        paymentManager.confirmPayment(paymentIntentParams, with:self, completion: { (status, paymentIntent, error) in
            switch (status){
            case .failed:
                // Handle error
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    print("inside failed payment status")
                    self.view.makeToast("Payment failed", duration: 2.0, position: .bottom)
                    self.transactionId = ""
                    self.isPaymentSuccessful = false
                    self.eventRegistrationApiCallFunc()
                }
                break
            case .canceled:
                // Handle cancel
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                break
            case .succeeded:
                // Payment Intent is confirmed
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.view.makeToast("Payment successful", duration: 2.0, position: .bottom)
                    self.transactionId = paymentIntent?.stripeId
                    self.isPaymentSuccessful = true
                    self.eventRegistrationApiCallFunc()
                }
                break
                
            @unknown default:
                fatalError()
            }
        })
    }

    func eventRegistrationApiCallFunc() {
        self.payBtn.isUserInteractionEnabled = false

        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        
        
        let totalAmount:String  = String(self.totalAmount!)
        let priceOptionString = convertIntoJSONString(arrayObject: self.selectedPriceOptionsStringArray!)
        
        let fields = ["email-Primary": self.eventRegistrationDetails?.primaryEmail,
                      "email-5": self.eventRegistrationDetails?.billingEmail,
                      "formal_title": self.eventRegistrationDetails?.title,
                      "first_name": self.eventRegistrationDetails?.firstName,
                      "last_name": self.eventRegistrationDetails?.lastName,
                      "job_title": self.eventRegistrationDetails?.jobTitle,
                      "custom_213": self.eventRegistrationDetails?.placeOfWork,
                      "gender_id": self.eventRegistrationDetails?.gender,
                      "custom_107": self.eventRegistrationDetails?.profession,
                      "phone-Primary-2": self.eventRegistrationDetails?.mobile,
                      "url-11": self.eventRegistrationDetails?.twitterName,
                      "custom_324": self.eventRegistrationDetails?.dietaryRequirement,
                      "custom_404": self.eventRegistrationDetails?.otherDietaryRequirement,
                      "custom_326": self.eventRegistrationDetails?.accessRequirement,
                      "custom_485": self.eventRegistrationDetails?.familyFriendlyRoomAccess,
                      "custom_354": self.eventRegistrationDetails?.volunteerChairSession,
                      "custom_403": self.eventRegistrationDetails?.joinDelegate,
                      "custom_221": self.eventRegistrationDetails?.biography,
                      "image_URL": self.eventRegistrationDetails?.picture];
        
        let fieldsString = JSONStringify(value: fields as! [String : String])
        
        let isPaymentSuccessful = (self.isPaymentSuccessful == true) ? 1 : 0
        
        let param = ["entity" : "CiviMobileParticipant",
                     "action" : "registration",
                     "token" : tokenString,
                     "event_id" : self.eventRegistrationDetails?.eventId as Any,
                     "cid" :  dict["ContactId"]!,
                     "price_options" : priceOptionString as Any,
                     "is_paylater" : 0,
                     "payment_successful" : isPaymentSuccessful,
                     "trxn_id" : self.transactionId as Any,
                     "total_amount" : totalAmount,
                     "debug" : "1",
                     "fields" : fieldsString] as [String : Any];
        
        print("Events Registration request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Events Registration response : \(json)")
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    let dataDictonary = dictonary["data"]
                    
                    let msg = dataDictonary["message"].stringValue
                    if dataDictonary["status"] == true{
                        
                        let dataDict = dataDictonary["event_registration"].dictionaryValue

                        self.participantId = dataDict["pid"]?.stringValue
                        
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            // Display toast msg
                            let style = ToastStyle()
                            self.view?.makeToast(msg, duration: 2.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                                
                                self.payBtn.isUserInteractionEnabled = true
                                // Move to Event Details
                                self.moveToEventDetailsVC(self.eventDetails!.eventId!)
                            })
                        }
                    }
                    else{
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            let style = ToastStyle()
                            self.view?.makeToast(msg, duration: 2.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                                self.payBtn.isUserInteractionEnabled = true
                                self.moveToEventListVC()

                            })
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast("Event registraion failed", duration: 3.0, position: .center)
                        self.payBtn.isUserInteractionEnabled = true
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.payBtn.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    func moveToParticipantDetailsVC(_ participantId:String) {
        var viewControllers = ( self.currentVC?.navigationController?.viewControllers)!
        let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
        let myEventDetailVC = storyboard.instantiateViewController(withIdentifier: "MyEventDetailViewController") as! MyEventDetailViewController
        let event:Event = Event()
        event.participantId = participantId
        myEventDetailVC.event = event
    self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1],myEventDetailVC], animated: true)

    }
    
    func moveToEventDetailsVC(_ evendId:String) {
        var viewControllers = ( self.currentVC?.navigationController?.viewControllers)!
        
        let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
        let eventDetailsVC = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
        eventDetailsVC.eventId = evendId
    self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1],eventDetailsVC], animated: true)
        
    }

    func moveToEventListVC() {
        var viewControllers = ( self.currentVC?.navigationController?.viewControllers)!
        self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1]], animated: true)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
