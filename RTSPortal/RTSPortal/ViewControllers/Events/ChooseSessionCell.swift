//
//  ChooseSessionCell.swift
//  RTSPortal
//
//  Created by sajan on 01/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

class ChooseSessionCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var radioImgView: UIImageView!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
