//
//  AllEventListViewController.swift
//  RTSPortal
//
//  Created by sajan on 26/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD
import MapKit
import IQKeyboardManagerSwift

class AllEventListViewController: UIViewController,UISearchResultsUpdating{
    
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var allEventTableView: UITableView!
    
    let cellIdentifier = "AllEventListCell"
    var searchController:UISearchController!

    let locationManager:CLLocationManager = CLLocationManager()

    var allEventsList:[Event] = []
    var allEventsListFilter:[Event] = []
    
    var latitude:String?
    var longitude:String?

    // MARK: -

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.sizeToFit()
        definesPresentationContext = true
        searchController.searchBar.placeholder = "Search"
        searchController.searchBar.barTintColor = UIColor.white
        self.searchContainerView.addSubview(searchController.searchBar)
        
        let lineView = UIView(frame: CGRect(x: 0, y: searchController.searchBar.frame.height-1, width: view.bounds.width, height: 1))
        lineView.backgroundColor = UIColor.white
        searchController.searchBar.addSubview(lineView)

        self.searchContainerView.layer.cornerRadius = 5
        self.searchContainerView.layer.shadowColor = UIColor.gray.cgColor
        self.searchContainerView.layer.shadowOpacity = 0.6
        self.searchContainerView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        self.searchContainerView.layer.shadowRadius = 4

        self.loadAllEventListApiCallFunc()
    }
    
    // MARK: - IBActions

    @IBAction func actionMenuBtnTapped(_ sender: Any) {
        IQKeyboardManager.shared.resignFirstResponder()
        UserActionManager.displayLeftMenu()
    }
    
    @objc func actionDirectionBtnTapped(_ sender: UIButton) {
        
        let btn = sender
        if let tempPostCode = self.allEventsListFilter[btn.tag].postalCode{
            if tempPostCode != ""{
                self.getLatLngForZip(zipCode: self.allEventsListFilter[btn.tag].postalCode!)
                
                if (self.latitude != nil || self.longitude != nil){
                    self.openGoogleDirectionMap(self.latitude!, self.longitude!)
                }else{
                    self.view.makeToast("Direction not found", duration: 3.0, position: .center)
                }
            }
            else
            {
                self.view.makeToast("Direction not found", duration: 3.0, position: .center)
            }
        }
    }

    func getLatLngForZip(zipCode: String) {
        
        let zipString = zipCode.removingWhitespaces()
        let baseUrl = kGoogleMapsBaseUrl
        let apikey = kGoogleMapsAPIKey
        
        let url = NSURL(string: "\(baseUrl)address=\(zipString)&key=\(apikey)")
        let data = NSData(contentsOf: url! as URL)
        if let json = try? JSON(data: data! as Data) {
            
            if let result = json["results"].array {
                if result.count > 0{
                    if let geometry = result[0]["geometry"].dictionary {
                        if let location = geometry["location"]?.dictionary {
                            let latitude = location["lat"]?.stringValue
                            let longitude = location["lng"]?.stringValue
                            self.latitude = latitude
                            self.longitude = longitude
                            print("\n\(String(describing: latitude)), \(String(describing: longitude))")
                        }
                    }
                }
            }
        }
    }

    func openGoogleDirectionMap(_ destinationLat: String, _ destinationLng: String) {
        
        let LocationManager = CLLocationManager()
        
        if let myLat = LocationManager.location?.coordinate.latitude, let myLng = LocationManager.location?.coordinate.longitude {
            
            if let tempURL = URL(string: "comgooglemaps://?saddr=&daddr=\(destinationLat),\(destinationLng)&directionsmode=driving") {
                
                UIApplication.shared.open(tempURL, options: [:], completionHandler: { (isSuccess) in
                    
                    if !isSuccess {
                        
                        if UIApplication.shared.canOpenURL(URL(string: "https://www.google.co.th/maps/dir///")!) {
                            
                            UIApplication.shared.open(URL(string: "https://www.google.co.th/maps/dir/\(myLat),\(myLng)/\(destinationLat),\(destinationLng)/")!, options: [:], completionHandler: nil)
                            
                        } else {
                            print("Can't open URL.")
                        }
                    }
                })
                
            } else {
                print("Can't open GoogleMap Application.")
            }
            
        } else {
            print("Please allow permission")
        }
    }

    // MARK: - Webservice
    
    func loadAllEventListApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileEvent",
                     "action" : "allevents",
                     "token" : tokenString];
        print("All Events request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("All Events List response : \(json)")
            
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let myevents = dataDict["allevents"]?.arrayValue
                        if myevents!.count > 0 {
                            
                            let responseArray = myevents
                            
                            for (_, eventDict) in (responseArray?.enumerated())! {
                                let eventObj = Event(allEventdict: eventDict.dictionary!)
                                self.allEventsList.append(eventObj)
                            }
                            self.allEventsListFilter = self.allEventsList
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.allEventTableView.reloadData()
                                if self.allEventsListFilter.count == 0 {
                                    self.allEventTableView.setEmptyMessage("Nothing found")
                                } else {
                                    self.allEventTableView.restore()
                                }
                            }
                        }
                        else {
                            //Display msg
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast("No events found", duration: 3.0, position: .center)
                            }
                        }
                    }
                    else
                    {
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                else
                {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
        
    }

    
    // MARK: - UISearch result update delegate
    
    func updateSearchResults(for searchController: UISearchController) {
        
        if let searchText = self.searchController.searchBar.text {

            var resultArr:[Event] = []
            
            var arr2 = searchText.components(separatedBy: .whitespaces)
            arr2 = arr2.filter({ $0 != ""})
            
            let arr1 = self.allEventsList
            
            for (_, str1) in (arr1.enumerated()) {
                var counter = 0
                for (_, str2) in (arr2.enumerated()) {
                    
                    if ((str1.eventTitle?.lowercased().contains(string: str2.lowercased()))!){
                        counter += 1
                        if counter == arr2.count{
                            resultArr.append(str1)
                        }
                    }
                }
            }
            
            self.allEventsListFilter = searchText.trimmingCharacters(in: .whitespaces).isEmpty ? self.allEventsList : resultArr
            
            self.allEventTableView.reloadData()
            if self.allEventsListFilter.count == 0 {
                self.allEventTableView.setEmptyMessage("Nothing found")
            } else {
                self.allEventTableView.restore()
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AllEventListViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allEventsListFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let allEventCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! AllEventListCell
        
        allEventCell.containerView.layer.cornerRadius = 5
        allEventCell.containerView.layer.shadowColor = UIColor.lightGray.cgColor
        allEventCell.containerView.layer.shadowOpacity = 0.3
        allEventCell.containerView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        allEventCell.containerView.layer.shadowRadius = 4
        
        let event:Event = self.allEventsListFilter[indexPath.row]
        
        allEventCell.titleLbl.text = event.eventTitle
        
        let startDate = event.eventStartDate!.toDate()!
        let startDateString = startDate.toString()
        allEventCell.dateLbl.text = startDateString
        
        if (event.postalCode == nil || event.postalCode == "") {
            allEventCell.directionBtn.isHidden = true
            allEventCell.arrowImgView.isHidden = true
        }else{
            allEventCell.directionBtn.isHidden = false
            allEventCell.arrowImgView.isHidden = false
        }
        
        allEventCell.directionBtn.tag = indexPath.row
        allEventCell.directionBtn.addTarget(self, action:#selector(actionDirectionBtnTapped(_:)), for: .touchUpInside)
        
        return allEventCell

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        IQKeyboardManager.shared.resignFirstResponder()
        let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
        let eventDetailsVC = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
        eventDetailsVC.eventId = self.allEventsListFilter[indexPath.row].eventId
        self.navigationController?.pushViewController(eventDetailsVC, animated: true)
    }
}
