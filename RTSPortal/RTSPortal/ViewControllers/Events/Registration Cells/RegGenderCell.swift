//
//  RegGenderCell.swift
//  RTSPortal
//
//  Created by sajan on 06/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

class RegGenderCell: UITableViewCell {

    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var noneBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
