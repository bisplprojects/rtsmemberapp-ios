//
//  PaymentModeViewController.swift
//  RTSPortal
//
//  Created by sajan on 19/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Stripe
import Toast_Swift
import IQKeyboardManagerSwift

class PaymentModeViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIScrollViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryPickerArray.count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countryPickerArray[row]
    }
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        countryText.text = countryPickerArray[row]
    }
    
    var currentVC:UIViewController?

    @IBOutlet weak var cardPaymentBtn: UIButton!
    @IBOutlet weak var payByInvoiceBtn: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet var billingScrollView: UIScrollView!
    
    
    @IBOutlet var billingTitleLbl: UILabel!
    
    
    @IBOutlet var billingFnameLbl: UILabel!
    
    @IBOutlet var billingMnameLbl: UILabel!
    
    @IBOutlet var billingMnameText: UITextField!
    
    @IBOutlet var billingFnameText: UITextField!
    
    @IBOutlet var billingLnameText: UITextField!
    
    
    @IBOutlet var billingLnameLbl: UILabel!
    
    @IBOutlet var billingAddressLbl: UILabel!
    
    @IBOutlet var billingAddressText: UITextField!
    
    
    @IBOutlet var cityLbl: UILabel!
    
    @IBOutlet var cityText: UITextField!
    
    @IBOutlet var postCodeLbl: UILabel!
    
    @IBOutlet var postCodeText: UITextField!
    
    @IBOutlet var countryLbl: UILabel!
    
    @IBOutlet var countryText: UITextField!
    var activeField:UITextField?
    var lastOffset: CGPoint!
    var keyboardHeight: CGFloat!
    var constraintContentHeight: NSLayoutConstraint!
    
    var eventDetails : Event?

    var eventRegistrationDetails : EventRegister?
    var selectedPriceOptionsStringArray:[String] = []
    var totalAmount:Double = 0

    var registrationParams:Any?
    
    var participantId : String?

    var initentKey:String = ""
    var countryPickerArray: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
        self.currentVC = self
        self.payByInvoiceBtn.isHidden = (self.eventDetails?.isPayLater == "0") ? true : false
        self.billingTitleLbl.isHidden = true
        self.billingFnameLbl.isHidden = true
        self.billingFnameText.isHidden = true
        self.billingLnameLbl.isHidden = true
        self.billingLnameText.isHidden = true
        self.billingAddressLbl.isHidden = true
        self.billingAddressText.isHidden = true
        self.postCodeLbl.isHidden = true
        self.postCodeText.isHidden = true
        self.countryLbl.isHidden = true
        self.countryText.isHidden = true
        self.cityLbl.isHidden = true
        self.cityText.isHidden = true
        self.billingMnameText.isHidden = true
        self.billingMnameLbl.isHidden = true
        self.billingScrollView.showsHorizontalScrollIndicator = false
        self.billingScrollView.isDirectionalLockEnabled = true
        
    
        self.billingScrollView.delegate = self
        
        self.billingFnameText.delegate = self
        self.billingLnameText.delegate = self
        self.billingMnameText.delegate = self
        self.billingAddressText.delegate = self
        self.cityText.delegate = self
        self.postCodeText.delegate = self
        self.countryText.delegate = self
        
        // Observe keyboard change
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // Add touch gesture for scrollview
        self.billingScrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(returnTextView(gesture:))))
    
constraintContentHeight = self.billingScrollView.heightAnchor.constraint(equalToConstant: 363)
        print ("scrollview height")
        print(constraintContentHeight)
        
        let thePicker = UIPickerView()
        countryText.inputView = thePicker
        
        let lc = NSLocale.current
        countryPickerArray = Locale.counrtyNames(for: lc)
        thePicker.delegate = self
       
    }
    @objc func returnTextView(gesture: UIGestureRecognizer) {
        guard activeField != nil else {
            return
        }
        
        activeField?.resignFirstResponder()
        activeField = nil
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
            scrollView.contentOffset.x = 0
        }
        //IQKeyboardManager.shared.resignFirstResponder()
    }
    
   
    

    //array of countries:
    
    
    // MARK: - Webservice
    
    func eventRegistrationApiCallFunc() {
        self.submitBtn.isUserInteractionEnabled = false

        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        
        let totalAmount:String  = String(self.totalAmount)
        let priceOptionString = convertIntoJSONString(arrayObject: self.selectedPriceOptionsStringArray)
        
        let fields = ["email-Primary": self.eventRegistrationDetails?.primaryEmail,
                      "email-5": self.eventRegistrationDetails?.billingEmail,
                      "formal_title": self.eventRegistrationDetails?.title,
                      "first_name": self.eventRegistrationDetails?.firstName,
                      "last_name": self.eventRegistrationDetails?.lastName,
                      "job_title": self.eventRegistrationDetails?.jobTitle,
                      "custom_213": self.eventRegistrationDetails?.placeOfWork,
                      "gender_id": self.eventRegistrationDetails?.gender,
                      "custom_107": self.eventRegistrationDetails?.profession,
                      "phone-Primary-2": self.eventRegistrationDetails?.mobile,
                      "url-11": self.eventRegistrationDetails?.twitterName,
                      "custom_324": self.eventRegistrationDetails?.dietaryRequirement,
                      "custom_404": self.eventRegistrationDetails?.otherDietaryRequirement,
                      "custom_326": self.eventRegistrationDetails?.accessRequirement,
                      "custom_485": self.eventRegistrationDetails?.familyFriendlyRoomAccess,
                      "custom_354": self.eventRegistrationDetails?.volunteerChairSession,
                      "custom_403": self.eventRegistrationDetails?.joinDelegate,
                      "custom_221": self.eventRegistrationDetails?.biography,
                      "image_URL": self.eventRegistrationDetails?.picture];
        
        let fieldsString = JSONStringify(value: fields as! [String : String])
        
        let isPayLater = (self.payByInvoiceBtn.isSelected) ? 1 : 0
        
        print("biiling first name is: ", billingFnameText.text!)
        
        let billingField = ["billing_first_name": billingFnameText.text!,
                            "billing_middle_name":billingMnameText.text!,
                            "billing_last_name": billingLnameText.text!,
                            "billing_street_address-5": billingAddressText.text!,
                            "billing_city-5": cityText.text!,
                            "billing_postal_code-5": postCodeText.text!,
                            "billing_country_id-5": countryText.text!]
        let billingString = JSONStringify(value: billingField )
        print("billingstring: \(billingString)")
        let param = ["entity" : "CiviMobileParticipant",
                     "action" : "registration",
                     "token" : tokenString,
                     "event_id" : self.eventRegistrationDetails?.eventId as Any,
                     "cid" :  dict["ContactId"]!,
                     "price_options" : priceOptionString as Any,
                     "is_paylater" : isPayLater,
                     "payment_successful" : 0,
                     "trxn_id" : "",
                     "total_amount" : totalAmount,
                     "debug" : "1",
                     "fields" : fieldsString,
                     "billing_fields": billingString
            ] as [String : Any];

        print("Events Registration request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Events Registration response : \(json)")
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    let dataDictonary = dictonary["data"]
                    
                    let msg = dataDictonary["message"].stringValue
                    if dataDictonary["status"] == true{
                        
                        let dataDict = dataDictonary["event_registration"].dictionaryValue
                        self.participantId = dataDict["pid"]?.stringValue

                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            //  Display toast msg
                            let style = ToastStyle()
                            self.view?.makeToast(msg, duration: 2.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                                self.submitBtn.isUserInteractionEnabled = true
                                // Move to Event Details
                                self.moveToEventDetailsVC(self.eventDetails!.eventId!)
                            })
                        }
                    }
                    else{
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            let style = ToastStyle()
                            self.view?.makeToast(msg, duration: 2.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                                self.submitBtn.isUserInteractionEnabled = true
                                self.moveToEventListVC()
                            })
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast("Event registraion failed", duration: 3.0, position: .center)
                        self.submitBtn.isUserInteractionEnabled = true
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.submitBtn.isUserInteractionEnabled = true
                }
            }
        }
    }

    // MARK: - IBAction

    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionCardPaymentBtnTapped(_ sender: Any) {
        self.cardPaymentBtn.isSelected = true
        self.payByInvoiceBtn.isSelected = false
        
        self.billingTitleLbl.isHidden = true
        self.billingFnameLbl.isHidden = true
        self.billingFnameText.isHidden = true
        self.billingLnameLbl.isHidden = true
        self.billingLnameText.isHidden = true
        self.billingAddressLbl.isHidden = true
        self.billingAddressText.isHidden = true
        self.postCodeLbl.isHidden = true
        self.postCodeText.isHidden = true
        self.countryLbl.isHidden = true
        self.countryText.isHidden = true
        self.cityLbl.isHidden = true
        self.cityText.isHidden = true
        self.cardPaymentBtn.setBackgroundImage(UIImage(imageLiteralResourceName: "blue_btn_background_icon"), for: .selected)
        self.payByInvoiceBtn.setBackgroundImage(UIImage(imageLiteralResourceName: "border_btn_background_icon"), for: .normal)
        
    }
    
    @IBAction func actionPayByInvoiceBtnTapped(_ sender: Any) {
        self.cardPaymentBtn.isSelected = false
        self.payByInvoiceBtn.isSelected = true
        
            
            self.billingTitleLbl.isHidden = false
            self.billingFnameLbl.isHidden = false
            self.billingFnameText.isHidden = false
            self.billingLnameLbl.isHidden = false
            self.billingLnameText.isHidden = false
            self.billingAddressLbl.isHidden = false
            self.billingAddressText.isHidden = false
            self.postCodeLbl.isHidden = false
            self.postCodeText.isHidden = false
            self.countryLbl.isHidden = false
            self.countryText.isHidden = false
            self.cityLbl.isHidden = false
            self.cityText.isHidden = false
        self.billingMnameText.isHidden = false
        self.billingMnameLbl.isHidden = false
        self.cardPaymentBtn.setBackgroundImage(UIImage(imageLiteralResourceName: "border_btn_background_icon"), for: .normal)
        self.payByInvoiceBtn.setBackgroundImage(UIImage(imageLiteralResourceName: "blue_btn_background_icon"), for: .selected)

    }
    
    @IBAction func actionSubmitBtnTapped(_ sender: Any) {
        if (!self.cardPaymentBtn.isSelected && !self.payByInvoiceBtn.isSelected){
            self.view.makeToast("Please choose payment method", duration: 3.0, position: .center)
        }
        else if (self.payByInvoiceBtn.isSelected){
            if(self.billingFnameText.text != "" && self.billingLnameText.text !=  "" &&
                self.billingAddressText.text != "" && self.postCodeText.text != "" && self.countryText.text != ""){ self.eventRegistrationApiCallFunc()
            }
            else{
                if self.billingFnameText.text == "" {
                    
                    self.view.makeToast("Please fill the First Name", duration: 3.0, position: .center)
                    self.billingFnameText.becomeFirstResponder()
                }
                else if self.billingLnameText.text == "" {
                    self.view.makeToast("Please fill in Last Name", duration: 3.0, position: .center)
                    self.billingLnameText.becomeFirstResponder()
                }
                else if self.billingAddressText.text == "" {
                    self.view.makeToast("Please fill in Street Address", duration: 3.0, position: .center)
                    self.billingAddressText.becomeFirstResponder()
                }
                else if self.cityText.text == "" {
                    self.view.makeToast("Please fill in City", duration: 3.0, position: .center)
                    self.cityText.becomeFirstResponder()
                }
                else if self.postCodeText.text == "" {
                    self.view.makeToast("Please fill in Post Code", duration: 3.0, position: .center)
                    self.postCodeText.becomeFirstResponder()
                }
                else if self.countryText.text == "" {
                    self.view.makeToast("Please Select Country", duration: 3.0, position: .center)
                }
                else{
                self.view.makeToast("Please fill in all Mandatory Billing Details marked with *", duration: 3.0, position: .center)
                }
            }
        }
        else if (self.cardPaymentBtn.isSelected){
            self.moveToPaymentViewController()
        }
    }
    
    func moveToPaymentViewController()  {
        let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
        let paymentVC = storyboard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        paymentVC.eventRegistrationDetails = self.eventRegistrationDetails
        paymentVC.totalAmount = self.totalAmount
        paymentVC.selectedPriceOptionsStringArray = self.selectedPriceOptionsStringArray
        var eventDetail:Event = Event()
        eventDetail = self.eventDetails!
        paymentVC.eventDetails = eventDetail
        self.navigationController?.pushViewController(paymentVC, animated: true)
    }
    
    func moveToParticipantDetailsVC(_ participantId:String) {
        var viewControllers = ( self.currentVC?.navigationController?.viewControllers)!
        let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
        let myEventDetailVC = storyboard.instantiateViewController(withIdentifier: "MyEventDetailViewController") as! MyEventDetailViewController
        let event:Event = Event()
        event.participantId = participantId
        myEventDetailVC.event = event
    self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1],myEventDetailVC], animated: true)
        
    }

    func moveToEventDetailsVC(_ evendId:String) {
        var viewControllers = ( self.currentVC?.navigationController?.viewControllers)!
        
        let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
        let eventDetailsVC = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
        eventDetailsVC.eventId = evendId
    self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1],eventDetailsVC], animated: true)
        
    }
    
    func moveToEventListVC() {
        var viewControllers = ( self.currentVC?.navigationController?.viewControllers)!
        self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1]], animated: true)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension Locale {
    
    public var counrtyNames: [String] {
        return Locale.counrtyNames(for: self)
    }
    
    public static func counrtyNames(for locale: Locale) -> [String] {
        let nsLocale = locale as NSLocale
        let result: [String] = NSLocale.isoCountryCodes.compactMap {
            return nsLocale.displayName(forKey: .countryCode, value: $0)
        }
        // Seems `isoCountryCodes` already sorted. So, we skip sorting.
        return result
    }
}

//MARK: - Observers
// MARK: UITextFieldDelegate
extension PaymentModeViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField
        lastOffset = self.billingScrollView.contentOffset
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeField?.resignFirstResponder()
        activeField = nil
        return true
    }
}

// MARK: Keyboard Handling
extension PaymentModeViewController {
    @objc func keyboardWillShow(notification: NSNotification) {
        if keyboardHeight != nil {
            return
        }
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.height
            
            // so increase contentView's height by keyboard height
            UIScrollView.animate(withDuration: 0.3, animations: {
            //self.constraintContentHeight.constant += self.keyboardHeight
                self.billingScrollView.frame.size.height += self.keyboardHeight
                
            })
            
            
            
           
            
            // move if keyboard hide input field
            let distanceToBottom = self.billingScrollView.frame.size.height - (activeField?.frame.origin.y)! - (activeField?.frame.size.height)!
            let collapseSpace = keyboardHeight - distanceToBottom
            
            
            if collapseSpace < 0 {
                // no collapse
                return
            }
            
            // set new offset for scroll view
            UIScrollView.animate(withDuration: 0.3, animations: {
                // scroll to the position above keyboard 10 points
                self.billingScrollView.contentOffset = CGPoint(x: self.lastOffset.x, y: collapseSpace + 10)
            })
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIScrollView.animate(withDuration: 0.3) {
            //self.constraintContentHeight.constant -= self.keyboardHeight
          //  self.billingScrollView.frame.size.height -= self.keyboardHeight
            
            self.billingScrollView.contentOffset = self.lastOffset
        }
        
        keyboardHeight = nil
    }
}



