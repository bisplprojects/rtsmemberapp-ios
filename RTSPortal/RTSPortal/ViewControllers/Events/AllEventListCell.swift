//
//  AllEventListCell.swift
//  RTSPortal
//
//  Created by sajan on 26/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

class AllEventListCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var directionBtn: UIButton!
    @IBOutlet weak var arrowImgView: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
