//
//  VerificationViewController.swift
//  RTSPortal
//
//  Created by sajan on 10/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift

class VerificationViewController: UIViewController {

    var emailAddress:String?
    var titleText:String?
    var displayText:String?
     var userRegistered:String = "1"
    var regCheck:String = ""

    @IBOutlet weak var navigationBarLine: UIView!
    @IBOutlet weak var phoneTxtField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.addShadowToNavigation()
        self.phoneTxtField.clearButtonMode = UITextField.ViewMode.whileEditing

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    // MARK: - IBActions

    @IBAction func actionSendVerificationBtnTapped(_ sender: Any) {
        self.view.endEditing(true)
        print("Send Verification Button Clicked")
        let valid = validateVerificationCredentials()
        if valid {
            print("Valid Fields")
            self.verificationApiCallFunc()
        }
    }
    
    @IBAction func actionBackBtnTapped(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let SignInVC = storyboard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
        
        SignInVC.registeredStatus = self.userRegistered
        self.navigationController?.pushViewController(SignInVC, animated: true)
        //self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Webservice

    func verificationApiCallFunc() {
        self.view.endEditing(true)
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        let phoneText = self.phoneTxtField.text
        let ext = "+44"
        let phoneNumber = ext + phoneText!
        
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        
        let param = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "entity": "CiviMobileContact",
                     "action": "otp",
                     "email": self.emailAddress,
                     "phone": phoneNumber];
        print("Verification request params : \(param)")
        
        apiObj.postData(url:kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Verification response before otpview : \(json)")
            if status
            {
                let dictonary = json["values"].dictionaryValue
                
                if dictonary["already_registered"] == 0 {
                    //self.userRegistered = "0"
                    //let defaults = //UserDefaults.standard
                   // defaults.set("true", forKey: self.regCheck)
                    
                    UserDefaults.standard.setRegisteredStatus(value: true)
                    
                    let otpStr = dictonary["otp"]?.stringValue
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        let otpVC = storyboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                        otpVC.emailAddress = self.emailAddress
                        otpVC.phoneNumber = self.phoneTxtField.text
                        otpVC.otpText = otpStr
                        otpVC.isFromForgotPIN = false
                        self.phoneTxtField.text = ""
                        self.navigationController?.pushViewController(otpVC, animated: true)
                        
                       // let msg = "PIN sent"
                        //var style = ToastStyle()
                       // style.messageColor = UIColor.white
                        /*self.view?.makeToast(msg, duration: 3.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                            let otpVC = storyboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                            otpVC.emailAddress = self.emailAddress
                            otpVC.phoneNumber = self.phoneTxtField.text
                            otpVC.otpText = otpStr
                            self.phoneTxtField.text = ""
                            self.navigationController?.pushViewController(otpVC, animated: true)

                        })*/
                    }
                }
                else if dictonary["already_registered"] == 1{
                    let title = dictonary["title"]?.stringValue
                    let display = dictonary["message"]?.stringValue
                    UserDefaults.standard.setRegisteredStatus(value: true)
                    
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        let optionVC = storyboard.instantiateViewController(withIdentifier: "OptionScreenViewController") as! OptionScreenViewController
                        optionVC.emailAddress = self.emailAddress
                        optionVC.phone = self.phoneTxtField.text
                        optionVC.labelTitle = title
                        optionVC.display = display
                        
                      self.navigationController?.pushViewController(optionVC, animated: true)
                    }
                    //Display msg
                    /*let title = dictonary["title"]?.stringValue
                    let errorMsg = dictonary["message"]?.stringValue
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
//                        self.view.makeToast(errorMsg, duration: 3.0, position: .bottom)
                        var style = ToastStyle()
                        style.messageColor = UIColor.white
                        self.view?.makeToast(errorMsg, duration: 3.0, position: ToastPosition.center, title:title, image:nil, style: style, completion: { (true) in
                        })
                    }*/
                }
                else {
                    //Display msg
                    let title = dictonary["title"]?.stringValue
                    let errorMsg = dictonary["message"]?.stringValue
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        //                        self.view.makeToast(errorMsg, duration: 3.0, position: .bottom)
                        var style = ToastStyle()
                        style.messageColor = UIColor.white
                        self.view?.makeToast(errorMsg, duration: 3.0, position: ToastPosition.center, title:title, image:nil, style: style, completion: { (true) in
                        })
                    }
                }
            }
            
        }
    }

    func validateVerificationCredentials() -> Bool{
        
        if(self.phoneTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            self.view.makeToast("Enter phone number", duration: 3.0, position: .center)
            return false
        }
        else
        {
            let temp = isPhoneValid(self.phoneTxtField.text!)
            if (temp == false)
            {
                self.view.makeToast("Enter valid phone number", duration: 3.0, position: .center)
                return false
            }
        }
        return true
    }
    
    func isPhoneValid(_ phone : String) -> Bool{
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", "[0-9]{8,15}")
        return phoneTest.evaluate(with: phone)
    }

    func addShadowToNavigation() {
        self.navigationBarLine.layer.shadowColor = UIColor.black.cgColor
        self.navigationBarLine.layer.shadowOpacity = 1
        self.navigationBarLine.layer.shadowOffset = .zero
        self.navigationBarLine.layer.shadowRadius = 2
        self.navigationBarLine.layer.shadowPath = UIBezierPath(rect: self.navigationBarLine.bounds).cgPath
        self.navigationBarLine.layer.shouldRasterize = true
        self.navigationBarLine.layer.rasterizationScale = UIScreen.main.scale
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
