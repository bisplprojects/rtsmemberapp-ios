//
//  RepeatPINViewController.swift
//  RTSPortal
//
//  Created by sajan on 06/01/20.
//  Copyright © 2020 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift

protocol RepeatPinDelegate {
    func pinDidNotMatch(isMissMatch:Bool)
}

class RepeatPINViewController: UIViewController,MyTextFieldDelegate {

    func textFieldDidDelete(textField: UITextField) {
        print("delete")
        self.textFieldDidChange(textField: textField)
    }
    
    var repeatPinDelegate : RepeatPinDelegate?
    
    var emailAddress : String?
    var otpText : String?
    var pinString : String?
    var repeatPINString : String?
    var isFromForgotPIN : Bool?
    
    @IBOutlet weak var firstTxtField: PinTextField!
    @IBOutlet weak var secondTxtField: PinTextField!
    @IBOutlet weak var thirdTxtField: PinTextField!
    @IBOutlet weak var fourthTxtField: PinTextField!
    @IBOutlet weak var fifthTxtField: PinTextField!
    @IBOutlet weak var sixthTxtField: PinTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        firstTxtField.useUnderline()
        secondTxtField.useUnderline()
        thirdTxtField.useUnderline()
        fourthTxtField.useUnderline()
        fifthTxtField.useUnderline()
        sixthTxtField.useUnderline()

        firstTxtField.delegate = self
        secondTxtField.delegate = self
        thirdTxtField.delegate = self
        fourthTxtField.delegate = self
        fifthTxtField.delegate = self
        sixthTxtField.delegate = self
        
        firstTxtField.myDelegate = self
        secondTxtField.myDelegate = self
        thirdTxtField.myDelegate = self
        fourthTxtField.myDelegate = self
        fifthTxtField.myDelegate = self
        sixthTxtField.myDelegate = self

    }
    
    // MARK: - IBActions

    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSubmitBtnTapped(_ sender: Any) {
        self.view.endEditing(true)
        let valid = validatePIN()
        if valid {
            if self.isFromForgotPIN == true {
                self.resetPINApiCallFunc()
            }
            else{
                self.signUpApiCallFunc()
            }
        }

    }
    
    func validatePIN() -> Bool {
            self.repeatPINString = "\((firstTxtField?.text)!)\((secondTxtField?.text)!)\((thirdTxtField?.text)!)\((fourthTxtField?.text)!)\((fifthTxtField?.text)!)\((sixthTxtField?.text)!)"

            print(self.repeatPINString as Any)

            if(self.repeatPINString!.trimmingCharacters(in: .whitespaces).isEmpty)
            {
                self.view.makeToast("Enter PIN", duration: 2.0, position: .center)
                return false
            }
            else
            {
                let temp = isPinValid(self.repeatPINString!)
                if (temp == false)
                {
                    self.view.makeToast("Enter valid PIN", duration: 2.0, position: .center)
                    return false
                }
            }
            if(self.repeatPINString != self.pinString)
            {
    //            self.view.makeToast("PIN does not match", duration: 2.0, position: .center)
                
                var style = ToastStyle()
                style.messageColor = UIColor.white
                style.titleFont.withSize(16)
                self.view?.makeToast("PIN does not match", duration: 2.0, position: ToastPosition.center, title:title, image:nil, style: style, completion: { (true) in
                    
                    //                            Move to Create PIN
                    self.repeatPinDelegate?.pinDidNotMatch(isMissMatch: true)
                    self.navigationController?.popViewController(animated: true)

                })
                return false


            }

            return true
    }
    
    func isPinValid(_ pin : String) -> Bool{
        let pinTest = NSPredicate(format: "SELF MATCHES %@", "[0-9]{6,6}")
        return pinTest.evaluate(with: pin)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
                
        // On inputing value to textfield
        if ((textField.text?.characters.count)! < 1  && string.characters.count > 0){
            if(textField == firstTxtField)
            {
                secondTxtField.becomeFirstResponder()
            }
            if(textField == secondTxtField)
            {
                thirdTxtField.becomeFirstResponder()
            }
            if(textField == thirdTxtField)
            {
                fourthTxtField.becomeFirstResponder()
            }
            if(textField == fourthTxtField)
            {
                fifthTxtField.becomeFirstResponder()
            }
            if(textField == fifthTxtField)
            {
                sixthTxtField.becomeFirstResponder()
            }

            textField.text = string
            return false
        }
        else if ((textField.text?.characters.count)! >= 1  && string.characters.count == 0){
            // on deleting value from Textfield
            if(textField == secondTxtField)
            {
                firstTxtField.becomeFirstResponder()
            }
            if(textField == thirdTxtField)
            {
                secondTxtField.becomeFirstResponder()
            }
            if(textField == fourthTxtField)
            {
                thirdTxtField.becomeFirstResponder()
            }
            if(textField == fifthTxtField)
            {
                fourthTxtField.becomeFirstResponder()
            }
            if(textField == sixthTxtField)
            {
                fifthTxtField.becomeFirstResponder()
            }

            textField.text = ""
            return false
        }
        else if ((textField.text?.characters.count)! >= 1  )
        {
            textField.text = string
            self.textFieldDidChange(textField: textField)
            return false
        }
        return true

    }
    
    @objc func textFieldDidChange(textField: UITextField){

        let text = textField.text

        if (text?.utf16.count)! >= 1{
            switch textField{
            case firstTxtField:
                secondTxtField.becomeFirstResponder()
            case secondTxtField:
                thirdTxtField.becomeFirstResponder()
            case thirdTxtField:
                fourthTxtField.becomeFirstResponder()
            case fourthTxtField:
                fifthTxtField.becomeFirstResponder()
            case fifthTxtField:
                sixthTxtField.becomeFirstResponder()
//            case sixthTxtField:
//                sixthTxtField.resignFirstResponder()
            default:
                break
            }
        }else{
        }
        
        if  text?.count == 0 {
            switch textField{
            case firstTxtField:
                firstTxtField.becomeFirstResponder()
            case secondTxtField:
                firstTxtField.becomeFirstResponder()
            case thirdTxtField:
                secondTxtField.becomeFirstResponder()
            case fourthTxtField:
                thirdTxtField.becomeFirstResponder()
            case fifthTxtField:
                fourthTxtField.becomeFirstResponder()
            case sixthTxtField:
                fifthTxtField.becomeFirstResponder()

            default:
                break
            }
        }
        else{
        }

    }
    
    // MARK: - Webservice

    func signUpApiCallFunc() {
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        
        let param = ["deviceId": deviceToken!,
                     "appName": "CiviMemberAPP",
                     "entity" : "CiviMobileContact",
                     "action": "register",
                     "email": self.emailAddress ?? "",
                     "otp" : self.otpText ?? "",
                     "pin" : self.repeatPINString ?? ""] as [String : Any];
        print("Sign Up request params : \(param)")
        
        apiObj.postData(url:kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Sign Up response : \(json)")
            if status
            {
                let dictonary = json["values"].dictionaryValue
                
                if dictonary["status"] == true {
                    
                    let msg = dictonary["message"]?.stringValue
                    let title = dictonary["title"]?.stringValue
                    
                    UserDefaults.standard.setTouchIDEnabled(value: false)

                    DispatchQueue.main.async {
                        print("Sign Up success")
                        SVProgressHUD.dismiss()
                        var style = ToastStyle()
                        style.messageColor = UIColor.white
                        style.titleFont.withSize(16)
                        self.view?.makeToast(msg, duration: 3.0, position: ToastPosition.center, title:title, image:nil, style: style, completion: { (true) in
                            
                            //                            Move to Login Screen
                            self.navigationController?.popToRootViewController(animated: true)
                            
                        })
                    }
                    
                }
                else if dictonary["status"] == false{
                    //Display msg
                    let msg = dictonary["message"]?.stringValue
                    let title = dictonary["title"]?.stringValue
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        var style = ToastStyle()
                        style.messageColor = UIColor.white
                        self.view?.makeToast(msg, duration: 3.0, position: ToastPosition.center, title:title, image:nil, style: style, completion: { (true) in
                        })
                    }
                }
                else {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    func resetPINApiCallFunc() {
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()

        let param = ["deviceId": deviceToken!,
                     "appName": "CiviMemberAPP",
                     "entity" : "CiviMobileContact",
                     "action": "resetpin",
                     "email": self.emailAddress ?? "",
                     "otp" : self.otpText ?? "",
                     "pin" : self.repeatPINString ?? ""] as [String : Any];
        
        print("Reset PIN request params : \(param)")
        
        apiObj.postData(url:kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Reset PIN response : \(json)")
            if status
            {
                let dictonary = json["values"].dictionaryValue
                
                if dictonary["status"] == true {
                    
                    let msg = dictonary["message"]?.stringValue
                    let title = dictonary["title"]?.stringValue
                    
                    DispatchQueue.main.async {
                        print("Reset PIN success")
                        SVProgressHUD.dismiss()
                        var style = ToastStyle()
                        style.messageColor = UIColor.white
                        style.titleFont.withSize(16)
                        self.view?.makeToast(msg, duration: 3.0, position: ToastPosition.center, title:title, image:nil, style: style, completion: { (true) in
                            
                            //                            Move to Login Screen
                            self.navigationController?.popToRootViewController(animated: true)
                            
                        })
                    }
                    
                }
                else if dictonary["status"] == false{
                    //Display msg
                    let msg = dictonary["message"]?.stringValue
                    let title = dictonary["title"]?.stringValue
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        var style = ToastStyle()
                        style.messageColor = UIColor.white
                        self.view?.makeToast(msg, duration: 3.0, position: ToastPosition.center, title:title, image:nil, style: style, completion: { (true) in
                        })
                    }
                }
                else {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RepeatPINViewController: UITextFieldDelegate{
    private func textFieldDidBeginEditing(textField: UITextField) {
        textField.text = ""
    }
}
