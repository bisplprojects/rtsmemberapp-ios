//
//  CreatePINViewController.swift
//  RTSPortal
//
//  Created by sajan on 06/01/20.
//  Copyright © 2020 BISPL. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class CreatePINViewController: UIViewController,MyTextFieldDelegate,RepeatPinDelegate {

    var emailAddress : String?
    var otpText : String?
    var pinString : String?
    var isFromForgotPIN : Bool?
    
    @IBOutlet weak var firstTxtField: PinTextField!
    @IBOutlet weak var secondTxtField: PinTextField!
    @IBOutlet weak var thirdTxtField: PinTextField!
    @IBOutlet weak var fourthTxtField: PinTextField!
    @IBOutlet weak var fifthTxtField: PinTextField!
    @IBOutlet weak var sixthTxtField: PinTextField!
    
    
    func textFieldDidDelete(textField: UITextField) {
        print("delete")
        self.textFieldDidChange(textField: textField)

    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        firstTxtField.useUnderline()
        secondTxtField.useUnderline()
        thirdTxtField.useUnderline()
        fourthTxtField.useUnderline()
        fifthTxtField.useUnderline()
        sixthTxtField.useUnderline()

        firstTxtField.delegate = self
        secondTxtField.delegate = self
        thirdTxtField.delegate = self
        fourthTxtField.delegate = self
        fifthTxtField.delegate = self
        sixthTxtField.delegate = self

        firstTxtField.myDelegate = self
        secondTxtField.myDelegate = self
        thirdTxtField.myDelegate = self
        fourthTxtField.myDelegate = self
        fifthTxtField.myDelegate = self
        sixthTxtField.myDelegate = self

    }
    

    func clearAllFields()  {
        self.firstTxtField.text = ""
        self.secondTxtField.text = ""
        self.thirdTxtField.text = ""
        self.fourthTxtField.text = ""
        self.fifthTxtField.text = ""
        self.sixthTxtField.text = ""
    }
    
    // MARK: - IBActions
    
    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionContinueBtnTapped(_ sender: Any) {
        self.view.endEditing(true)
        
        let valid = validatePIN()
        if valid{
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let repeatPinVC = storyboard.instantiateViewController(withIdentifier: "RepeatPINViewController") as! RepeatPINViewController
            repeatPinVC.emailAddress = self.emailAddress
            repeatPinVC.otpText = self.otpText
            repeatPinVC.pinString = self.pinString
            repeatPinVC.isFromForgotPIN = self.isFromForgotPIN
            repeatPinVC.repeatPinDelegate = self
            self.navigationController?.pushViewController(repeatPinVC, animated: true)
        }

    }
    
    
    func validatePIN() -> Bool {
        self.pinString = "\((firstTxtField?.text)!)\((secondTxtField?.text)!)\((thirdTxtField?.text)!)\((fourthTxtField?.text)!)\((fifthTxtField?.text)!)\((sixthTxtField?.text)!)"
       
        print(self.pinString as Any)

        if(self.pinString!.trimmingCharacters(in: .whitespaces).isEmpty)
        {
            self.view.makeToast("Enter PIN", duration: 2.0, position: .center)
            return false
        }
        else
        {
            let temp = isPinValid(self.pinString!)
            if (temp == false)
            {
                self.view.makeToast("Enter valid PIN", duration: 2.0, position: .center)
                return false
            }
        }

        return true
    }
    
    func isPinValid(_ pin : String) -> Bool{
        let pinTest = NSPredicate(format: "SELF MATCHES %@", "[0-9]{6,6}")
        return pinTest.evaluate(with: pin)
    }

    
    @objc func textFieldDidChange(textField: UITextField){

        let text = textField.text

        if (text?.utf16.count)! >= 1{
            switch textField{
            case firstTxtField:
                secondTxtField.becomeFirstResponder()
            case secondTxtField:
                thirdTxtField.becomeFirstResponder()
            case thirdTxtField:
                fourthTxtField.becomeFirstResponder()
            case fourthTxtField:
                fifthTxtField.becomeFirstResponder()
            case fifthTxtField:
                sixthTxtField.becomeFirstResponder()
//            case sixthTxtField:
//                sixthTxtField.resignFirstResponder()

            default:
                break
            }
        }else{

        }
        
        if  text?.count == 0 {
            switch textField{
            case firstTxtField:
                firstTxtField.becomeFirstResponder()
            case secondTxtField:
                firstTxtField.becomeFirstResponder()
            case thirdTxtField:
                secondTxtField.becomeFirstResponder()
            case fourthTxtField:
                thirdTxtField.becomeFirstResponder()
            case fifthTxtField:
                fourthTxtField.becomeFirstResponder()
            case sixthTxtField:
                fifthTxtField.becomeFirstResponder()

            default:
                break
            }
        }
        else{
            
        }

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // On inputing value to textfield
        if ((textField.text?.characters.count)! < 1  && string.characters.count > 0){
            if(textField == firstTxtField)
            {
                secondTxtField.becomeFirstResponder()
            }
            if(textField == secondTxtField)
            {
                thirdTxtField.becomeFirstResponder()
            }
            if(textField == thirdTxtField)
            {
                fourthTxtField.becomeFirstResponder()
            }
            if(textField == fourthTxtField)
            {
                fifthTxtField.becomeFirstResponder()
            }
            if(textField == fifthTxtField)
            {
                sixthTxtField.becomeFirstResponder()
            }

            textField.text = string
            return false
        }
        else if ((textField.text?.characters.count)! >= 1  && string.characters.count == 0){
            // on deleting value from Textfield
            if(textField == secondTxtField)
            {
                firstTxtField.becomeFirstResponder()
            }
            if(textField == thirdTxtField)
            {
                secondTxtField.becomeFirstResponder()
            }
            if(textField == fourthTxtField)
            {
                thirdTxtField.becomeFirstResponder()
            }
            if(textField == fifthTxtField)
            {
                fourthTxtField.becomeFirstResponder()
            }
            if(textField == sixthTxtField)
            {
                fifthTxtField.becomeFirstResponder()
            }

            textField.text = ""
            return false
        }
        else if ((textField.text?.characters.count)! >= 1  )
        {
            textField.text = string
            self.textFieldDidChange(textField: textField)
            return false
        }
        return true

    }
         
    // MARK: - Repeat PIN Delegate Method

    func pinDidNotMatch(isMissMatch: Bool) {
        self.clearAllFields()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CreatePINViewController: UITextFieldDelegate{
    private func textFieldDidBeginEditing(textField: UITextField) {
        textField.text = ""
    }
}

extension UITextField{
    
    func useUnderline() {

        let border = CALayer()
        let borderWidth = CGFloat(1.0)
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - borderWidth, width: self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = borderWidth
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }

}
