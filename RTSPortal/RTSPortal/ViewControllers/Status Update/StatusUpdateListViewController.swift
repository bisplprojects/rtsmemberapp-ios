//
//  StatusUpdateListViewController.swift
//  BGSPortal
//
//  Created by sajan on 20/01/20.
//  Copyright © 2020 BISPL. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD
import IQKeyboardManagerSwift

class StatusUpdateListViewController: UIViewController,UISearchResultsUpdating,PaticipantLookupDelegate {

    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var statusUpdateListTableView: UITableView!
    
    let cellIdentifier = "StatusUpdateCell"
    var searchController:UISearchController!

    var eventID:String?
    var participantID :String?
    var tID :String?

    var allParticipantList:[Paticipant] = []
    var alParticipantListFilter:[Paticipant] = []
    

    // MARK: -

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.sizeToFit()
        definesPresentationContext = true
        searchController.searchBar.placeholder = "Search"
        if #available(iOS 13.0, *){
            searchController.searchBar.searchTextField.font = UIFont(name: "Verdana", size: 14)
        }
        searchController.searchBar.barTintColor = UIColor.white
        self.searchContainerView.addSubview(searchController.searchBar)
        
        let lineView = UIView(frame: CGRect(x: 0, y: searchController.searchBar.frame.height-1, width: view.bounds.width, height: 1))
        lineView.backgroundColor = UIColor.white
        searchController.searchBar.addSubview(lineView)

        self.searchContainerView.layer.cornerRadius = 5
        self.searchContainerView.layer.shadowColor = UIColor.gray.cgColor
        self.searchContainerView.layer.shadowOpacity = 0.6
        self.searchContainerView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        self.searchContainerView.layer.shadowRadius = 4

        self.loadStatusUpdateApiCallFunc()

    }
    
    // MARK: - IBActions
    
    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - PaticipantLookup Delegate

    func didClickOnFindButton(eventID: String?) {
        self.eventID = eventID
        self.loadStatusUpdateApiCallFunc()
    }

    // MARK: - Webservice
    
    func loadStatusUpdateApiCallFunc() {
        
        SVProgressHUD.show()
        self.allParticipantList.removeAll()
        self.alParticipantListFilter.removeAll()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileEvent",
                     "action" : "participantlookup",
                     "event_id" : self.eventID,
                     "search_term" : "",
                     "token" : tokenString];
        print("Paticipant list  params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Paticipant list response : \(json)")
            
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let participantsList = dataDict["participants"]?.arrayValue
                        if participantsList!.count > 0 {
                            
                            let responseArray = participantsList
                            
                            for (_, participantDict) in (responseArray?.enumerated())! {
                                let participantObj = Paticipant(participantsRegisteredList: participantDict.dictionary!)
                                self.allParticipantList.append(participantObj)
                            }
                            self.alParticipantListFilter = self.allParticipantList
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.statusUpdateListTableView.reloadData()
                                if self.alParticipantListFilter.count == 0 {
                                    self.statusUpdateListTableView.setEmptyMessage("Nothing found")
                                } else {
                                    self.statusUpdateListTableView.restore()
                                }
                            }
                        }
                        else {
                            //Display msg
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast("No participant found", duration: 2.0, position: .center)
                                self.statusUpdateListTableView.reloadData()
                                if self.alParticipantListFilter.count == 0 {
                                    self.statusUpdateListTableView.setEmptyMessage("No participant found")
                                } else {
                                    self.statusUpdateListTableView.restore()
                                }

                            }
                        }
                    }
                    else
                    {
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                else
                {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
        
    }
    
    // MARK: - UISearch result update delegate
    
    func updateSearchResults(for searchController: UISearchController) {
        
        if let searchText = self.searchController.searchBar.text {

            var resultArr:[Paticipant] = []
            
            var arr2 = searchText.components(separatedBy: .whitespaces)
            arr2 = arr2.filter({ $0 != ""})
            
            let arr1 = self.allParticipantList
            
            for (_, str1) in (arr1.enumerated()) {
                var counter = 0
                for (_, str2) in (arr2.enumerated()) {
                    
                    if ((str1.participantDisplayName?.lowercased().contains(string: str2.lowercased()))! || ((str1.ticketNumber?.lowercased().contains(string: str2.lowercased()))!)){
                        counter += 1
                        if counter == arr2.count{
                            resultArr.append(str1)
                        }
                    }
                }
            }
            
            self.alParticipantListFilter = searchText.trimmingCharacters(in: .whitespaces).isEmpty ? self.allParticipantList : resultArr
            
            self.statusUpdateListTableView.reloadData()
            if self.alParticipantListFilter.count == 0 {
                self.statusUpdateListTableView.setEmptyMessage("Nothing found")
            } else {
                self.statusUpdateListTableView.restore()
            }
        }
    }
    
    func moveToParticipantDetailsViewController()  {
        let detailVC = UIViewController.participantDetailsViewController() as! ParticipantDetailsViewController
        detailVC.participantID = self.participantID
        detailVC.tID = self.tID
        detailVC.participantLookupDelegate = self
        self.navigationController?.pushViewController(detailVC, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension StatusUpdateListViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.alParticipantListFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let statusUpdateCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! StatusUpdateCell
        
//        statusUpdateCell.containerView.layer.cornerRadius = 5
//        statusUpdateCell.containerView.layer.shadowColor = UIColor.lightGray.cgColor
//        statusUpdateCell.containerView.layer.shadowOpacity = 0.3
//        statusUpdateCell.containerView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
//        statusUpdateCell.containerView.layer.shadowRadius = 4
        
        let participant:Paticipant = self.alParticipantListFilter[indexPath.row]
        
        statusUpdateCell.titleLbl.text = participant.participantDisplayName
        statusUpdateCell.tidLbl.text = participant.ticketNumber

        
        
        return statusUpdateCell

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        IQKeyboardManager.shared.resignFirstResponder()
        let participant:Paticipant = self.alParticipantListFilter[indexPath.row]
        self.participantID = participant.participantId
        self.tID = participant.tID
        self.moveToParticipantDetailsViewController()
    }
}
 
