//
//  StatusUpdateCell.swift
//  BGSPortal
//
//  Created by sajan on 20/01/20.
//  Copyright © 2020 BISPL. All rights reserved.
//

import UIKit

class StatusUpdateCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var tidLbl: UILabel!
    
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
