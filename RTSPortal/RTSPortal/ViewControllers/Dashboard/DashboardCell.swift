//
//  DashboardCell.swift
//  RTSPortal
//
//  Created by sajan on 09/09/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

class DashboardCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var itemBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    
}
