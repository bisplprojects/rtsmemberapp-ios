//
//  DashboardViewController.swift
//  RTSPortal
//
//  Created by sajan on 09/09/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import Foundation
import SVProgressHUD
import Toast_Swift
import CoreData
import Reachability

class DashboardViewController: UIViewController,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerImgView: UIImageView!
    @IBOutlet weak var navigationBarLine: UIView!
    
    @IBOutlet weak var newsHeaderView: UIView!
    @IBOutlet weak var newsTableView: UITableView!
    
    let screenSize: CGRect = UIScreen.main.bounds
    var screenWidth:CGFloat!, screenHeight:CGFloat!

    private let reuseIdentifier = "dashCollectionCell"
    let cellIdentifier = "NewsCell"

    var dashboardListTitlesFilter:[String] = []
    var dashboardListTitles:[String] = []
    var dashboardListItemImagesFilter:[String] = []
    var dashboardListItemImages:[String] = []
    var dashboardMenuList:[Menu] = []
    var dashboardMenuListFilter:[Menu] = []
    var currentVC:UIViewController?
    var menuCollectionView:UICollectionView!
    var dVC:DashboardViewController?

    
    var newsList:[News] = []
    var offlineNewsList:[NewsData] = []
    var sortedOfflineNewsList:[NewsData] = []

    var pageNumber:String = "1"
    var isLastPage:Bool = false
    var currentPage:Int?
    var nextPage:String?
    var offlineItemsCount:Int?
        
    let reachability = try! Reachability()

    var isFirstLoad:String?
    
    // MARK: -

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isFirstLoad = "1"
        
        let frame:CGRect = UIScreen.main.bounds
        screenWidth = frame.size.width;
        screenHeight = frame.size.height;
        self.currentVC = self
        self.addShadowToNavigation()
        

        
        if Reachabilit.isConnectedToNetwork() {
            DatabaseHelper.shareInstance.deleteAllNewsRecords()
        }
        
        self.loadMenuItem()

        self.loadNewsListApiCallFunc()

        
       /* let roles = UserDefaults.standard.getUserRoles()
        
        if roles.contains("EventManager") {
            //Event Manager
            print("EventManager")
            dashboardListTitles = ["PROFILE", "EVENTS","SCAN"]
            dashboardListItemImages = ["profile_icon","event_white_icon","scan_icon"]
        }else if roles.contains("Supporter"){
            //Supporter
            print("Supporter")
            dashboardListTitles = ["PROFILE", "EVENTS"]
            dashboardListItemImages = ["profile_icon","event_white_icon"]
        }
        else if roles.contains(""){
            //Display msg
            let messageStr = "Something went wrong. Please contact admin!"
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.view.makeToast(messageStr, duration: 3.0, position: .center)
 
        }
        }*/
        
        print("value of dashboardmenufiler: \(self.dashboardMenuListFilter)")
        
       // print("dashboard title list after loading menu : \(self.dashboardListTitles.count)")
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: screenWidth * 0.32, height: screenWidth * 0.32)
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)

        self.menuCollectionView = UICollectionView(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: screenSize.width * 0.4), collectionViewLayout: flowLayout)
        menuCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        menuCollectionView.delegate = self
        menuCollectionView.dataSource = self
        menuCollectionView.backgroundColor = UIColor(red: 17.0/255.0, green: 131.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        self.headerView.addSubview(menuCollectionView)
        
        print("items inside viewdidload: \(self.dashboardListTitles)")

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Get news from coredata
        self.offlineNewsList = DatabaseHelper.shareInstance.fetchNewsData()
        

//        self.sortedOfflineNewsList = self.offlineNewsList.sorted(by: { ($0.displayOrderID!) < ($1.displayOrderID!) })

        
//        self.sortedOfflineNewsList = self.offlineNewsList.sorted(by: { (item1, item2) -> Bool in
//            return item1.displayOrderID.compare(item2.displayOrderID) == ComparisonResult.orderedAscending
//        })

//        self.sortedOfflineNewsList = self.offlineNewsList.sorted(by: { (Obj1, Obj2) -> Bool in
//           let Obj1_Name = Obj1.displayOrderID ?? ""
//           let Obj2_Name = Obj2.displayOrderID ?? ""
//            return (Obj1_Name.compare(Obj2_Name) == .orderedAscending)
//        })
        
         NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
          try reachability.startNotifier()
        }catch{
          print("could not start reachability notifier")
        }

        
    }
    
    @objc func reachabilityChanged(note: Notification) {

      let reachability = note.object as! Reachability

      switch reachability.connection {
      case .wifi:
          print("Reachable via WiFi")
//        self.pageNumber = "1"
//        self.newsList.removeAll()
//        self.loadNewsListApiCallFunc()
          
          if (self.newsList.count == 0 && isFirstLoad == "0") {
            self.pageNumber = "1"
            self.newsList.removeAll()
            self.loadNewsListApiCallFunc()
          }else{
            self.newsTableView.reloadData()
          }

      case .cellular:
          print("Reachable via Cellular")
        if (self.newsList.count == 0 && isFirstLoad == "0") {
            self.pageNumber = "1"
            self.newsList.removeAll()
            self.loadNewsListApiCallFunc()
        }else{
            self.newsTableView.reloadData()
        }

      case .unavailable:
        print("Network not reachable")
        self.offlineNewsList = DatabaseHelper.shareInstance.fetchNewsData()

        self.newsTableView.reloadData()
      case .none:
        break
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //Get news from coredata
//        DispatchQueue.global().async {
//            self.offlineNewsList = DatabaseHelper.shareInstance.fetchNewsData()
//            DispatchQueue.main.async {
//                        self.newsTableView.reloadData()
//            }
//        }
//        self.offlineNewsList = DatabaseHelper.shareInstance.fetchNewsData()
//        self.newsTableView.reloadData()

        self.isFirstLoad = "0"

    }
    
    // MARK: - Webservice
    
    func loadMenuItem(){
        
        if !(Reachabilit.isConnectedToNetwork()) {
            self.view.makeToast("No internet", duration: 2.0, position: .center)
            return
        }

        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let roles = UserDefaults.standard.getUserRoles()
        
        let rolesString = convertIntoJSONString(arrayObject: roles)
        let param = ["entity" : "CiviMobileApp",
                     "action" : "menuitems",
                     "cid" : dict["ContactId"],
                     "token" : tokenString,
                     "roles" : rolesString];
        print("Menu Items request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Menu List response : \(json)")
            
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let menu = dataDict["menuitems"]?.arrayValue
                        if menu!.count > 0 {
                            let homeMenu = menu?.first
                            print("home menu \(homeMenu)")
                            let subMenu = homeMenu!["home_menu"].dictionaryValue
                            let home_menu = subMenu["submenu"]!.arrayValue
                            print("home menu submenu items : \(home_menu)")
                            
                            if home_menu.count > 0 {
                                
                                let responseArray = home_menu
                                print("response array count : \(responseArray.count)")
                                                                
                                
                                for (_, menudict) in (responseArray.enumerated()) {
                                    let menuObj = Menu(menuDict: menudict.dictionary!)
                                    print ("menu object : \(menuObj)")
                                  //self.dashboardMenuList.append(menuObj)
                                    let menuTitle = menuObj.name!
                                    let menuIcon = menuObj.icon!
                                    let menuDisplay = menuObj.display!
                                    if (menuDisplay == "1"){
                                        
                                        print("value of menutitle: \(menuTitle)")
                                        // self.dashboardListTitlesFilter.append(menuTitle)
                                        self.dashboardListTitlesFilter.append(menuTitle)
                                        self.dashboardListItemImagesFilter.append(menuIcon)
                                        
                                    }
                                    else{
                                        self.dashboardListTitles = self.dashboardListTitlesFilter
                                        self.dashboardListItemImages = self.dashboardListItemImagesFilter
                                    }
                                    
                                }
                                //self.dashboardMenuListFilter = self.dashboardMenuList
                               // print("menulistfilter: \(self.dashboardMenuListFilter)")
                                self.dashboardListTitles = self.dashboardListTitlesFilter
                                print("dashboard title list: \(self.dashboardListTitles)")
                                self.dashboardListItemImages = self.dashboardListItemImagesFilter
                                print("dashboard image list: \(self.dashboardListItemImages)")
                                
                  /* for(_, m) in (self.dashboardMenuListFilter.enumerated()){
                                    let menuTitle = m.name!
                                    let menuIcon = m.icon!
                                    let menuDisplay = m.display!
                                    if (menuDisplay == "0"){
                                        
                                        print("value of menutitle: \(menuTitle)")
                                       // self.dashboardListTitlesFilter.append(menuTitle)
                                        self.dashboardListTitlesFilter.append(menuTitle)
                                        self.dashboardListItemImagesFilter.append(menuIcon)
                                        
                                    }
                                    else{
                                        self.dashboardListTitles = self.dashboardListTitlesFilter
                                        self.dashboardListItemImages = self.dashboardListItemImagesFilter
                                    }
                                }*/
                                
                                print("title list: \(self.dashboardListTitles)")
                                   print("dashboardListTitlescount  \(self.dashboardListTitles.count)")
                                
                               // self.dashboardListItemImages = self.dashboardListItemImagesFilter
                               // print("image list: \(self.dashboardListItemImages)")
                        
                                DispatchQueue.main.async {
                                   SVProgressHUD.dismiss()
                                
                                    //self.dashboardMenuListFilter = self.dashboardMenuList
                                    self.menuCollectionView.reloadData()
                                    if self.dashboardListTitles.count == 0 {
                                        self.view.makeToast("No Menu Item found", duration: 3.0, position: .center)
                                    } else {
                                        self.menuCollectionView.reloadData()
                                        //self.dashboardMenuListFilter = self.dashboardMenuList
                                    }
                               }
                            }
                            else {
                                //Display msg
                                DispatchQueue.main.async {
                                    SVProgressHUD.dismiss()
                                    self.view.makeToast("No Menu Item found", duration: 3.0, position: .center)
                                }
                            }
                            
                        }
                     
                       
                    }
                    else
                    {
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.view.makeToast("something wrong in json if block", duration: 3.0, position: .center)
                        }
                    }
                }
                else
                {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast("something wrong in status if block", duration: 3.0, position: .center)
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.view.makeToast("something wrong in api block", duration: 3.0, position: .center)
                }
            }
        }
        print("value of menu list titles: \(self.dashboardListTitles)")
        
    }
    
    
    func logoutApiCallFunc() {
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileContact",
                     "action" : "logout",
                     "cid" : dict["ContactId"],
                     "token" : tokenString];
        print("Logout request params : \(param)")
        
        apiObj.postData(url:kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Logout response : \(json)")
            if status
            {
                let dictonary = json["values"].dictionaryValue
                let dataDict = dictonary["data"]
                
                if dataDict?["status"] == true {
                    
                    let titleStr = dataDict?["title"].stringValue
                    let messageStr = dataDict?["message"].stringValue
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        var style = ToastStyle()
                        style.messageColor = UIColor.white
                        self.currentVC?.view?.makeToast(messageStr, duration: 1.0, position: ToastPosition.center, title:titleStr, image:nil, style: style, completion: { (true) in
                            self.clearLoginDetails()
                            
                        })
                    }
                }
                else if dataDict?["status"] == false{
                    //Display msg
                    let messageStr = "Something went wrong. Please contact admin!"
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast(messageStr, duration: 3.0, position: .center)
                    }
                }
                else {
                    //Display msg
                    let messageStr = "Something went wrong. Please contact admin!"
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast(messageStr, duration: 2.0, position: .center)
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    func loadNewsListApiCallFunc() {
        
        
        if !(Reachabilit.isConnectedToNetwork()) {
            self.view.makeToast("No internet", duration: 2.0, position: .center)
            self.newsTableView.reloadData()
            return
        }
        
        SVProgressHUD.show()
        
        if(self.newsList.count != 0 && self.pageNumber == "1")
        {
            self.newsList.removeAll()
        }

        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileNews",
                     "action" : "getlist",
                     "cid" : dict["ContactId"],
                     "page_no" : self.pageNumber,
                     "token" : tokenString];
        print("News request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("News List response : \(json)")
            
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        self.isLastPage = dataDict["is_last_page"]!.boolValue
                        self.currentPage = dataDict["current_page"]?.intValue
                        self.nextPage = dataDict["next_page"]?.stringValue
                        self.offlineItemsCount = dataDict["offline_items"]?.intValue

                        let newsList = dataDict["newslist"]?.arrayValue
                        if newsList!.count > 0 {
                            
                            let responseArray = newsList
                            
                            for (_, newsDict) in (responseArray?.enumerated())! {
                                let newsObj = News(dict: newsDict.dictionary!)
                                self.newsList.append(newsObj)
                            }

//                            //save to coredata
//                            self.saveNewsDataToCoredata()
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.newsTableView.reloadData()
                                if self.newsList.count == 0 {
                                    self.newsTableView.setEmptyMessage("News not found")
                                } else {
                                    self.newsTableView.restore()
                                }
//                                //save to coredata
                                self.saveNewsDataToCoredata()

                            }
                        }
                        else {
                            //Display msg
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast("News not found", duration: 3.0, position: .center)
                            }
                        }
                    }
                    else
                    {
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                else
                {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
        
    }
    
    func clearLoginDetails()  {
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()
       
       let svc = SignInViewController()
        self.currentVC?.navigationController?.popViewController(animated: true)
        
    }

    
    func saveNewsDataToCoredata()  {
        
        
        if Reachabilit.isConnectedToNetwork() {
            
            self.offlineNewsList = DatabaseHelper.shareInstance.fetchNewsData()

            if self.offlineNewsList.count == self.offlineItemsCount {
                return
            }
            DatabaseHelper.shareInstance.deleteAllNewsRecords()
            
            if (self.newsList.count > 0 && self.newsList.count <= self.offlineItemsCount!) {
                
                for news in self.newsList {
                    
                    DatabaseHelper.shareInstance.saveNewsDataWithoutImg(newsId: news.newsId ?? "", displayOrderID: news.displayOrderID!, newsTitle: news.newsTitle ?? "", newsIntroductoryText: news.newsIntroductoryText ?? "", newsCategory: news.newsCategory ?? "", newsCreatedDate: news.newsCreatedDate ?? "", newsColor: news.newsColor ?? "")


//                    let task = URLSession.shared.downloadTask(with: imgUrl) { localURL, urlResponse, error in
//                        if let localURL = localURL {
//                            if let data = try? Data(contentsOf: localURL) {
//                                let newsImage:UIImage = UIImage(data: data)!
//                                if let imageData = newsImage.pngData() {
//                                    DatabaseHelper.shareInstance.saveNewsData(newsId: news.newsId ?? "", displayOrderID: news.displayOrderID!, newsTitle: news.newsTitle ?? "", newsIntroductoryText: news.newsIntroductoryText ?? "", newsCategory: news.newsCategory ?? "", newsCreatedDate: news.newsCreatedDate ?? "", newsColor: news.newsColor ?? "", newsImgData: imageData)
//                                }
//
//                                print(data)
//                            }
//                        }
//                    }
//                    task.resume()


                    
//                    DispatchQueue.global().async { [weak self] in
//                        if let data = try? Data(contentsOf: imgUrl){
//                            let newsImage:UIImage = UIImage(data: data)!
//                            if let imageData = newsImage.pngData() {
//                                DispatchQueue.main.async {
//
//                                    DatabaseHelper.shareInstance.saveNewsDataWithoutImg(newsId: news.newsId ?? "", displayOrderID: news.displayOrderID!, newsTitle: news.newsTitle ?? "", newsIntroductoryText: news.newsIntroductoryText ?? "", newsCategory: news.newsCategory ?? "", newsCreatedDate: news.newsCreatedDate ?? "", newsColor: news.newsColor ?? "")
//                                }
//                            }
//                        }
//                    }
                    
                    
                }
                
                self.updateNewsImagesToCoredata()
            }
            else if (self.newsList.count > 0 && self.newsList.count > self.offlineItemsCount!){
                
                for i in 0..<self.offlineItemsCount! {
                    let news = self.newsList[i]
                    DatabaseHelper.shareInstance.saveNewsDataWithoutImg(newsId: news.newsId ?? "", displayOrderID: news.displayOrderID!, newsTitle: news.newsTitle ?? "", newsIntroductoryText: news.newsIntroductoryText ?? "", newsCategory: news.newsCategory ?? "", newsCreatedDate: news.newsCreatedDate ?? "", newsColor: news.newsColor ?? "")

                }
                self.updateNewsImagesToCoredata()
            }
        }
    }
    
    func updateNewsImagesToCoredata()  {
        if Reachabilit.isConnectedToNetwork() {
            
            
            if self.newsList.count > 0 {
                
                for news in self.newsList {
                    
                    let imgUrlString:String? = news.newsImageUrl
                    guard let stringURL = imgUrlString else {
                        return
                    }
                    guard let imgUrl = URL(string: stringURL) else {
                        // We should handle an invalid stringURL
                        return
                    }
                    
                    DispatchQueue.global().async { [weak self] in
                        if let data = try? Data(contentsOf: imgUrl){
                            let newsImage:UIImage = UIImage(data: data)!
                            if let imageData = newsImage.pngData() {
                                DispatchQueue.main.async {

                                    DatabaseHelper.shareInstance.updateImageDataForNews(newsID: news.newsId!, newsImageData: imageData)
                                }
                            }
                        }
                    }
                }
            }
        }

    }
    
    // MARK: - IBActions

    @IBAction func actionMenuBtnTapped(_ sender: Any) {
        UserActionManager.displayLeftMenu()
    }
    
    @objc func actionMenuItemTapped(_ sender:UIButton)  {
        
        print("tag \(sender.tag)")
        switch sender.tag {
        case 0:
            if self.dashboardListTitles == ["PROFILE", "EVENTS", "SCAN"]{
                self.moveToController(UIViewController.profileDashboardViewController())
            }
            else if self.dashboardListTitles == ["EVENTS", "SCAN"]{
                self.moveToController(UIViewController.allEventsListViewController())
            }
            else if self.dashboardListTitles == ["EVENTS"]{
                self.moveToController(UIViewController.allEventsListViewController())
            } else if self.dashboardListTitles == ["SCAN"]{
                let qrScanVC = UIViewController.qrScanViewController() as! QRScanViewController
                self.navigationController?.pushViewController(qrScanVC, animated: true)
            }else{
                self.moveToController(UIViewController.profileDashboardViewController())
            }
            
            break
        case 1:
            if self.dashboardListTitles == ["PROFILE", "EVENTS", "SCAN"]{
                self.moveToController(UIViewController.allEventsListViewController())
            }
            else if self.dashboardListTitles == ["PROFILE", "SCAN"]{
                let qrScanVC = UIViewController.qrScanViewController() as! QRScanViewController
                self.navigationController?.pushViewController(qrScanVC, animated: true)
            }
            else if self.dashboardListTitles == ["PROFILE", "EVENTS"]{
                self.moveToController(UIViewController.allEventsListViewController())
            } else if self.dashboardListTitles == ["EVENTS", "SCAN"]{
                let qrScanVC = UIViewController.qrScanViewController() as! QRScanViewController
                self.navigationController?.pushViewController(qrScanVC, animated: true)
            }
            break
        case 2 :
            
            let qrScanVC = UIViewController.qrScanViewController() as! QRScanViewController
            self.navigationController?.pushViewController(qrScanVC, animated: true)
            break
            
        default:
            print("Push to viewControllers")
        }
        
    }
    
    func moveToController(_ controller:UIViewController){
        
        var viewControllers = ( self.currentVC?.navigationController?.viewControllers)!
        viewControllers.removeLast()
        viewControllers.append(controller)
        self.currentVC?.navigationController?.setViewControllers(viewControllers, animated: true)
    }

    func moveToNewsDetailsViewController(newsID : String)  {
        let storyboard = UIStoryboard(name: "General", bundle: Bundle.main)
         let newsDetailsVC = storyboard.instantiateViewController(withIdentifier: "NewsDetailsViewController") as! NewsDetailsViewController
        newsDetailsVC.NewsID = newsID
        self.navigationController?.pushViewController(newsDetailsVC, animated: true)

    }
    
    func addShadowToNavigation() {
        self.navigationBarLine.layer.shadowColor = UIColor.black.cgColor
        self.navigationBarLine.layer.shadowOpacity = 1
        self.navigationBarLine.layer.shadowOffset = .zero
        self.navigationBarLine.layer.shadowRadius = 1
        self.navigationBarLine.layer.shadowPath = UIBezierPath(rect: self.navigationBarLine.bounds).cgPath
        self.navigationBarLine.layer.shouldRasterize = true
        self.navigationBarLine.layer.rasterizationScale = UIScreen.main.scale
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DashboardViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return self.dashboardListTitles.count
        //return self.dashboardMenuListFilter.count
       // return 3
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath)
        
        let containerView = UIView()
        containerView.frame = CGRect(x: cell.frame.size.width * 0.15, y: cell.frame.size.width * 0.1, width: cell.frame.size.width * 0.7, height: cell.frame.size.width * 0.7)

//        if indexPath.row % 2 == 0 {
//            containerView.frame = CGRect(x: cell.frame.size.width * 0.35, y: cell.frame.size.width * 0.2, width: cell.frame.size.width * 0.5, height: cell.frame.size.width * 0.5)
//        }
//        else
//        {
//            containerView.frame = CGRect(x: cell.frame.size.width * 0.15, y: cell.frame.size.width * 0.2, width: cell.frame.size.width * 0.5, height: cell.frame.size.width * 0.5)
//        }
        
        
        
        containerView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.3)
        containerView.layer.cornerRadius = containerView.frame.size.width * 0.5
        containerView.clipsToBounds = true
        
        //let menu:Menu = (self.dashboardMenuListFilter[indexPath.row])
       // print("idexpath of menu \(menu)")
       // let displayMenu = menu.display
        
        let url = URL(string: self.dashboardListItemImages[indexPath.row])
        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        //imageView.image = UIImage(data: data!)
        
        let menuBtnItem:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: containerView.frame.size.width, height: containerView.frame.size.width))
       // menuBtnItem.setImage(UIImage(named: self.dashboardListItemImages[indexPath.row]), for: .normal)
        menuBtnItem.setImage(UIImage(data: data!), for: .normal)
        menuBtnItem.contentVerticalAlignment = .fill
        menuBtnItem.contentHorizontalAlignment = .fill
        menuBtnItem.imageEdgeInsets = UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 30)
       
        menuBtnItem.tag = indexPath.row
        menuBtnItem.addTarget(self, action: #selector(self.actionMenuItemTapped(_:)), for: UIControl.Event.touchUpInside)
        containerView.addSubview(menuBtnItem)
        
        let titleLbl: UILabel = UILabel(frame: CGRect(x: 0.0, y: containerView.frame.origin.y + containerView.frame.size.height , width:cell.frame.size.width, height: 25))
        titleLbl.text = self.dashboardListTitles[indexPath.row]
        //titleLbl.text = menu.name!
        titleLbl.textAlignment = .center
        titleLbl.font = UIFont(name: "Arial", size: 12)
        titleLbl.textColor = UIColor.white
        titleLbl.center.x = containerView.center.x
        cell.addSubview(titleLbl)
        
        let badgeView = UIView(frame: CGRect(x: containerView.frame.origin.x + containerView.frame.size.width - 20, y: containerView.frame.origin.y - 8, width: 20, height:20))
        badgeView.backgroundColor = UIColor(red: 246/255, green: 61/255, blue: 66/255, alpha: 1.0)
        badgeView.layer.cornerRadius = badgeView.frame.size.width * 0.5
        badgeView.clipsToBounds = true
        
        let badgeLbl: UILabel = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: badgeView.frame.size.width, height: 20))
        badgeLbl.text = "3"
        badgeLbl.textAlignment = .center
        badgeLbl.font = UIFont(name: "Arial", size: 10)
        badgeLbl.textColor = UIColor.white
        badgeView.addSubview(badgeLbl)
        
        cell.addSubview(containerView)
        
//                cell.addSubview(badgeView)
        
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: screenSize.width * 0.32, height: screenSize.width * 0.32)
    }

}

extension DashboardViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if Reachabilit.isConnectedToNetwork() {
            return self.newsList.count
        }else{
            return self.offlineNewsList.count
        }
//        return self.newsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let newsCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! NewsCell
        
        if Reachabilit.isConnectedToNetwork() {
            let news:News = self.newsList[indexPath.row]
            
//            newsCell.newsImageView.layer.cornerRadius = 5
//            newsCell.newsImageView.clipsToBounds = true

            newsCell.titleLbl.text = news.newsTitle?.uppercased()
    //        newsCell.descLbl.text = news.newsIntroductoryText
            newsCell.dateLbl.text = news.newsCreatedDate
            newsCell.newsImageView.sd_setImage(with: URL(string:news.newsImageUrl!))
//            newsCell.contentMode = .left
//            newsCell.newsImageView.backgroundColor = UIColor(hexString: news.newsColor ?? "#969696")

            newsCell.categoryLbl.text = news.newsCategory?.uppercased()
            newsCell.categoryView.backgroundColor = UIColor(hexString: news.newsColor ?? "#969696")

            let attributedString = NSMutableAttributedString(string: (news.newsIntroductoryText?.htmlToString)!)
            // *** Create instance of `NSMutableParagraphStyle`
            let paragraphStyle = NSMutableParagraphStyle()
            // *** set LineSpacing property in points ***
    //        paragraphStyle.lineSpacing = 4 // Whatever line spacing you want in points
    //        paragraphStyle.alignment = .justified
            // *** Apply attribute to string ***
            attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
            // *** Set Attributed String to your label ***
            newsCell.descLbl.attributedText = attributedString

        }
        else{
            
            if (self.offlineNewsList.count > 0){
                let news:NewsData = self.offlineNewsList[indexPath.row]
                
                newsCell.newsImageView.layer.cornerRadius = 5
                newsCell.newsImageView.clipsToBounds = true

                newsCell.titleLbl.text = news.newsTitle?.uppercased()
                newsCell.dateLbl.text = news.newsDate
                newsCell.newsImageView.image = (news.newsImgData != nil) ? UIImage(data: news.newsImgData!) : UIImage(imageLiteralResourceName: "img_placeholder")
                newsCell.categoryLbl.text = news.newsCategory?.uppercased()
                newsCell.categoryView.backgroundColor = UIColor(hexString: news.newsColor ?? "#969696")

                let attributedString = NSMutableAttributedString(string: (news.newsIntroductoryText?.htmlToString)!)
                // *** Create instance of `NSMutableParagraphStyle`
                let paragraphStyle = NSMutableParagraphStyle()
                // *** set LineSpacing property in points ***
        //        paragraphStyle.lineSpacing = 4 // Whatever line spacing you want in points
        //        paragraphStyle.alignment = .justified
                // *** Apply attribute to string ***
                attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
                // *** Set Attributed String to your label ***
                newsCell.descLbl.attributedText = attributedString

            }

        }
        
//        let news:News = self.newsList[indexPath.row]
//
//        newsCell.newsImageView.layer.cornerRadius = 5
//        newsCell.newsImageView.clipsToBounds = true
//
//        newsCell.titleLbl.text = news.newsTitle?.uppercased()
////        newsCell.descLbl.text = news.newsIntroductoryText
//        newsCell.dateLbl.text = news.newsCreatedDate
//        newsCell.newsImageView.sd_setImage(with: URL(string:news.newsImageUrl!))
//        newsCell.categoryLbl.text = news.newsCategory?.uppercased()
//        newsCell.categoryView.backgroundColor = UIColor(hexString: news.newsColor ?? "#969696")
//
//        let attributedString = NSMutableAttributedString(string: (news.newsIntroductoryText?.htmlToString)!)
//        // *** Create instance of `NSMutableParagraphStyle`
//        let paragraphStyle = NSMutableParagraphStyle()
//        // *** set LineSpacing property in points ***
////        paragraphStyle.lineSpacing = 4 // Whatever line spacing you want in points
////        paragraphStyle.alignment = .justified
//        // *** Apply attribute to string ***
//        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
//        // *** Set Attributed String to your label ***
//        newsCell.descLbl.attributedText = attributedString

        return newsCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if Reachabilit.isConnectedToNetwork() {
            if self.newsList.count > 0 {
                let news:News = self.newsList[indexPath.row]
                guard let newsId = news.newsId else {
                    return
                }
                self.moveToNewsDetailsViewController(newsID: newsId)
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (scrollView.contentOffset.y + scrollView.frame.size.height > scrollView.contentSize.height) {
            print("load more ..")
            if self.isLastPage == false{
                var nxtPage:Int = Int(self.pageNumber)!
//                print("\(nxtPage)")
                nxtPage = nxtPage + 1
                self.pageNumber = String(nxtPage)
//                print("\(self.pageNumber)")

                self.loadNewsListApiCallFunc()
            }
        }
    }
}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
       let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
       let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
         scanner.scanLocation = 1
       }
       var color: UInt32 = 0
       scanner.scanHexInt32(&color)
       let mask = 0x000000FF
       let r = Int(color >> 16) & mask
       let g = Int(color >> 8) & mask
       let b = Int(color) & mask
       let red = CGFloat(r) / 255.0
       let green = CGFloat(g) / 255.0
       let blue = CGFloat(b) / 255.0
       self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    
    func toHexString() -> String {
       var r:CGFloat = 0
       var g:CGFloat = 0
       var b:CGFloat = 0
       var a:CGFloat = 0
       getRed(&r, green: &g, blue: &b, alpha: &a)
       let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}
