//
//  ChooseSigCell.swift
//  RTSPortal
//
//  Created by sajan on 31/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

class ChooseSigCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
