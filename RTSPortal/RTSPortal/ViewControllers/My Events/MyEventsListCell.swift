//
//  MyEventsListCell.swift
//  RTSPortal
//
//  Created by sajan on 17/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

class MyEventsListCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var eventTitleLbl: UILabel!
    @IBOutlet weak var eventStatusLbl: UILabel!
    @IBOutlet weak var eventDateLbl: UILabel!
    @IBOutlet weak var directionBtn: UIButton!
    @IBOutlet weak var arrowImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
