//
//  MyEventsListViewController.swift
//  RTSPortal
//
//  Created by sajan on 17/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import Foundation
import SVProgressHUD
import SwiftyJSON
import GoogleMaps
import GooglePlaces
import MapKit
import IQKeyboardManagerSwift

class MyEventsListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchResultsUpdating ,CLLocationManagerDelegate{

    @IBOutlet weak var myEventsTableView: UITableView!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchContainerView: UIView!
    
    var searchController:UISearchController!

    let cellIdentifier = "MyEventsListCell"

    var myEventsList:[Event] = []
    var myEventsListFilter:[Event] = []

    let locationManager = CLLocationManager()
    let LocationManager = CLLocationManager()

    var latitude:String?
    var longitude:String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // searchController will use this view controller to display the search results
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.sizeToFit()
        definesPresentationContext = true
        searchController.searchBar.placeholder = "Search"
        searchController.searchBar.barTintColor = UIColor.white
        
        self.searchContainerView.addSubview(searchController.searchBar)
        
        let lineView = UIView(frame: CGRect(x: 0, y: searchController.searchBar.frame.height-1, width: view.bounds.width, height: 1))
        lineView.backgroundColor = UIColor.white
        searchController.searchBar.addSubview(lineView)

        self.searchContainerView.layer.cornerRadius = 5
        self.searchContainerView.layer.shadowColor = UIColor.gray.cgColor
        self.searchContainerView.layer.shadowOpacity = 0.6
        self.searchContainerView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        self.searchContainerView.layer.shadowRadius = 4

        self.loadMyListApiCallFunc()
        
        locationManager.delegate = self
        self.startLocationServices()
        //locationManager.requestAlwaysAuthorization()
        //locationManager.startUpdatingLocation()
        
        

    }
    func startLocationServices() {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
            let locationAuthorizationStatus = CLLocationManager.authorizationStatus()
        
            switch locationAuthorizationStatus {
                case .notDetermined:
                       self.locationManager.requestWhenInUseAuthorization() // This is where you request permission to use location services
                self.locationManager.requestAlwaysAuthorization()
               case .authorizedWhenInUse, .authorizedAlways:
                       if CLLocationManager.locationServicesEnabled() {
                               self.locationManager.startUpdatingLocation()
                           }
                case .restricted, .denied:
                    self.locationManager.requestWhenInUseAuthorization()
                    self.locationManager.requestAlwaysAuthorization()
                        //self.alertLocationAccessNeeded()
               }
        }
    
    func alertLocationAccessNeeded() {
        let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
        
           let alert = UIAlertController(
        title: "Need Location Access",
            message: "Location access is required for including the location of the hazard.",
            preferredStyle: UIAlertController.Style.alert
            )
        
          alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
         alert.addAction(UIAlertAction(title: "Allow Location Access", style: .cancel,
        handler: { (alert) -> Void in UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            }))
    
        present(alert, animated: true, completion: nil)
        
    
        }
    
    // MARK: - IBActions

    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSearchBtnTapped(_ sender: Any) {
    }
    
    @objc func actionDirectionBtnTapped(_ sender: UIButton) {
        locationManager.delegate = self
        self.startLocationServices()
        let btn = sender
        if let tempPostCode = self.myEventsListFilter[btn.tag].postalCode{
            if tempPostCode != ""{
                self.getLatLngForZip(zipCode: self.myEventsListFilter[btn.tag].postalCode!)
                if (self.latitude != nil || self.longitude != nil){
                    
                    
                        self.openGoogleDirectionMap(self.latitude!, self.longitude!)
                        //locationManager.startUpdatingLocation()
                    
                    //self.openGoogleDirectionMap(self.latitude!, self.longitude!)
                }else{
                    self.view.makeToast("Direction not found", duration: 3.0, position: .center)
                }
            }
            else
            {
                self.view.makeToast("Direction not found", duration: 3.0, position: .center)
            }
        }
    }

    func openGoogleDirectionMap(_ destinationLat: String, _ destinationLng: String) {
        
       // let LocationManager = CLLocationManager()
        
        if let myLat = LocationManager.location?.coordinate.latitude, let myLng = LocationManager.location?.coordinate.longitude {
            
            if let tempURL = URL(string: "comgooglemaps://?saddr=&daddr=\(destinationLat),\(destinationLng)&directionsmode=driving") {
                LocationManager.delegate = self
                self.startLocationServices()
                
                UIApplication.shared.open(tempURL, options: [:], completionHandler: { (isSuccess) in
                    
                    if !isSuccess {
                        
                        if UIApplication.shared.canOpenURL(URL(string: "https://www.google.co.th/maps/dir///")!) {
                            
                            UIApplication.shared.open(URL(string: "https://www.google.co.th/maps/dir/\(myLat),\(myLng)/\(destinationLat),\(destinationLng)/")!, options: [:], completionHandler: nil)
                            
                        } else {
                            print("Can't open URL.")
                        }
                    }
                })
                
            } else {
                print("Can't open GoogleMap Application.")
            }
            
        } else {
            print("Please allow permission")
            self.alertLocationAccessNeeded()
        }
        
    }

    func getLatLngForZip(zipCode: String) {
        
        let zipString = zipCode.removingWhitespaces()
        
        let baseUrl = kGoogleMapsBaseUrl
        let apikey = kGoogleMapsAPIKey
        
        let url = NSURL(string: "\(baseUrl)address=\(zipString)&key=\(apikey)")
        let data = NSData(contentsOf: url! as URL)
        if let json = try? JSON(data: data! as Data) {
            
            if let result = json["results"].array {
                if result.count > 0{
                    if let geometry = result[0]["geometry"].dictionary {
                        if let location = geometry["location"]?.dictionary {
                            let latitude = location["lat"]?.stringValue
                            let longitude = location["lng"]?.stringValue
                            self.latitude = latitude
                            self.longitude = longitude
                            print("\n\(String(describing: latitude)), \(String(describing: longitude))")
                        }
                    }
                }
            }
        }
    }

    @objc func directionTapped() {
        let fromLatitude = 12.9141
        let fromLongitude = 74.8560
        let toLatitude = 12.9716
        let toLongitude = 77.5946
        
        // if GoogleMap installed
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
           locationManager.delegate = self
            self.startLocationServices()
            
            UIApplication.shared.openURL(NSURL(string:
                "comgooglemaps://?saddr=\(fromLatitude),\(fromLongitude)&daddr=\(toLatitude),\(toLongitude)&directionsmode=driving")! as URL)
            
        } else {
            // if GoogleMap App is not installed
            UIApplication.shared.openURL(NSURL(string:
                "https://www.google.co.in/maps/dir/?saddr=\(fromLatitude),\(fromLongitude)&daddr=\(toLatitude),\(toLongitude)&directionsmode=driving")! as URL)
        }
        
    }

    // MARK: - Webservice
    
    func loadMyListApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileEvent",
                     "action" : "myevents",
                     "token" : tokenString,
                     "cid" : dict["ContactId"]];
        print("My Events request params : \(param)")

        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("My Events List response : \(json)")

            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                    
                        let dataDict = dictonary["data"].dictionaryValue
                        let myevents = dataDict["myevents"]?.arrayValue
                        if myevents!.count > 0 {
                            
                            let responseArray = myevents
                            
                            for (_, eventDict) in (responseArray?.enumerated())! {
                                let eventObj = Event(dict: eventDict.dictionary!)
                                self.myEventsList.append(eventObj)
                            }
                            self.myEventsListFilter = self.myEventsList
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                // TO DO : Display toast msg
                                self.myEventsTableView.reloadData()
                                if self.myEventsListFilter.count == 0 {
                                    self.myEventsTableView.setEmptyMessage("Nothing found")
                                } else {
                                    self.myEventsTableView.restore()
                                }
                            }
                        }
                        else {
                            //Display msg
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast("No events found", duration: 3.0, position: .center)
                                if self.myEventsListFilter.count == 0 {
                                    self.myEventsTableView.setEmptyMessage("Nothing found")
                                } else {
                                    self.myEventsTableView.restore()
                                }
                            }
                        }
                    }
                    else
                    {
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                else
                {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
        
    }

    // MARK: - TableView DataSource and Delegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.myEventsListFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let myEventCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! MyEventsListCell
        
        myEventCell.containerView.layer.cornerRadius = 5
        myEventCell.containerView.layer.shadowColor = UIColor.lightGray.cgColor
        myEventCell.containerView.layer.shadowOpacity = 0.3
        myEventCell.containerView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        myEventCell.containerView.layer.shadowRadius = 4

        let event:Event = self.myEventsListFilter[indexPath.row]
        
        myEventCell.eventTitleLbl.text = event.eventTitle
        myEventCell.eventStatusLbl.text = event.participantStatus

        let startDate = event.eventStartDate!.toDate()!
        let startDateString = startDate.toString()
        myEventCell.eventDateLbl.text = startDateString

        if (event.postalCode == nil || event.postalCode == "") {
            myEventCell.directionBtn.isHidden = true
            myEventCell.arrowImgView.isHidden = true
        }else{
            myEventCell.directionBtn.isHidden = false
            myEventCell.arrowImgView.isHidden = false
        }

        myEventCell.directionBtn.tag = indexPath.row
        myEventCell.directionBtn.addTarget(self, action:#selector(actionDirectionBtnTapped(_:)), for: .touchUpInside)
        
        return myEventCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        IQKeyboardManager.shared.resignFirstResponder()
        let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "MyEventDetailViewController") as! MyEventDetailViewController
        vc.event = self.myEventsListFilter[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }

    // MARK: - UISearch result update delegate
    
    func updateSearchResults(for searchController: UISearchController) {
        
        if let searchText = self.searchController.searchBar.text {
            
            var resultArr:[Event] = []

            var arr2 = searchText.components(separatedBy: .whitespaces)
            arr2 = arr2.filter({ $0 != ""})
            
            let arr1 = self.myEventsList
            
            for (_, str1) in (arr1.enumerated()) {
                var counter = 0
                for (_, str2) in (arr2.enumerated()) {
                    
                    if ((str1.eventTitle?.lowercased().contains(string: str2.lowercased()))!){
                        counter += 1
                        if counter == arr2.count{
                            resultArr.append(str1)
                        }
                    }
                }
            }
            
            self.myEventsListFilter = searchText.trimmingCharacters(in: .whitespaces).isEmpty ? self.myEventsList : resultArr

            self.myEventsTableView.reloadData()
            if self.myEventsListFilter.count == 0 {
                self.myEventsTableView.setEmptyMessage("Nothing found")
            } else {
                self.myEventsTableView.restore()
            }

        }
    }
    
    // MARK: - CLLocationManager delegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    // do stuff
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UITableView {
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 120, width: self.bounds.size.width, height: self.bounds.size.height - 120))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "Arial", size: 20)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
    }
    
    func restore() {
        self.backgroundView = nil
    }
}

extension String {
    func contains(string: String)->Bool {
        guard !self.isEmpty else {
            return false
        }
        var s = self.map{ $0 }
        let c = string.map{ $0 }
        repeat {
            if s.starts(with:c){
                return true
            } else {
                s.removeFirst()
            }
        } while s.count > c.count - 1
        return false
    }
}
