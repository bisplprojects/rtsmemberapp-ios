//
//  EditMyDetailsViewController.swift
//  RTSPortal
//
//  Created by sajan on 04/09/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift

protocol MyDetailsDelegate {
    func didFinishUpdatingMyDetails(isUpdated:Bool)
}

class EditMyDetailsViewController: UIViewController,SelectionListViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    var myDetailsDelegate: MyDetailsDelegate?

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var editMyDetailsTableView: UITableView!
    @IBOutlet weak var updateBtn: UIButton!
    
    let CellIdentifier_Default = "MyDetailsTextCell"
    let CellIdentifier_DropDown = "MyDetailsDropDownCell"
    let CellIdentifier_Gender = "MyDetailsGenderCell"
    let CellIdentifier_UploadImg = "MyDetailsUploadImgCell"

    // Defining the tableview field tags
    let kFirstNameTag    = 500
    let kLastNameTag     = 501
    let kPrimaryEmailTag = 502
    let kProfessionTag   = 503
    let kJobTitleTag     = 504
    let kPlaceOfWorkTag  = 505
    let kGenderTag       = 506
    let kMobileTag       = 507
    let kBiographyTag    = 508
    let kPictureTag      = 509
    
    let EMPTY_STRING = ""
    
    var myDetails : User?

    var myDetailsTitleList:[String] = []
    var myDetailsFieldList:[MyDetailsField]?

    var profession:String?
    var professionId:String?
    
    var regImage : UIImage?
    var imageString:String = ""
    var isImageSelected:Bool = false

    var updateMyDetails:MyDetails = MyDetails()

    // MARK: -

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.containerView.layer.shadowColor = UIColor.gray.cgColor
        self.containerView.layer.shadowOpacity = 0.3
        self.containerView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        self.containerView.layer.shadowRadius = 4
        
        self.myDetailsTitleList = ["First Name","Last Name","Email","Profession","Job Title","Place of Work","Gender","Mobile","Biography","Picture"]

        if let tmp = self.myDetails?.myDetailsFields{
            self.myDetailsFieldList = tmp
        }

        self.profession = ""
        self.professionId = ""
        
        self.saveDataToMyDetailsModal()
        self.getImageFromModal()

    }
    
    func saveDataToMyDetailsModal()  {
        
        self.updateMyDetails.contactId = self.myDetails?.contactId
        
        guard self.myDetailsFieldList!.count > 1 else {
            return
        }
        for item in self.myDetailsFieldList! {
            let name = item.name
            switch name {
            case "first_name":
                self.updateMyDetails.firstName = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "last_name":
                self.updateMyDetails.lastName = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "email-Primary":
                self.updateMyDetails.primaryEmail = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_107":
                self.updateMyDetails.profession = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "job_title":
                self.updateMyDetails.jobTitle = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_213":
                self.updateMyDetails.placeOfWork = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "gender_id":
                self.updateMyDetails.gender = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "phone-Primary-2":
                self.updateMyDetails.mobile = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_221":
                self.updateMyDetails.biography = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "image_URL":
                self.updateMyDetails.picture = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break

            default:
                break
            }
            
        }
    }

    func getImageFromModal() {
        if (self.updateMyDetails.picture != ""){
            self.regImage = self.ConvertBase64StringToImage(imageBase64String: self.updateMyDetails.picture!)
            self.isImageSelected = (self.regImage != nil) ? true : false
        }
    }

    func ConvertImageToBase64String (img: UIImage) -> String {
        let imageData:NSData = img.jpegData(compressionQuality: 0.2)! as NSData
        
        let imageSize: Int = imageData.length
        print("size of image in KB: %f ", Double(imageSize) / 1024.0)

        let imgString = imageData.base64EncodedString(options: .init(rawValue: 0))
        return imgString
    }
    
    func ConvertBase64StringToImage (imageBase64String:String) -> UIImage {
        let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0))
        if imageData != nil{
            let image = UIImage(data: imageData!)
            return image!
        }else{
            return UIImage()
        }
    }

    
    // MARK: - UITextField delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.saveTextfieldString(textField: textField)
    }
    
    func saveTextfieldString(textField:UITextField)  {
        
        let strText = textField.text
        switch textField.tag {
        case kFirstNameTag:
            self.updateMyDetails.firstName = strText
            break
        case kLastNameTag:
            self.updateMyDetails.lastName = strText
            break
        case kPrimaryEmailTag:
            self.updateMyDetails.primaryEmail = strText
            break
        case kJobTitleTag:
            self.updateMyDetails.jobTitle = strText
            break
        case kPlaceOfWorkTag:
            self.updateMyDetails.placeOfWork = strText
            break
        case kMobileTag:
            self.updateMyDetails.mobile = strText
            break
        case kBiographyTag:
            self.updateMyDetails.biography = strText
            break

        default:
            break
        }
        
    }

    // MARK: - IBActions

    @IBAction func actionBackBtnTaped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionUpdateBtnTapped(_ sender: Any) {
        
        self.view.endEditing(true)
        
        var isValid:Bool?
        
        for formField in self.myDetailsFieldList!{
            
            if formField.isRequired == "1"{
                
                var validation:Valid = .success
                
                switch formField.name {
                    
                case "first_name":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationFirstName, self.updateMyDetails.firstName!))
                    break
                case "last_name":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationLastName, self.updateMyDetails.lastName!))
                    break
                case "email-Primary":
                    validation = Validation.shared.validate(values: (ValidationType.Email, self.updateMyDetails.primaryEmail!))
                    break
                case "custom_107":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationProfession, self.updateMyDetails.profession!))
                    break
                case "job_title":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationJobTitle, self.updateMyDetails.jobTitle!))
                    break
                case "custom_213":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationPlaceOfWork, self.updateMyDetails.placeOfWork!))
                    break
                case "gender_id":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationGender, self.updateMyDetails.gender!))
                    break
                case "phone-Primary-2":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationMobile, self.updateMyDetails.mobile!))
                    break
                case "custom_221":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationBiography, self.updateMyDetails.biography!))
                    break
                case "image_URL":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationPicture, self.updateMyDetails.picture!))
                    break
                    
                default:
                    break
                }
                
                switch validation {
                    
                case .success:
                    print("Validation Success")
                    isValid = true
                    break
                    
                case .failure(_, let message):
                    isValid = false
                    print(message.localized())
                    self.view.makeToast(message.localized(), duration: 3.0, position: .center)
                    return
                }
            }
        }
        
        if isValid == true {
            print("Final Validation Success")
            self.updateMyDetailsApiCallFunc()
        }
        else{
            print(" Validation failed")
        }

    }
    
    @objc func actionDropDownBtnTapped(_ sender: UIButton) {
        
        for formField in self.myDetails!.myDetailsFields {
            if (formField.title == "Profession" && formField.htmlType == "Select"){
                if formField.selectOptions.count > 0{
                    // Safe Present
                    if let vc = UIStoryboard(name: "Event", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectionListViewController") as? SelectionListViewController
                    {
                        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        vc.selectOptionList = formField.selectOptions
                        vc.selectionDelegate = self
                        present(vc, animated: true, completion: nil)
                    }
                    
                }
                else{
                    self.view.makeToast("No data", duration: 3.0, position: .center)
                }
            }
        }
        
    }

    @objc func actionGenderBtnTapped(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            self.updateMyDetails.gender = "1"
            break
        case 2:
            self.updateMyDetails.gender = "2"
            break
        case 3:
            self.updateMyDetails.gender = "3"
            break
        default:
            break
        }
        let indexPath = IndexPath(item: 6, section: 0)
        self.editMyDetailsTableView.reloadRows(at: [indexPath], with: .none)
        
    }

    @objc func actionChooseImageBtnTapped(_ sender: UIButton) {
        
        self.showImagePickerWithSourcetype(sourceType:.photoLibrary)
    }

    // MARK: - SelectionListView Delegate
    
    func didFinishSelectingItem(item: SelectOption) {
        
        self.profession = item.name
        self.professionId = item.optionId
        self.updateMyDetails.profession = self.professionId
        let indexPath = IndexPath(item: 3, section: 0)
        self.editMyDetailsTableView.reloadRows(at: [indexPath], with: .none)
    }

    // MARK: - Webservice

    func updateMyDetailsApiCallFunc() {
        self.updateBtn.isUserInteractionEnabled = false

        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        
        let fields = ["email-Primary": self.updateMyDetails.primaryEmail,
                      "first_name": self.updateMyDetails.firstName,
                      "last_name": self.updateMyDetails.lastName,
                      "job_title": self.updateMyDetails.jobTitle,
                      "custom_213": self.updateMyDetails.placeOfWork,
                      "gender_id": self.updateMyDetails.gender,
                      "custom_107": self.updateMyDetails.profession,
                      "phone-Primary-2": self.updateMyDetails.mobile,
                      "custom_221": self.updateMyDetails.biography,
                      "image_URL": self.updateMyDetails.picture];
        
        let fieldsString = JSONStringify(value: (fields as? [String : String])!)

        let param = ["entity" : "CiviMobileContact",
                     "action" : "updatedetails",
                     "token" : tokenString,
                     "cid" :  dict["ContactId"]!,
                     "fields" : fieldsString] as [String : Any];
        
        print("Update My Details request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Update My Details response : \(json)")
            if status
            {
                
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    
                    if dictonary["validuser"] == true{
                        
                        let dataDictonary = dictonary["data"]
                        
                        let msg = dataDictonary["message"].stringValue
                        if dataDictonary["status"] == true{
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                //  Display toast msg
                                let style = ToastStyle()
                                self.view?.makeToast(msg, duration: 2.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                                    self.updateUserData()
                                    // Move to My Details
                                    self.updateBtn.isUserInteractionEnabled = true
                                    self.myDetailsDelegate?.didFinishUpdatingMyDetails(isUpdated: true)
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }
                        }
                        else{
                            //Display msg
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast(msg, duration: 3.0, position: .center)
                                self.updateBtn.isUserInteractionEnabled = true
                            }
                        }

                    }
                    else{
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.view.makeToast("Could not update", duration: 3.0, position: .center)
                            self.updateBtn.isUserInteractionEnabled = true
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast("Could not update", duration: 3.0, position: .center)
                        self.updateBtn.isUserInteractionEnabled = true
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.updateBtn.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    func updateUserData()  {

        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        let userName = self.updateMyDetails.firstName! + " " + self.updateMyDetails.lastName!
        let userImageString = self.updateMyDetails.picture
        let sessionID = dict["sessionId"]
        let contactId = dict["ContactId"]
        let roleStr = dict["role"]
        let pinStr = dict["PIN"]

        let bgsUserDict = ["UserName": userName, "ContactId": contactId, "PIN" : pinStr, "sessionId" : sessionID, "role" : roleStr, "ImageString" : userImageString]
        UserDefaults.standard.setBGSUserData(dict: bgsUserDict as! [String : String])

    }
    
    // MARK: -
    
    func showImagePickerWithSourcetype(sourceType:UIImagePickerController.SourceType)  {
        
        if sourceType == .photoLibrary {
            
            let imagePicker = UIImagePickerController.init()
            imagePicker.sourceType = sourceType
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
            
        }
        else{
            
            let imagePicker = UIImagePickerController.init()
            imagePicker.sourceType = sourceType
            imagePicker.cameraDevice = .front
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var tempImage:UIImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        let imgData = NSData(data: (info[UIImagePickerController.InfoKey.originalImage] as! UIImage).jpegData(compressionQuality: 1)!)
        let imageSize: Int = imgData.length 
        print("actual size of image in KB: %f ", Double(imageSize) / 1024.0)
        
        tempImage = self.resizeImage(image: tempImage)

        self.regImage = tempImage
        
        if tempImage.imageAsset != nil {
            self.isImageSelected = true
        }
        let indexPath = IndexPath(item: 9, section: 0)
        self.editMyDetailsTableView.reloadRows(at: [indexPath], with: .none)
        
        self.imageString = ConvertImageToBase64String(img: tempImage)
        
        self.updateMyDetails.picture = self.imageString
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        
        print("cancelled")
        self.dismiss(animated: true, completion: nil)
    }

    //image compression 750, 1334
    func resizeImage(image: UIImage) -> UIImage {
        var actualHeight: Float = Float(image.size.height)
        var actualWidth: Float = Float(image.size.width)
        let maxHeight: Float = 1334.0
        let maxWidth: Float = 750.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.5
        //50 percent compression
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img?.jpegData(compressionQuality: CGFloat(compressionQuality))//UIImageJPEGRepresentation(img!,CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!)!
    }

    // MARK: - TapGesture Methods
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer) {
        if self.regImage != nil {
            let newImageView = UIImageView(image: self.regImage)
            newImageView.contentMode = .scaleAspectFit
            newImageView.frame = UIScreen.main.bounds
            newImageView.backgroundColor = .black
            newImageView.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
            newImageView.addGestureRecognizer(tap)
            self.view.addSubview(newImageView)
        }
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }

    // MARK: - Configure Cell

    func configureRegistrationCellWithIndexPath(indexPath: IndexPath, txtField:UITextField) {
        
        //        let textfield:UITextField = txtField
        txtField.delegate = self
        txtField.clearButtonMode = UITextField.ViewMode.whileEditing
        
        switch indexPath.row {
        case 0:
            self.configureTextField(textField: txtField, tag: kFirstNameTag, keyboardType: .default)
            break
        case 1:
            self.configureTextField(textField: txtField, tag: kLastNameTag, keyboardType: .default)
            break
        case 2:
            self.configureTextField(textField: txtField, tag: kPrimaryEmailTag, keyboardType: .emailAddress)
            break
        case 4:
            self.configureTextField(textField: txtField, tag: kJobTitleTag, keyboardType: .default)
            break
        case 5:
            self.configureTextField(textField: txtField, tag: kPlaceOfWorkTag, keyboardType: .default)
            break
        case 7:
            self.configureTextField(textField: txtField, tag: kMobileTag, keyboardType: .phonePad)
            break
        case 8:
            self.configureTextField(textField: txtField, tag: kBiographyTag, keyboardType: .default)
            break

        default:
            break
        }
        
    }
    
    func configureTextField(textField:UITextField, tag:Int, keyboardType:UIKeyboardType) {
        textField.keyboardType = keyboardType
        textField.tag = tag
        switch textField.tag {
        case kFirstNameTag:
            textField.placeholder = "First Name"
            textField.text = self.updateMyDetails.firstName?.count == 0 ? EMPTY_STRING : self.updateMyDetails.firstName
            break
        case kLastNameTag:
            textField.placeholder = "Last Name"
            textField.text = self.updateMyDetails.lastName?.count == 0 ? EMPTY_STRING : self.updateMyDetails.lastName
            break
        case kPrimaryEmailTag:
            textField.placeholder = "Email"
            textField.text = self.updateMyDetails.primaryEmail?.count == 0 ? EMPTY_STRING : self.updateMyDetails.primaryEmail
            break
        case kJobTitleTag:
            textField.placeholder = "Job Title"
            textField.text = self.updateMyDetails.jobTitle?.count == 0 ? EMPTY_STRING : self.updateMyDetails.jobTitle
            break
        case kPlaceOfWorkTag:
            textField.placeholder = "Place of Work"
            textField.text = self.updateMyDetails.placeOfWork?.count == 0 ? EMPTY_STRING : self.updateMyDetails.placeOfWork
            break
        case kMobileTag:
            textField.placeholder = "Mobile"
            textField.text = self.updateMyDetails.mobile?.count == 0 ? EMPTY_STRING : self.updateMyDetails.mobile
            break
        case kBiographyTag:
            textField.placeholder = "Biography"
            textField.text = self.updateMyDetails.mobile?.count == 0 ? EMPTY_STRING : self.updateMyDetails.biography
            break

        default:
            break
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EditMyDetailsViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.myDetailsTitleList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 3{
            let dropDownCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier_DropDown) as! RegDropDownCell
            
            for item in self.myDetailsFieldList!{
                if item.name == "custom_107"{
                    if (item.isRequired == "1"){
                        dropDownCell.titleLbl.text = self.myDetailsTitleList[indexPath.row] + "*"
                    }
                    else{
                        dropDownCell.titleLbl.text = self.myDetailsTitleList[indexPath.row]
                    }
                }
            }
            
            dropDownCell.txtField.placeholder = "Profession"
            dropDownCell.txtField.isEnabled = false
            if (self.updateMyDetails.profession != "" ) {
                //todo change variable to modal class
                for item in self.myDetailsFieldList!{
                    if item.name == "custom_107"
                    {
                        for opt in item.selectOptions{
                            if opt.optionId == self.updateMyDetails.profession{
                                self.profession = opt.name
                            }
                        }
                    }
                }
                dropDownCell.txtField.text = self.profession
            }
            
            dropDownCell.dropDownBtn.tag = indexPath.row
            dropDownCell.dropDownBtn.addTarget(self, action:#selector(actionDropDownBtnTapped(_:)), for: .touchUpInside)
            
            return dropDownCell
            
        }
        else if indexPath.row == 6{
            let genderCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier_Gender) as! RegGenderCell
            
            genderCell.femaleBtn.addTarget(self, action:#selector(actionGenderBtnTapped(_:)), for: .touchUpInside)
            genderCell.maleBtn.addTarget(self, action:#selector(actionGenderBtnTapped(_:)), for: .touchUpInside)
            genderCell.noneBtn.addTarget(self, action:#selector(actionGenderBtnTapped(_:)), for: .touchUpInside)
            
            let genderId = self.updateMyDetails.gender
            
            switch genderId{
                
            case "1":
                genderCell.femaleBtn.setImage(UIImage(named: "radio_on_icon"), for: .normal)
                genderCell.maleBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                genderCell.noneBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                break
            case "2":
                genderCell.femaleBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                genderCell.maleBtn.setImage(UIImage(named: "radio_on_icon"), for: .normal)
                genderCell.noneBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                break
            case "3":
                genderCell.femaleBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                genderCell.maleBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                genderCell.noneBtn.setImage(UIImage(named: "radio_on_icon"), for: .normal)
                break
                
            default:
                genderCell.femaleBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                genderCell.maleBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                genderCell.noneBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                
                break
            }
            
            return genderCell
            
        }
        else if indexPath.row == 9{
            
            let imgCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier_UploadImg) as! RegUploadImgCell
            
            for item in self.myDetailsFieldList!{
                if item.name == "image_URL"{
                    if (item.isRequired == "1"){
                        imgCell.titleLbl.text = self.myDetailsTitleList[indexPath.row] + "*"
                    }
                    else{
                        imgCell.titleLbl.text = self.myDetailsTitleList[indexPath.row]
                    }
                }
            }
            
            if self.regImage != nil{
                imgCell.imgView.image = self.regImage
            }
            
            imgCell.chooseFileBtn.tag = indexPath.row
            imgCell.chooseFileBtn.addTarget(self, action:#selector(actionChooseImageBtnTapped(_:)), for: .touchUpInside)
            
            imgCell.statusLbl.text = (self.isImageSelected) ? "" : "No file choosen"
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
            imgCell.imgView.isUserInteractionEnabled = true
            imgCell.imgView.addGestureRecognizer(tapGestureRecognizer)
            
            return imgCell

        }
        else{
            let regTextCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier_Default) as! RegTextCell
            
            self.configureRegistrationCellWithIndexPath(indexPath: indexPath, txtField: regTextCell.txtField)
            regTextCell.descLbl.text = ""

            regTextCell.titleLbl.text = self.myDetailsTitleList[indexPath.row]
            
            switch indexPath.row{

            case 0:
                for item in self.myDetailsFieldList!{
                    if item.name == "first_name"{
                        if (item.isRequired == "1"){
                            regTextCell.titleLbl.text = self.myDetailsTitleList[indexPath.row] + "*"
                        }
                        else{
                            regTextCell.titleLbl.text = self.myDetailsTitleList[indexPath.row]
                        }
                    }
                }
                break
            case 1:
                for item in self.myDetailsFieldList!{
                    if item.name == "last_name"{
                        if (item.isRequired == "1"){
                            regTextCell.titleLbl.text = self.myDetailsTitleList[indexPath.row] + "*"
                        }
                        else{
                            regTextCell.titleLbl.text = self.myDetailsTitleList[indexPath.row]
                        }
                    }
                }
                regTextCell.txtField.smartQuotesType = .no
                break
            case 2:
                for item in self.myDetailsFieldList!{
                    if item.name == "email-Primary"{
                        if (item.isRequired == "1"){
                            regTextCell.titleLbl.text = self.myDetailsTitleList[indexPath.row] + "*"
                        }
                        else{
                            regTextCell.titleLbl.text = self.myDetailsTitleList[indexPath.row]
                        }
                    }
                }
                break
            case 4:
                for item in self.myDetailsFieldList!{
                    if item.name == "job_title"{
                        if (item.isRequired == "1"){
                            regTextCell.titleLbl.text = self.myDetailsTitleList[indexPath.row] + "*"
                        }
                        else{
                            regTextCell.titleLbl.text = self.myDetailsTitleList[indexPath.row]
                        }
                    }
                }
                break
            case 5:
                for item in self.myDetailsFieldList!{
                    if item.name == "custom_213"{
                        if (item.isRequired == "1"){
                            regTextCell.titleLbl.text = self.myDetailsTitleList[indexPath.row] + "*"
                        }
                        else{
                            regTextCell.titleLbl.text = self.myDetailsTitleList[indexPath.row]
                        }
                    }
                }
                break
            case 7:
                for item in self.myDetailsFieldList!{
                    if item.name == "phone-Primary-2"{
                        if (item.isRequired == "1"){
                            regTextCell.titleLbl.text = self.myDetailsTitleList[indexPath.row] + "*"
                        }
                        else{
                            regTextCell.titleLbl.text = self.myDetailsTitleList[indexPath.row]
                        }
                    }
                }
                break
            case 8:
                for item in self.myDetailsFieldList!{
                    if item.name == "custom_221"{
                        if (item.isRequired == "1"){
                            regTextCell.titleLbl.text = self.myDetailsTitleList[indexPath.row] + "*"
                        }
                        else{
                            regTextCell.titleLbl.text = self.myDetailsTitleList[indexPath.row]
                        }
                    }
                }
                regTextCell.txtField.smartQuotesType = .no
                break
            default:
                break
                
            }
            
            return regTextCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
