//
//  AddressCell.swift
//  RTSPortal
//
//  Created by sajan on 04/09/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

class AddressCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var streetNameLbl: UILabel!
    @IBOutlet weak var supAddress1Lbl: UILabel!
    @IBOutlet weak var aupAddress2Lbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var postCodeLbl: UILabel!
    @IBOutlet weak var stateLbl: UILabel!
    @IBOutlet weak var countryLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
