//
//  MyDetailHeaderCell.swift
//  RTSPortal
//
//  Created by sajan on 04/09/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

class MyDetailHeaderCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var jobTitleLbl: UILabel!
    @IBOutlet weak var placeOfWorkLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profileImgView.contentMode = .scaleAspectFill
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        profileImgView.layer.cornerRadius =  profileImgView.frame.width / 2
        profileImgView.layer.borderWidth = 0.5
        profileImgView.layer.borderColor = UIColor.white.cgColor
        profileImgView.layer.masksToBounds = true
    }

}
