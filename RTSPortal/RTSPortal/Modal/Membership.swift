//
//  Member.swift
//  RTSPortal
//
//  Created by sajan on 23/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SwiftyJSON

class Membership: NSObject {
    
    var membershipId : String?
    var membershipName : String?
    var membershipStatus : String?
    var membershipStartDate : String?
    var membershipEndDate : String?
    var membershipJoinDate : String?
    var membershipDurationUnit : String?
    var membershipDurationInterval : String?
    var membershipminimumFee : String?

    init(dict:[String : JSON]) {
        
        self.membershipId = dict["mid"]?.stringValue
        self.membershipName = dict["membership_name"]?.stringValue
        self.membershipStatus = dict["status"]?.string
        self.membershipStartDate = dict["start_date"]?.string
        self.membershipEndDate = dict["end_date"]?.string
        self.membershipJoinDate = dict["join_date"]?.stringValue
        self.membershipDurationUnit = dict["duration_unit"]?.stringValue
        self.membershipDurationInterval = dict["duration_interval"]?.string
        self.membershipminimumFee = dict["minimum_fee"]?.string
    }

}
/*
 {
 "mid": "9621",
 "start_date": "2019-01-01",
 "end_date": "2019-12-31",
 "join_date": "2019-01-01",
 "status": "Current",
 "duration_unit": "year",
 "duration_interval": "1",
 "minimum_fee": "205.00",
 "membership_name": "Category A Consultant \/ Senior Doctor"
 } */
