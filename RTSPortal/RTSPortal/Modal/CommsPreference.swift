//
//  CommsPreference.swift
//  RTSPortal
//
//  Created by sajan on 27/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SwiftyJSON

class CommsPreference: NSObject {

    var title:String?
    var tcLabel:String?
    var tcUrl:String?
    var groups = [Groups]()
    
    init(dict:[String : JSON]) {
        self.title = dict["title"]?.stringValue
        self.tcLabel = dict["tc_label"]?.stringValue
        self.tcUrl = dict["tc_url"]?.stringValue
        
        if dict["groups"]!.arrayValue.count > 0{
            for grp in (dict["groups"]?.array)! {
                let group = Groups(dict: grp.dictionaryValue)
                self.groups.append(group)
            }
        }
    }
    
}

class Groups: NSObject {
    
    var preferenceId : String?
    var preferenceName : String?
    var preferenceTitle : String?
    var preferenceIsAdded : Int?
    
    init(dict:[String : JSON]) {
        self.preferenceId = dict["id"]?.stringValue
        self.preferenceName = dict["name"]?.stringValue
        self.preferenceTitle = dict["title"]?.stringValue
        self.preferenceIsAdded = dict["is_added"]?.intValue
    }

}

/*
 {
 "id" : "60",
 "name" : "E_Bulletin_60",
 "is_added" : 0,
 "title" : "E-Bulletin"
 }
 */
