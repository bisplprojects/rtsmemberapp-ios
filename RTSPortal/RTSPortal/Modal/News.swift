//
//  News.swift
//  RTSPortal
//
//  Created by sajan on 24/02/20.
//  Copyright © 2020 BISPL. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON

class News: NSObject {
    
    var newsId : String?
    var displayOrderID : String?
    var newsTitle : String?
    var newsSummary : String?
    var newsCategory : String?
    var newsCreatedDate : String?
    var newsImageUrl : String?
    var newsColor : String?

    var newsIntroductoryText : String?
    var newsBody : String?
    
    
    override init() {
        super.init()
    }

    init(dict:[String : JSON]) {
        
        self.newsId = dict["nid"]?.stringValue
        self.displayOrderID = dict["sort_order"]?.stringValue
        self.newsTitle = dict["title"]?.stringValue
        self.newsSummary = (dict["summary"]?.string != nil) ? dict["summary"]?.string : ""
        self.newsCategory = dict["category"]?.string
        self.newsCreatedDate = dict["created_date"]?.string
        self.newsImageUrl = dict["image_url"]?.string
        self.newsColor = dict["color"]?.string
        self.newsIntroductoryText = (dict["introductory_text"]?.string != nil) ? dict["introductory_text"]?.string : ""

    }

    init(detailDict:[String : JSON]) {
        
        self.newsId = detailDict["nid"]?.stringValue
        self.newsTitle = detailDict["title"]?.stringValue
        self.newsCreatedDate = detailDict["created_date"]?.string
        self.newsImageUrl = detailDict["image_url"]?.string
        self.newsIntroductoryText = (detailDict["introductory_text"]?.string != nil) ? detailDict["introductory_text"]?.string : ""
        self.newsBody = (detailDict["body"]?.string != nil) ? detailDict["body"]?.string : ""
    }

}

/*
 {
     "title": "Alibi acquires US crime dramas Waco and I Am the Night  ",
     "category": "In Brief",
     "summary": "<p>UKTV’s crime drama channel Alibi has acquired the hit Emmy-nominated US mini-series <em>Waco,<\/em> staring Taylor Kitsch (<em>True Detective<\/em>, <em>Friday Night Lights<\/em>), and Michael Shannon (<em>Little Drummer Girl<\/em>, <em>Shape of Water<\/em>). <\/p>\n",
     "created_date": "1558362040",
     "image_url": "https:\/\/rts-staging.vedacrm.co.uk\/\/sites\/default\/files\/styles\/landing-page-main-listing\/public\/waco_image_compressed.jpg?itok=7qiNouNi",
     "nid": "7268",
     "color": "#2959a9"
 }
 */

/*
 {
        "nid": "7268",
        "title": "Alibi acquires US crime dramas Waco and I Am the Night  ",
        "created_date": "1558362040",
        "introductory_text": "<p>UKTV’s crime drama channel Alibi has acquired the hit Emmy-nominated US mini-series <em>Waco,<\/em> staring Taylor Kitsch (<em>True Detective<\/em>, Drummer Girl<\/em>, <em>Shape of Water<\/em>).&nbsp;<\/p>\r\n",
        "image_url": "https:\/\/rts-staging.vedacrm.co.uk\/\/sites\/default\/files\/waco_image_compressed.jpg",
        "body": "<p>The drama series focuses on the 51-day gun battle – the longest in U.S. law s Why<\/em>, <em>Spotlight)<\/em> serving as executive producers. &nbsp;<\/p>\r\n\r\n<p><em>I Am the Night<\/em> comes to Alibi later this year.<\/p>\r\n"
    }
 */
