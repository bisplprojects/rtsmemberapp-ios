//
//  Paticipant.swift
//  RTSPortal
//
//  Created by sajan on 18/09/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SwiftyJSON

class Paticipant: NSObject {
    
    var participantId : String?
    var participantRole : String?
    var statusClass : String?
    var participantDisplayName : String?
    var participantImageURL : String?
    var message : String?
    
    var isAttended : String?
    var multiEntryEvent : String?
    var lastAttendedDate : String?
    var eventTitle : String?
    var status : String?
    var registerDate : String?
    var startDate : String?
    var endDate : String?

    var eventID : String?

    var ticketNumber : String?
    var ticketDisplayName : String?
    var tID : String?
    var scannedTime : String?
    var isScanned : String?

    override init() {
        super.init()
    }

    init(participantStatusDict:[String : JSON]) {
        
        self.participantId = participantStatusDict["pid"]?.stringValue
        self.participantRole = participantStatusDict["role"]?.stringValue
        self.statusClass = participantStatusDict["status_class"]?.string
        self.participantDisplayName = participantStatusDict["display_name"]?.string
        self.participantImageURL = participantStatusDict["image_URL"]?.string
        
    }
    
        
    init(participantDetailsDict:[String : JSON]) {
        
        self.participantRole = participantDetailsDict["role"]?.stringValue
        self.statusClass = participantDetailsDict["status_class"]?.string
        self.participantDisplayName = participantDetailsDict["display_name"]?.string
        self.participantImageURL = participantDetailsDict["image_URL"]?.string

        self.isAttended = participantDetailsDict["is_attended"]?.stringValue
        self.multiEntryEvent = participantDetailsDict["multi_entry_event"]?.stringValue
        self.lastAttendedDate = participantDetailsDict["last_attended_date"]?.string
        self.eventTitle = participantDetailsDict["event_title"]?.string
        self.status = participantDetailsDict["status"]?.string
        self.registerDate = participantDetailsDict["register_date"]?.string
        self.startDate = participantDetailsDict["start_date"]?.string
        self.endDate = participantDetailsDict["end_date"]?.string
        self.participantId = participantDetailsDict["pid"]?.stringValue
        self.eventID = participantDetailsDict["event_id"]?.string
    }
    

    init(participantsRegisteredList:[String : JSON]) {
        
        self.participantId = participantsRegisteredList["pid"]?.stringValue
        self.participantDisplayName = participantsRegisteredList["participant_name"]?.stringValue
        self.ticketNumber = (participantsRegisteredList["ticket_number"]?.stringValue != nil) ? participantsRegisteredList["ticket_number"]?.stringValue : ""
        self.ticketDisplayName = (participantsRegisteredList["ticket_display_name"]?.stringValue != nil) ? participantsRegisteredList["ticket_display_name"]?.stringValue : ""
        self.tID = participantsRegisteredList["tid"]?.stringValue
        self.scannedTime = participantsRegisteredList["scanned_time"]?.stringValue
        self.isScanned = participantsRegisteredList["is_scanned"]?.stringValue

    }

    init(participantTicketDetails:[String : JSON]) {
        
        self.participantId = participantTicketDetails["pid"]?.stringValue
        self.participantDisplayName = participantTicketDetails["participant_name"]?.stringValue
        self.ticketNumber = participantTicketDetails["ticket_number"]?.stringValue
        self.ticketDisplayName = participantTicketDetails["ticket_display_name"]?.stringValue
        self.tID = participantTicketDetails["tid"]?.stringValue
        self.scannedTime = participantTicketDetails["scanned_time"]?.stringValue
        self.isScanned = participantTicketDetails["is_scanned"]?.stringValue
        self.status = participantTicketDetails["status"]?.string
        self.statusClass = participantTicketDetails["status_class"]?.string

    }

}

/*
 {
   "ticket_number" : "025831-003",
   "ticket_display_name" : "Reg By : Mr Mv Ticket Test Test 2301",
   "participant_name" : "Mr Mv Ticket Test Test 2301",
   "tid" : "t2u26413743434s264v2",
   "scanned_time" : "",
   "id" : "v2x294w254",
   "is_scanned" : "0"
 }
 */
