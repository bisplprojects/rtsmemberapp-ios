//
//  MyDetails.swift
//  RTSPortal
//
//  Created by sajan on 04/09/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SwiftyJSON

class MyDetails: NSObject {
    
    var contactId : String?
    
    var primaryEmail : String?
    var firstName : String?
    var lastName : String?
    var profession : String?
    var professionId : String?
    var jobTitle : String?
    var placeOfWork : String?
    var gender : String?
    var mobile : String?
    var biography : String?
    var picture : String?
    
    override init() {
        self.primaryEmail = ""
        self.firstName = ""
        self.lastName = ""
        self.profession = ""
        self.professionId = ""
        self.jobTitle = ""
        self.placeOfWork = ""
        self.primaryEmail = ""
        self.primaryEmail = ""
        self.biography = ""
        self.picture = ""
    }
}
