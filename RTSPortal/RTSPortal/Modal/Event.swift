//
//  Event.swift
//  RTSPortal
//
//  Created by sajan on 18/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON

class Event: NSObject {

    var eventId : String?
    var eventTitle : String?
    var eventStartDate : String?
    var participantId : String?
    var participantStatus : String?
    var postalCode : String?
    
    var eventEndDate : String?
    var participantRegisterDate : String?
    var participantRole : String?
    var qrCodeURL : String?
    var eventSummary : String?
    var eventDescription : String?
    
    var eventCancellationBtnText : String?
    var eventCancellationText : String?
    
    var eventRoleId : String?
    var alreadyRegistered : String?
    
    var isPayLater : String?
    var isMonetary : String?

    
    var sessionChoices : [String]?
    
    //Event Registration choose session
    var priceFields  = [PriceField]()
    var formFields  = [FormField]()
    
    override init() {
        super.init()
    }

    init(dict:[String : JSON]) {
        
        self.eventId = dict["event_id"]?.stringValue
        self.eventTitle = dict["event_title"]?.stringValue
        self.eventStartDate = dict["event_start_date"]?.string
        self.participantId = dict["pid"]?.string
        self.participantStatus = dict["participant_status"]?.string
        self.postalCode = dict["postal_code"]?.string
        
    }

    init(eventDetailsDict:[String : JSON]) {
        
        self.eventId = eventDetailsDict["event_id"]?.stringValue
        self.eventTitle = eventDetailsDict["event_title"]?.stringValue
        self.eventStartDate = eventDetailsDict["event_start_date"]?.string
        self.participantId = eventDetailsDict["pid"]?.string
        self.participantStatus = eventDetailsDict["participant_status"]?.string
        self.postalCode = eventDetailsDict["postal_code"]?.string
        self.eventEndDate = eventDetailsDict["event_end_date"]?.string
        self.participantRegisterDate = eventDetailsDict["participant_register_date"]?.string
        self.participantRole = eventDetailsDict["participant_role"]?.string
        self.qrCodeURL = eventDetailsDict["qrcode"]?.string
        
        self.eventSummary = (eventDetailsDict["event_summary"]?.string != nil) ? eventDetailsDict["event_summary"]?.string : ""
        self.eventDescription = (eventDetailsDict["event_description"]?.string != nil) ? eventDetailsDict["event_description"]?.string : ""

        self.sessionChoices = eventDetailsDict["session_choices"]?.arrayObject as? [String]
        
        if eventDetailsDict["cancellation"]?.dictionaryValue != nil {
            self.eventCancellationText = eventDetailsDict["cancellation"]!["text"].stringValue
            self.eventCancellationBtnText = eventDetailsDict["cancellation"]!["button"].stringValue
        }
        else{
            self.eventCancellationText = ""
            self.eventCancellationBtnText = ""
        }
        
        if eventDetailsDict["amend_sessions"]!.arrayValue.count > 0{
            for price in (eventDetailsDict["amend_sessions"]?.array)! {
                let priceField = PriceField(dict: price.dictionaryValue)
                self.priceFields.append(priceField)
            }
        }
        
//        self.priceFieldsArray = allEventDetailsDict["price_fields"]?.arrayObject


    }

    init(allEventdict:[String : JSON]) {
        
        self.eventId = allEventdict["event_id"]?.stringValue
        self.eventTitle = allEventdict["event_title"]?.stringValue
        self.eventStartDate = allEventdict["event_start_date"]?.string
        self.postalCode = allEventdict["postal_code"]?.string
        
    }

    init(allEventDetailsDict:[String : JSON]) {
        
        self.eventId = allEventDetailsDict["event_id"]?.stringValue
        self.eventTitle = allEventDetailsDict["event_title"]?.stringValue
        self.eventStartDate = allEventDetailsDict["event_start_date"]?.string
        self.participantId = allEventDetailsDict["pid"]?.string
        self.postalCode = allEventDetailsDict["postal_code"]?.string
        self.eventEndDate = allEventDetailsDict["event_end_date"]?.string
        self.eventSummary = allEventDetailsDict["event_summary"]?.string
        self.eventDescription = allEventDetailsDict["event_description"]?.string
        self.eventRoleId = allEventDetailsDict["role_id"]?.string
        self.alreadyRegistered = (allEventDetailsDict["already_registered"]?.intValue == 1) ? "1" : "0"

        self.isPayLater = allEventDetailsDict["is_pay_later"]?.string
        self.isMonetary = allEventDetailsDict["is_monetary"]?.string

        
//        if allEventDetailsDict["price_fields"]!.arrayValue.count > 0{
//            for price in (allEventDetailsDict["price_fields"]?.array)! {
//                let priceField = PriceField(dict: price.dictionaryValue)
//                self.priceFields.append(priceField)
//            }
//        }
//
//        self.priceFieldsArray = allEventDetailsDict["price_fields"]?.arrayObject

        if allEventDetailsDict["fields"]!.arrayValue.count > 0{
            for formFieldItem in (allEventDetailsDict["fields"]?.array)! {
                let formField = FormField(dict: formFieldItem.dictionaryValue)
                self.formFields.append(formField)
            }
        }

    }

    
}


/*
 {
 "event_id": "179",
 "event_title": "2020 Trainees Weekend",
 "event_start_date": "2020-02-07 09:00:00",
 "pid": "3484w2d4r2",
 "participant_status": "Registered",
 "postal_code": "E3 2PX"
 }
*/

/*
 {
 "pid": "12481",
 "event_id": "179",
 "event_title": "2020 Trainees Weekend",
 "event_start_date": "2020-02-07 09:00:00",
 "event_end_date": "2020-02-08 16:00:00",
 "participant_register_date": "2019-07-16 17:36:00",
 "participant_status": "Registered",
 "participant_role": "Attendee",
 "postal_code": "RG1 4PS",
 "qrcode": "<img src=\"https:\/\/bgs1.vedacrm.co.uk\/sites\/default\/files\/civicrm\/persist\/contribute\/eventregistration_12481.png\" alt=\"\" border=\"0\">",
 "event_summary": "This annual 2 day weekend meeting is built around the current curriculum needs and experiences of junior doctors training for a career in geriatric Medicine.",
 "event_description": "<h4>Who should attend:<\/h4>\r\n\r\n<p>The meeting will be of benefit to all registrars training in geriatric medicine, and is targeted at them.<\/p>\r\n\r\n<p>It may also benefit other specialist trainees who will come into regular contract with older people and are interested in the specialty:<\/p>\r\n\r\n<ul>\r\n\t<li>Registrars in Geriatric Medicine &amp; General Internal Medicine<\/li>\r\n\t<li>Junior doctors training in related specialties<\/li>\r\n\t<li>GP trainees<\/li>\r\n\t<li>Core medical trainees considering a career in geriatric medicine<\/li>\r\n<\/ul>\r\n\r\n<h4>Why participate:<\/h4>\r\n\r\n<ul>\r\n\t<li>Understand more on the key areas &amp; challenges faced in geriatric medicine<\/li>\r\n\t<li>Hear focused presentations on all main areas of the geriatrics curriculum, &amp; the latest scientific research from experienced British Geriatrics Society members and elected officers<\/li>\r\n\t<li>Opportunity to sit mock SCE exam or attend practice consultant interviews with experienced interviewers<\/li>\r\n\t<li>Small group sessions to target lesser covered curriculum area<\/li>\r\n\t<li>Network &amp; socialise with other trainees, consultants and other healthcare professionals over 2 days<\/li>\r\n<\/ul>\r\n\r\n<p>&nbsp;<\/p>"
 }
*/


/*
 {
 "event_title" : "2019 Spring Meeting",
 "postal_code" : "E3 2PX",
 "event_start_date" : "2019-04-10 09:00:00",
 "event_id" : "17"
 }
*/

/*
 {
 "event_end_date" : "2016-11-25 15:00:00",
 "event_title" : "British Geriatrics Society Autumn Meeting 2016",
 "postal_code" : "RG1 4PS",
 "pid" : 0,
 "role_id" : "Attendee",
 "event_description" : "",
 "event_start_date" : "2016-11-23 08:00:00",
 "event_id" : "27",
 "event_summary" : null,
 "already_registered" : 0
 }
*/
