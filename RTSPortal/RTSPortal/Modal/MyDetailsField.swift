//
//  MyDetailsField.swift
//  RTSPortal
//
//  Created by sajan on 04/09/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SwiftyJSON

class MyDetailsField: NSObject {
    
    var fieldId : String?
    var title : String?
    var name : String?
    var defaultValue : Array<Any>?
    var htmlType : String?
    var isRequired : String?
    
    var selectOptions = [SelectOption]()
    
    var helpPost : String?
    var helpPre : String?

    init(dict:[String : JSON]) {
        
        self.title = dict["title"]?.stringValue
        self.name = dict["name"]?.stringValue
        self.defaultValue = dict["default_value"]?.arrayObject
        self.htmlType = dict["html_type"]?.string
        self.isRequired = dict["is_required"]?.string
        self.fieldId = dict["field_id"]?.string
        
        self.helpPost = dict["help_post"]?.string
        self.helpPre = dict["help_pre"]?.string
        
        if (dict["html_type"]?.string == "Select" && dict["name"]?.string == "custom_107"){
            if dict["options"]!.arrayValue.count > 0{
                for opt in (dict["options"]?.array)! {
                    let optn = SelectOption(dict: opt.dictionaryValue)
                    self.selectOptions.append(optn)
                }
            }
        }
        
    }

}

/*
 {
 "is_required" : "1",
 "default_value" : [
 "appleteam@vedaconsulting.co.uk"
 ],
 "data_type" : "String",
 "field_id" : "313",
 "help_post" : null,
 "html_type" : "Text",
 "name" : "email-Primary",
 "title" : "Email (Primary)",
 "help_pre" : null
 },
*/
