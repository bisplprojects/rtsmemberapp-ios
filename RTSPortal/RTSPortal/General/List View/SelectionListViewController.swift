//
//  SelectionListViewController.swift
//  RTSPortal
//
//  Created by sajan on 07/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

protocol SelectionListViewDelegate {
    func didFinishSelectingItem(item:SelectOption)
}

class SelectionListViewController: UIViewController,UIGestureRecognizerDelegate {

    var selectionDelegate: SelectionListViewDelegate?

    let cellIdentifier = "ListViewCell"

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var listTableView: UITableView!
    
    var selectOptionList:[SelectOption]?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backViewTapped(_:)))
        tapGestureRecognizer.delegate = self
        self.backView.addGestureRecognizer(tapGestureRecognizer)

    }
    
    // MARK: - TapGesture Methods
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        // Prevent subviews of a specific view to send touch events to the view's gesture recognizers.
        if let touchedView = touch.view, let gestureView = gestureRecognizer.view, touchedView.isDescendant(of: gestureView), touchedView !== gestureView {
            return false
        }
        return true
    }

    @objc func backViewTapped(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SelectionListViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.selectOptionList!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let listViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ListViewCell
        let listItem:SelectOption = self.selectOptionList![indexPath.row]
        listViewCell.titleLbl.text = listItem.name
        
        return listViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let listItem:SelectOption = self.selectOptionList![indexPath.row]
        self.selectionDelegate?.didFinishSelectingItem(item: listItem)
        self.dismiss(animated: true, completion: nil)
    }
}
