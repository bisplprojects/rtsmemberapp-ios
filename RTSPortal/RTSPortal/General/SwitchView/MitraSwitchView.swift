//
//  MitraSwitchView.swift
//  RTSPortal
//
//  Created by Bolas intelli Solutions on 27/08/19.
//  Copyright © 2019 Bolas intelli Solutions. All rights reserved.
//

import UIKit

let SWITCHVIEW_WIDTH = 65
let SWITCHVIEW_HEIGHT = 10 // height = (width/2-10)

let BACK_GROUND_COLOR  = UIColor.init(red: 161/255.0, green: 158/255.0, blue: 159/255.0, alpha: 1)
let OFF_COLOR: UIColor = UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1) // UIColor.lightGray
let ON_COLOR: UIColor  = UIColor.init(red: 59/255.0, green: 107/255.0, blue: 241/255.0, alpha: 1) // UIColor.black

let BACK_GROUND_COLOR_ON  = UIColor.init(red: 152/255.0, green: 192/255.0, blue: 234/255.0, alpha: 1)
let BACK_GROUND_COLOR_OFF  = UIColor.init(red: 161/255.0, green: 158/255.0, blue: 159/255.0, alpha: 1)

let OFF_TEXT_COLOR: UIColor = UIColor.black
let ON_TEXT_COLOR: UIColor = UIColor.init(red: 251/255.0, green: 223/255.0, blue: 50/255.0, alpha: 1)

let ON_TEXT = "ON" ;
let OFF_TEXT = "OFF";

enum MTSwithType{
    case MTSWithType_with_letter
    case MTSWithType_small
}
enum MTSwithState{
    case ON
    case OFF
}

protocol MTSwitchViewDelegate:class {

    func switchView(switchView:MitraSwitchView, stateChanged switchState:MTSwithState)
    
}

class MitraSwitchView: UIView {

    weak var delegate: MTSwitchViewDelegate?
    var onStateColor : UIColor? , offStateColor :UIColor?
    private(set) var currentState:MTSwithState
    
    
    var ONImage:UIImage? ,OFFImage:UIImage?
    var sliderView:UIView?
    var ONView:UILabel? , OFFView: UILabel?
    var swipeGesture: UISwipeGestureRecognizer?
    var tapGesture :UITapGestureRecognizer?
//    var switchState:MTSwithState?
//    var switchType:MTSwithType?

    var offStateFrame:CGRect? , onStateFrame:CGRect?

    init(withFrame frame:CGRect, initialState state:MTSwithState){
        
        currentState = state
        super.init(frame: frame)
        self.initialSetUP()

    }
    
    required init?(coder aDecoder: NSCoder) {
        
        self.currentState =  .OFF
        super.init(coder:aDecoder)
        self.initialSetUP()
        
    }
    
    func initialSetUP()  {
        
        let height:CGFloat = CGFloat(SWITCHVIEW_HEIGHT) + 10 //  CGFloat( SWITCHVIEW_HEIGHT ) - 5.0
        let width :CGFloat = height
        let originY:CGFloat = (self.frame.size.height/2 - height/2)
//        offStateFrame =  CGRect(x: self.frame.size.width - width, y: originY, width: width, height: height)
//        onStateFrame = CGRect(x: 0, y: originY, width: width, height: height)

        offStateFrame =  CGRect(x: 0, y: originY, width: width, height: height)
        onStateFrame = CGRect(x: self.frame.size.width - width, y: originY, width: width, height: height)

        sliderView = UIView.init(frame: onStateFrame!)
        sliderView?.backgroundColor = UIColor.white
        sliderView?.layer.cornerRadius = height/2;
        
        
//        sliderView?.layer.borderWidth = 1
//        sliderView?.layer.borderColor = UIColor.lightGray.cgColor
        sliderView?.layer.shadowColor = UIColor.gray.cgColor
        sliderView?.layer.shadowOpacity = 0.5
        sliderView?.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        sliderView?.layer.shadowRadius = 3

        
        self.layer.cornerRadius = self.frame.size.height/2;

        ONView = UILabel.init(frame: CGRect(x: CGFloat(SWITCHVIEW_WIDTH)/3, y: 0.0, width: CGFloat( SWITCHVIEW_WIDTH-(SWITCHVIEW_WIDTH/3)), height: CGFloat(SWITCHVIEW_HEIGHT)))
        ONView?.textColor = ON_TEXT_COLOR;
        ONView?.textAlignment = .center;
        ONView?.font = UIFont.boldSystemFont(ofSize: CGFloat((SWITCHVIEW_HEIGHT + 8) / 2))

        OFFView = UILabel.init(frame: CGRect(x: 0, y: 0, width: SWITCHVIEW_WIDTH-(SWITCHVIEW_WIDTH/3), height: SWITCHVIEW_HEIGHT))
        OFFView?.textColor = OFF_TEXT_COLOR;
        OFFView?.textAlignment = .center ;
        OFFView?.font = ONView!.font;

        
//        switch(self.switchType){
//
//        case .MTSWithType_with_letter:
//
//            ONView?.text = ON_TEXT ;
//            OFFView?.text = OFF_TEXT;
//         break
//
//        }

        self.addSubview(OFFView!)
        self.addSubview(ONView!)
        self.addSubview(sliderView!)
    
    
        swipeGesture = UISwipeGestureRecognizer.init(target: self, action:#selector(MitraSwitchView.swipeGestureEnabled(gesture:)) )

        swipeGesture?.direction = UISwipeGestureRecognizer.Direction(rawValue: UISwipeGestureRecognizer.Direction.RawValue(UInt8(UISwipeGestureRecognizer.Direction.left.rawValue) | UInt8(UISwipeGestureRecognizer.Direction.right.rawValue))) ;

        sliderView?.addGestureRecognizer(swipeGesture!)

        tapGesture = UITapGestureRecognizer.init(target: self, action:#selector(MitraSwitchView.tapGestureEnabled(gesture:)) )

        tapGesture?.numberOfTapsRequired = 1;
        self.addGestureRecognizer(tapGesture!)

        self.backgroundColor = BACK_GROUND_COLOR;

        self.changeToSwitchState(state:self.currentState, animated: false)
    
    }
    
    func changeToSwitchState(state: MTSwithState, animated: Bool) {
    
        var sliderFrame:CGRect = sliderView!.frame;
        switch (state) {
            
        case .ON:
            
        currentState = .ON ;
//        ONView?.text = ON_TEXT;
//        OFFView?.text = "";
        self.backgroundColor = BACK_GROUND_COLOR_ON;
        
        sliderView?.backgroundColor = ON_COLOR
        sliderFrame.origin = (onStateFrame?.origin)! ;
        
        case .OFF:
            
        currentState = .OFF ;
        sliderFrame.origin = (offStateFrame?.origin)! ;
//        OFFView?.text = OFF_TEXT;
//        ONView?.text = "";
        self.backgroundColor = BACK_GROUND_COLOR_OFF;
        
        sliderView?.backgroundColor = OFF_COLOR

        
        break;
        
        }
        if(animated) {
        
            UIView.animate(withDuration: 0.5, animations: {
                
                self.sliderView?.frame = sliderFrame;

            }, completion: { completed  in
                
               self.delegate?.switchView(switchView: self, stateChanged: state)
                
            })
            
        }else{
            
         self.sliderView?.frame = sliderFrame;
        }

    }
    
    
    // MARK: GESTURE METHODS
    
    @objc func swipeGestureEnabled(gesture:UISwipeGestureRecognizer) {
    
        currentState = (currentState == .OFF) ? .ON : .OFF

        self.changeToSwitchState(state:currentState, animated: true)
    }
    
    @objc func tapGestureEnabled(gesture:UITapGestureRecognizer) {
    
        currentState = (currentState == .OFF) ? .ON: .OFF

        self.changeToSwitchState(state:currentState, animated: true)
    }
    
    
//    -(void)changeSwitchToState:(TKSWithState) toState animated:(BOOL) animated ;


    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
