//
//  UIViewController+Shortcuts.swift
//  RTSPortal
//
//  Created by sajan on 23/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    
    class func dashboardViewController() -> UIViewController{
        let storyboard = UIStoryboard(name: "Profile", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "DashboardViewController")
    }

    class func profileDashboardViewController() -> UIViewController{
        let storyboard = UIStoryboard(name: "Profile", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "ProfileDashboardViewController")
    }
    
    class func myEventsListViewController() -> UIViewController{
        let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "MyEventsListViewController")
    }
    
    class func myEventDetailViewController() -> UIViewController{
        let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "MyEventDetailViewController")
    }
    
    class func allEventsListViewController() -> UIViewController{
        let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "AllEventListViewController")
    }

    class func allEventDetailsViewController() -> UIViewController{
        let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController")
    }

    class func myDetailsViewController() -> UIViewController{
        let storyboard = UIStoryboard(name: "Profile", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "MyDetailsViewController")
    }
    
    class func membershipViewController() -> UIViewController{
        let storyboard = UIStoryboard(name: "Membership", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "MemberShipViewController")
    }

    
    
    class func mySIGListViewController() -> UIViewController{
        let storyboard = UIStoryboard(name: "Profile", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "MySIGListViewController")
    }
    
    class func commasPreferenceViewController() -> UIViewController{
        let storyboard = UIStoryboard(name: "Profile", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "CommsPreferenceViewController")
    }

    class func qrScanViewController() -> UIViewController{
        let storyboard = UIStoryboard(name: "Profile", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "QRScanViewController")
    }

    class func statusUpdateListViewController() -> UIViewController{
        let storyboard = UIStoryboard(name: "General", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "StatusUpdateListViewController")
    }

    class func participantStatusViewController() -> UIViewController{
        let storyboard = UIStoryboard(name: "Profile", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "ParticipantStatusViewController")
    }
    
    class func participantDetailsViewController() -> UIViewController{
        let storyboard = UIStoryboard(name: "Profile", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "ParticipantDetailsViewController")
    }
    
    class func appInfoViewController() -> UIViewController{
        let storyboard = UIStoryboard(name: "Profile", bundle: Bundle.main)
        
        return storyboard.instantiateViewController(withIdentifier: "AppInfoViewController")
    }


}
