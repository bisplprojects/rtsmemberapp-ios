//
//  JSONStringify.swift
//  RTSPortal
//
//  Created by sajan on 10/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import Foundation

func JSONStringify(value: [String:String]) -> String {
    
    let encoder = JSONEncoder()
    var jsonString = ""
    if let jsonData = try? encoder.encode(value) {
        if let jsonStr = String(data: jsonData, encoding: .utf8) {
            jsonString = jsonStr
//            print(jsonString)
            return jsonString
        }
    }
    return jsonString
}

func convertIntoJSONString(arrayObject: [Any]) -> String? {
    
    do {
        let jsonData: Data = try JSONSerialization.data(withJSONObject: arrayObject, options: [])
        if  let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
            return jsonString as String
        }
        
    } catch let error as NSError {
        print("Array convertIntoJSON - \(error.description)")
    }
    return nil
}
