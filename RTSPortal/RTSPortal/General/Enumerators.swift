//
//  Enumerators.swift
//  RTSPortal
//
//  Created by sajan on 10/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import Foundation
import UIKit

enum RegistrationTextFieldType {
    
    case kRegistrationTextFieldTypeValid
    case kRegistrationTextFieldTypePrimaryEmail
    case kRegistrationTextFieldTypeBillingEmail
    case kRegistrationTextFieldTypeTitle
    case kRegistrationTextFieldTypeFirstName
    case kRegistrationTextFieldTypeLastName
    case kRegistrationTextFieldTypeProfession
    case kRegistrationTextFieldTypeJobTitle
    case kRegistrationTextFieldTypePlaceOfWork
    case kRegistrationTextFieldTypeGender
    case kRegistrationTextFieldTypeMobile
}


