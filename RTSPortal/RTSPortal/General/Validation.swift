//
//  Validation.swift
//  RTSPortal
//
//  Created by Sajan on 10/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

enum Alert {        //for failure and success results
    case success
    case failure
    case error
}
//for success or failure of validation with alert message
enum Valid {
    case success
    case failure(Alert, AlertMessages)
}
enum ValidationType {
    case FirstName
    case LastName
    case Email
    case Phone
    
    case RegistrationPrimaryEmail
    case RegistrationBillingEmail
    case RegistrationTitle
    case RegistrationFirstName
    case RegistrationLastName
    case RegistrationProfession
    case RegistrationJobTitle
    case RegistrationPlaceOfWork
    case RegistrationGender
    case RegistrationMobile

    case RegistrationTwitterName
    case RegistrationDietaryRequirements
    case RegistrationOtherDietaryRequirements
    case RegistrationAccessRequirements
    case RegistrationFamilyFriendlyRoomAccessRequirements
    case RegistrationVolunteer
    case RegistrationJoinDelegate
    case RegistrationBiography
    case RegistrationPicture


}
enum RegEx: String {
    case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" // Email
    case phoneNo = "[+0-9]{8,16}" // PhoneNo 5-16 Digits
    case name = "^[a-zA-Z ' ‘ '\\s]{1,32}$"
    
    case text = "^[a-zA-Z0-9 ' ‘ '\\s]{1,32}$"

    case base64String = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$"
    case notEmptyString = "/^[ A-Za-z0-9()[]+-*/%]*$/"

}

enum AlertMessages: String {
    case emptyFirstName = "Enter first name"
    case inValidFirstName = "Enter valid first name"
    case emptyLastName = "Enter last name"
    case inValidLastName = "Enter valid last name"
    case emptyEmail = "Enter email address"
    case inValidEmail = "Enter valid email"
    case emptyPhone = "Enter phone number"
    case inValidPhone = "Enter valid phone number"

    case emptyPrimaryEmail = "Enter primary email address"
    case inValidPrimaryEmail = "Enter valid primary email"
    case emptyBillingEmail = "Enter billing email address"
    case inValidBillingEmail = "Enter valid billing email"
    case emptyTitle = "Enter title"
    case inValidTitle = "Enter valid title"
    case emptyProfession = "Select profession"
    case inValidProfession = "Select profession "
    case emptyJobTitle = "Enter job title"
    case inValidJobTitle = "Enter valid job title"
    case emptyPlaceOfWork = "Enter place of work"
    case inValidPlaceOfWork = "Enter valid place of work"
    case emptyGender = "Select gender"
    case inValidGender = "Select gender "
    case emptyMobile = "Enter mobile number"
    case inValidMobile = "Enter valid mobile number"

    
    case emptyTwitterName = "Enter twitter name"
    case inValidTwitterName = "Enter valid twitter name"
    case emptyDietaryRequirements = "Select Dietary Requirements"
    case inValidDietaryRequirements = "Select Dietary Requirements "
    case emptyOtherDietaryRequirements = "Enter Other - Dietary Requirements"
    case inValidOtherDietaryRequirements = "Enter valid Other - Dietary Requirements"
    case emptyAccessRequirements = "Select Access Requirements"
    case inValidAccessRequirements = "Select Access Requirements "
    case emptyFamilyFriendlyRoomAccessRequirements = "Select Family friendly room access requested"
    case inValidFamilyFriendlyRoomAccessRequirements = "Select Family friendly room access requested "
    case emptyVolunteer = "Select Volunteer - Chair Session"
    case inValidVolunteer = "Select Volunteer - Chair Session "
    case emptyJoinDelegate = "Select Join Delegate list"
    case inValidJoinDelegate = "Select Join Delegate list "
    case emptyBiography = "Enter Biography"
    case inValidBiography = "Enter valid Biography"
    case emptyPicture = "Choose Picture"
    case inValidPicture = "Choose Picture "


    func localized() -> String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}

class Validation: NSObject {

    public static let shared = Validation()

    func validate(values: (type: ValidationType, inputValue: String)...) -> Valid {
        for valueToBeChecked in values {
            switch valueToBeChecked.type {
            case .FirstName:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .name, .emptyFirstName, .inValidFirstName)) {
                    return tempValue
                }
            case .LastName:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .name, .emptyLastName, .inValidLastName)) {
                    return tempValue
                }

            case .Email:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .email, .emptyEmail, .inValidEmail)) {
                    return tempValue
                }
            case .Phone:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .phoneNo, .emptyPhone, .inValidPhone)) {
                    return tempValue
                }
                
                
                
            case .RegistrationPrimaryEmail:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .email, .emptyPrimaryEmail, .inValidPrimaryEmail)) {
                    return tempValue
                }
            case .RegistrationBillingEmail:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .email, .emptyBillingEmail, .inValidBillingEmail)) {
                    return tempValue
                }
            case .RegistrationTitle:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .text, .emptyTitle, .inValidTitle)) {
                    return tempValue
                }
            case .RegistrationFirstName:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .text, .emptyFirstName, .inValidFirstName)) {
                    return tempValue
                }
            case .RegistrationLastName:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .text, .emptyLastName, .inValidLastName)) {
                    return tempValue
                }
            case .RegistrationProfession:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .text, .emptyProfession, .inValidProfession)) {
                    return tempValue
                }
            case .RegistrationJobTitle:
                if let tempValue = validField((valueToBeChecked.inputValue, .text, .emptyJobTitle, .inValidJobTitle)) {
                    return tempValue
                }
            case .RegistrationPlaceOfWork:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .text, .emptyPlaceOfWork, .inValidPlaceOfWork)) {
                    return tempValue
                }
            case .RegistrationGender:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .text, .emptyGender, .inValidGender)) {
                    return tempValue
                }
            case .RegistrationMobile:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .phoneNo, .emptyMobile, .inValidMobile)) {
                    return tempValue
                }
                
                
                
                
            case .RegistrationTwitterName:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .text, .emptyTwitterName, .inValidTwitterName)) {
                    return tempValue
                }
            case .RegistrationDietaryRequirements:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .text, .emptyDietaryRequirements, .inValidDietaryRequirements)) {
                    return tempValue
                }
            case .RegistrationOtherDietaryRequirements:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .text, .emptyOtherDietaryRequirements, .inValidOtherDietaryRequirements)) {
                    return tempValue
                }
            case .RegistrationAccessRequirements:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .text, .emptyAccessRequirements, .inValidAccessRequirements)) {
                    return tempValue
                }
            case .RegistrationFamilyFriendlyRoomAccessRequirements:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .text, .emptyFamilyFriendlyRoomAccessRequirements, .inValidFamilyFriendlyRoomAccessRequirements)) {
                    return tempValue
                }
            case .RegistrationVolunteer:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .text, .emptyVolunteer, .inValidVolunteer)) {
                    return tempValue
                }
            case .RegistrationJoinDelegate:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .text, .emptyJoinDelegate, .inValidJoinDelegate)) {
                    return tempValue
                }
            case .RegistrationBiography:
                if let tempValue = validField((valueToBeChecked.inputValue, .text, .emptyBiography, .inValidBiography)) {
                    return tempValue
                }
            case .RegistrationPicture:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .base64String, .emptyPicture, .inValidPicture)) {
                    return tempValue
                }


            }
        }
        return .success
    }
    
    func isValidString(_ input: (text: String, regex: RegEx, emptyAlert: AlertMessages, invalidAlert: AlertMessages)) -> Valid? {
        if input.text.trimmingCharacters(in: .whitespaces).isEmpty {
            return .failure(.error, input.emptyAlert)
        } else if isValidRegEx(input.text, input.regex) != true {
            return .failure(.error, input.invalidAlert)
        }
        return nil
    }
    
    func isValidRegEx(_ testStr: String, _ regex: RegEx) -> Bool {
        let stringTest = NSPredicate(format:"SELF MATCHES %@", regex.rawValue)
        let result = stringTest.evaluate(with: testStr)
        return result
    }

    func validField(_ input: (text: String, regex: RegEx, emptyAlert: AlertMessages, invalidAlert: AlertMessages)) -> Valid?
    {
        if input.text.isEmpty {
            return .failure(.error, input.emptyAlert)
        }
        return nil
    }

}
